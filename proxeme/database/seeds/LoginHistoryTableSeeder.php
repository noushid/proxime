<?php

use Illuminate\Database\Seeder;

class LoginHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\LoginHistory::class, 15)->create();
    }
}
