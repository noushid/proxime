<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        static $password;
        \App\User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'username' => 'admin',
            'password' => $password ?: $password = bcrypt('admin'),
            'remember_token' => str_random(10),
            'type' => 'admin',
            ]);

        \App\User::create([
            'name' => 'subadmin',
            'email' => 'sub@admin.com',
            'username' => 'subadmin',
            'password' => $password ?: $password = bcrypt('admin'),
            'remember_token' => str_random(10),
            'type' => 'subAdmin',
        ]);
        \App\User::create([
            'name' => 'noushid',
            'email' => 'pnoushid@gmail.com',
            'username' => 'pnoushid@gmail.com',
            'password' => bcrypt('admin'),
            'phone' => '8089183628',
            'status' => '1',
            'type' => 'public',
        ]);

        $command = $this->command;
//        $this->call('UserTableSeeder');

//        $command->info('User table seeding started...');
//        $this->call('UserTableSeeder');

        $command->info('Carrier table seeding started...');
        $this->call('CarrierTableSeeder');

        $command->info('Device table seeding started...');
        $this->call('DeviceTableSeeder');

        $command->info('Device History table seeding started...');
        $this->call('DeviceHistoryTableSeeder');

        $command->info('Device History table seeding started...');
        $this->call('DevicePackageTableSeeder');

        $command->info('Login history table seeding started...');
        $this->call('LoginHistoryTableSeeder');

        $command->info('Package table seeding started...');
        $this->call('PackageTableSeeder');
    }
}
