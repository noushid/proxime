<?php

use Illuminate\Database\Seeder;

class DevicePackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\DevicePackage::class, 10)->create();
    }
}
