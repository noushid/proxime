<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->nullable()->unique();
            $table->string('address')->nullable();
            $table->string('pin')->nullable();
            $table->string('place')->nullable();
            $table->string('district')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->integer('status')->default(1);
            $table->integer('online')->default(0);
            $table->string('ip_address')->nullable();
            $table->string('activation_code')->nullable();
            $table->string('secret_key')->nullable();
            $table->string('type')->nullable();
            $table->boolean('email_verify')->default(1);
            $table->string('email_token')->nullable();
            $table->string('update_email')->nullable();
            $table->boolean('verified')->default(0);
//            $table->string('forgotten_password_time')->nullable();
            $table->string('img')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->rememberToken()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
