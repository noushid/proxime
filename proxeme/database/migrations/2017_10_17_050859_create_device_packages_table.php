<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->default(1);
            $table->timestamp('recharge_date');
            $table->string('order_no');
            $table->integer('device_id');
            $table->integer('package_id');
            $table->integer('is_free')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_packages');
    }
}
