<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('model_no')->nullable();
            $table->string('color')->nullable();
            $table->string('deviceId')->unique();
            $table->string('activation_time')->nullable();
            $table->string('last_used_time')->nullable();
            $table->integer('status')->default(1);
            $table->integer('carrier_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('current_device_package_id')->nullable();
            $table->integer('current_package_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
