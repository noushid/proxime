<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->userName,
        'password' => $password ?: $password = bcrypt('admin'),
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'pin' => $faker->postcode,
        'place' => $faker->streetName,
        'district' => $faker->streetName,
        'state' => $faker->streetName,
        'country' => $faker->country,
        'status' => $faker->randomElement([1,0]),
        'online' => $faker->randomElement([1,0]),
        'ip_address' => $faker->ipv4,
        'activation_code' => str_random(10),
        'type' => $faker->randomElement(['public']),
        'last_login' => $faker->dateTime,

    ];
});



$factory->define(App\Carrier::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['vodafone', 'airtel', 'idea']),
    ];
});


$factory->define(App\Device::class, function (Faker $faker) {

    return [
        'name' => $faker->name(),
        'deviceId' => 'dvc'.$faker->unique()->numberBetween(001,100),
        'activation_time' => $faker->dateTime(),
        'last_used_time' => $faker->dateTime(),
        'status' => $faker->randomElement([0, 1]),
        'carrier_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'current_device_package_id' => $faker->randomElement([1, 2, 3, 4, 5]),
    ];
});


$factory->define(App\DeviceHistory::class, function (Faker $faker) {

    return [
        'datetime' => $faker->dateTime(),
        'latitude' => $faker->latitude(),
        'longitude' => $faker->longitude(),
        'range_status' => $faker->randomElement([1, 2, 3, 4]),
        'device_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});



$factory->define(App\DevicePackage::class, function (Faker $faker) {

    return [
        'status' => $faker->randomElement([1,2]),
        'recharge_date' => $faker->dateTime(),
        'order_no' => $faker->numberBetween(100,1000),
        'device_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'package_id' => $faker->randomElement([1, 2, 3, 4]),
    ];
});


$factory->define(App\LoginHistory::class, function (Faker $faker) {

    return [
        'datetime' => $faker->dateTime(),
        'user_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});


$factory->define(App\Package::class, function (Faker $faker) {

    return [
        'name' => $faker->name(),
        'description' => $faker->sentence(),
        'price' => $faker->numberBetween(1, 2),
        'duration' => $faker->numberBetween(1,30),
        'usage' => $faker->numberBetween(100, 1000),
    ];
});