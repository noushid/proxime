/**
 * Created by psybo-03 on 22/11/17.
 */
var modalLock = 0;
/****Profile edit start ********/
/*Show form (old)*/
function showInput(item){
    $('#'+item).css('display','inline-block');
    $('#editDvcName'+item).hide();
}
/*hide form (old)*/
function hideInput(item){
    $('#'+item).css('display','none');
    $('#editDvcName'+item).show();
}
/*Show form */
$('#editProfileBtn').click(function(e){
    e.preventDefault();
    $('#editProfileSection').removeClass('hidden');
    $('#showProfileSection').addClass('hidden');
    $('#resendOtp').addClass('hidden');
});

/*hide form */
$('#cancelEditProfile').click(function (e) {
    e.preventDefault();
    $('#showProfileSection').removeClass('hidden');
    $('#editProfileSection').addClass('hidden');
});

function editProfile(action){
    $('#edit'+action).removeClass('hidden');
    //$('#' + action).val('');
    $('#successphone').addClass('hidden');
    $('#verifyOTP').addClass('hidden');
    $('#genOTP').show();
    $('#genOTP').removeClass('active');
    $("#resendOtp").addClass('hidden');
    $("#error" + action).html('');
    $("#error" + action).removeClass('error');
    //$('#genOTP').toggleClass('active');
//    $('#edit'+action).html('<div class="error">'+response.responseJSON.phone+'</div>');

}

function cancelProfile(action){
    $('#edit' + action).addClass('hidden');
    $('#error' + action).html('');
    $('#otpphone').addClass('hidden');
    $('#resendOtp').addClass('hidden');
    $('#genOTP').toggleClass('active');
}


/*update profile by item*/
function update(item,e){
    $(e).prop('disabled', true);
    $.post('update-profile',$('#'+item).serialize())
        .done(function(response){
            if (item == 'name') {
                $('#usrName').html($('#' + item).val());
                $('#usrNameRes').html($('#' + item).val());
            }
            if($('#' + item).val() != '') {
                $('#show'+item).html($('#'+item).val());
            }
            $('#edit'+item).addClass('hidden');
            $(e).prop('disabled', false);
        })
        .fail(function(response){
            $(e).prop('disabled', false);
            $('#error'+item).addClass('error');
            $('#error'+item).html(response.responseJSON[item]);
        });

}
/*Generate OTP for Edit phone number*/

function getOtpPhone(item){
    $('#genOTP').addClass('active');
    var isValid = $("#phone").intlTelInput("isValidNumber");
    /****Remove error messages if exist*/
    $('#genOTP').prop('disabled',true);
    $('#otpphone').removeClass('has-error');
    $('#' + item).removeClass('error-phone');

    /*****Validate phone number ****/
    if(isValid == false) {
        $('#phone').val('');
        var phone = '';
    }else if(isValid == true){
        /****Append country based phone number to phone number field ****/
        //$("#phone").val($("#phone").intlTelInput("getNumber"));
        var phone = $("#phone").intlTelInput("getNumber");
    }
    $.post('getOtpPhone', {phone: phone})
        .done(function (response) {
            $('.load').removeClass('loading');

            $('#genOTP').prop('disabled', true);
            $('#genOTP').hide();
            $('#verifyOTP').removeClass('hidden');
            $('#otpphone').removeClass('hidden');
            //$('#successphone').removeClass('hidden');
            //$('#successphone').html('OTP Send to ' + $('#' + item).val());
            createAlert('', 'Success!', 'OTP send to ' + $('#' + item).val(), 'info', true, false, 'pageMessages');
            $('#genOTP').removeClass('active');
            setTimeout(function () {
                $("#resendOtp").removeClass('hidden');
            }, 30000);
        })
        .fail(function (response) {
            console.log(response.responseJSON.phone);
            $('#otpBtn').prop('disabled', false);
            $('.load').removeClass('loading');
            //$('#error' + item).addClass('has-error');
            $('#' + item).addClass('error-phone');

            $('#error' + item).removeClass('hidden');
            $('#error' + item).addClass('error');
            $('#error' + item).html(response.responseJSON.phone);

            $('#genOTP').removeClass('active');
            //setTimeout(function () {
            //    $('#error' + item).html('');
            //}, 30000);
        });
}

/****** Regenerate OTP In case of unable receive OTP ******/
function reGenOtpPhone(item){
    $.post('getOtpPhone',$('#'+item).serialize())
        .done(function(response){
            $('.load').removeClass('loading');
            $("#resendOtp").hide();
            $('#success' + item).html('OTP Send to ' + $('#' + item).val());
            setTimeout(function () {
                $("#resendOtp").addClass('hidden');
            }, 30000);
        })
        .fail(function(response){
            $('#otpBtn').prop('disabled', false);
            $('.load').removeClass('loading');
        });
}

/*Validate new phone number with current phone number*/
function validate(el,item){
    var input= $(el).val();
    var replaced = input.replace(/\s/g,'').substr(1);
    if(replaced != item && replaced.replace(/_/g, '').length >11) {
        $('#genOTP').prop('disabled',false);
        $('#updateEmail').prop('disabled',false);
    }else{
        $('#genOTP').prop('disabled',true);
        $('#updateEmail').prop('disabled',true);
    }
}

/*Validate new phone number with curren tphone number*/
function validateEmail(el,item){
    var input= $(el).val();
    console.log(input);
    console.log(item);
    if(input != item) {
        $('#updateEmail').prop('disabled', false);
    }else{
        $('#updateEmail').prop('disabled', true);
    }
}

/*Verify Entered OTP*/
function verifyOtpPhone(item){
    $('#otp' + item).removeClass('error-phone');
    $.post('verifyOtp',$('#otp'+item).serialize())
        .done(function(response){
            //var input= $('#'+item).val();
            var input = $("#phone").intlTelInput("getNumber");
            //var replaced = input.replace(/\s/g,'');
            input = input.substr(1);
            $.post('update-profile',{phone:input})
                .done(function(response){
                    $("#phone").val($("#phone").intlTelInput("getNumber"));
                    $('#edit'+item).addClass('hidden');
                    $('#show'+item).html($('#'+item).val());
                })
                .fail(function(response){
                    $('#error'+item).html('<span> Enter valid OTP.</span>');
                    //$('#otp'+item).addClass('has-error');
                    $('#otp' + item).addClass('error-phone');

                });
            $('#otp' + item).val('');
            $('#otp' + item).addClass('hidden');
            $('#genOTP').show();
            $('#verifyOTP').addClass('hidden');
        })
        .fail(function(response){
            $('#error'+item).html('<span> Enter valid OTP.</span>');
            $('#otp' + item).addClass('error-phone');
            //$('#genOTP').show();
        });
}

/*Update User email*/
$('#updateEmail').click(function(){
    $('#updateEmail').prop('disabled',true);
    $.post('verify-email',$('#email').serialize())
        .done(function(response){
            $('#editemail').addClass('hidden');
            createAlert('', 'Success!', 'Email verification link send to ' + $('#email').val(), 'success', true, true, 'pageMessages');
            $('#email').val('');
            $('#updateEmail').prop('disabled',false);
            //$('#showmessage').html('Email verification link send to'+ $('#email').val());
            //setTimeout(function () {
            //    $('#showmessage').html('');
            //}, 20000);
        })
        .fail(function(response){
            $('#otpIntput').addClass('has-error');
            $('#erroremail').html('<div class="error">'+response.responseJSON.email+'</div>');
            $('#updateEmail').prop('disabled',false);
        });
});

jQuery(document).ready(function($) {
    $(document).ready(function() {
        $('.lab-slide-up').find('a').attr('data-toggle', 'modal');
        $('.lab-slide-up').find('a').attr('data-target', '#lab-slide-bottom-popup');
    });

});
/****Profile edit end ********/

/*device sort form submit on select*/
$('#deviceOrder').change(function(){
    $('#deviceOrderForm').submit();
});

/*subscription sort form submit on select*/
$('#subscriptionOrderForm').change(function () {
    $('#subscriptionOrderForm').submit();
});


/*Modal close on click*/
$('#modalDismiss').click(function () {
    if (modalLock != 1) {
        $('#deviceId').val('');
        $('#lab-slide-bottom-popup').modal('hide');
    }
});


/************ Map Scripts start *******/

function openNav() {
    document.getElementById("mySidenav").style.width = "25%";
    console.log()
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

jQuery(document).ready(function($) {
    $(document).ready(function() {
        $('.lab-slide-up').find('a').attr('data-toggle', 'modal');
        $('.lab-slide-up').find('a').attr('data-target', '#lab-slide-bottom-popup');
    });

});
/************ Map Scripts end *******/

/************ Prevent form submission on hitting Enter key for Text  *******/
document.getElementById("deviceId").onkeypress = function (e) {
    var key = e.charCode || e.keyCode || 0;
    if (key == 13) {
        e.preventDefault();
    }
};



$('#addDeviceBtn').click(function () {
    $.post($('#deviceAddForm').attr('action'), $('#deviceAddForm').serialize())
        .done(function (response) {
            modalLock = 1;
            window.location = 'map';
            /*
            //////This code for show the all packages on the same popup
            if (response.status == 'package_null') {
                $('#addDeviceBtn').prop('disabled', true);
                //$('#userPackage').removeClass('hidden');
                *//*get packages and append to a div*//*
                $.get('getPackages')
                    .done(function (response) {
                        //$('#userPackage').html(response);

                    })
                    .fail(function (response) {
                        console.log('error');
                    });

                $('#deviceId').val('');
            } else {
                //window.location = "user-devices";
                //window.location = document.referrer;
                $.post('set-session', {type: 'message', data: 'Device added'});
                window.location.reload();
                createAlert('', 'Success!', 'Device Added', 'success', true, true, 'pageMessages');
            }*/
        })
        .fail(function (response) {
            console.log(response.responseText);
            modalLock = 0;
            $('#deviceAddError').html(response.responseText);
            createAlert('Opps!', 'Something went wrong', response.responseText, 'danger', true, true, 'pageMessages', true);
        });
});

