var properties = {
    "data": [
        {
            "id": 1,
            "title": "Devise 1",
            "listing_for": "Sale",
            "is_featured": true,
            "latitude": 11.120298,
            "longitude": 76.119968,
            "address": "123 Kathal St. Tampa City, ",
            "area": "battery-full",
            "area2": 50,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "img/properties/properties-1.jpg",
            "type_icon": "img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor "
        },
        {
            "id": 2,
            "title": "Devise 2",
            "listing_for": "Rent",
            "is_featured": false,
            "latitude": 51.538395,
            "longitude": -0.097418,
            "address": "123 Kathal St. Tampa City, ",
            "area": "battery-empty",
            "area2": 50,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "img/properties/properties-2.jpg",
            "type_icon": "img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor "
        },
        {
            "id": 3,
            "title": "Devise 3",
            "listing_for": "Sale",
            "is_featured": true,
            "latitude": 51.539212,
            "longitude": -0.118403,
            "address": "123 Kathal St. Tampa City, ",
            "area": "battery-full",
            "area2": 50,
            "room": 4,
            "bathroom": 3,
            "balcony": 3,
            "lounge": 1,
            "garage": 1,
            "image": "img/properties/properties-1.jpg",
            "type_icon": "img/building.png",
            "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor "
        }
    ]
};

function drawInfoWindow(property) {
    var image = 'img/logo.png';
    if (property.image) {
        image = property.image
    }

    var title = 'N/A';
    if (property.title) {
        title = property.title
    }

    var address = '';
    if (property.address) {
        address = property.address
    }

    var description = 'lorem ipsum dolor sit amet, cfeugiat congue qmper lorem ipsum dolor sit amet';
    if (property.description) {
        description = property.description
    }

    var area = 1000;
    if (property.area) {
        area = property.area
    }

    var Beds = 'inActive';
    if (property.bedroom) {
        Beds = property.bedroom
    }

    var bathroom = 5;
    if (property.bathroom) {
        bathroom = property.bathroom
    }

    var garage = 1;
    if (property.garage) {
        garage = property.garage
    }

    var price = 253.33;
    if (property.price) {
        price = property.price
    }

    var ibContent = '';
    ibContent =
        "<div class='map-properties'>" +
        "<div class='map-content'>" +
        "<h4><a href='properties-details.html'>" + title + "</a></h4>" +
        "<p class='address'> <i class='fa fa-map-marker'></i>" + address + "</p>" +
        "<p class='description'>" + description + "</p>" +"</div>";
    return ibContent;
}

function insertPropertyToArray(property, layout) {
    var image = 'img/logo.png';
    if (property.image) {
        image = property.image
    }

    var title = 'N/A';
    if (property.title) {
        title = property.title
    }

    var listing_for = 'Sale';
    if (property.listing_for) {
        listing_for = property.listing_for
    }

    var address = '';
    if (property.address) {
        address = property.address
    }

    var description = 'lorem ipsum dolor sit amet, cfeugiat congue qmper lorem ipsum dolor sit amet';
    if (property.description) {
        description = property.description
    }

    var area = 1000;
    if (property.area) {
        area = property.area
    }

    var area2 = 10;
    if (property.area) {
        area = property.area
    }

    var Beds = 'inActive';
    if (property.bedroom) {
        Beds = property.bedroom
    }

    var bathroom = 5;
    if (property.bathroom) {
        bathroom = property.bathroom
    }

    var garage = 1;
    if (property.garage) {
        garage = property.garage
    }

    var balcony = 1;
    if (property.balcony) {
        balcony = property.balcony
    }

    var lounge = 1;
    if (property.lounge) {
        lounge = property.lounge
    }

    var price = 253.33;
    if (property.price) {
        price = property.price
    }

    var is_featured = '';
    if (property.is_featured) {
        is_featured = '<div class="property-tag button alt featured">Featured</div> ';
    }

    var element = '';

    if(layout == 'grid_layout'){
        element = '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><div class="property">' +
            '<!-- Property img --> ' +
            '<a href="properties-details.html" class="property-img">' +
            is_featured +
            '<div class="property-tag button sale">'+listing_for+'</div> ' +
            '<div class="property-price">$'+price+'</div> ' +
            '<img src="'+image+'" alt="properties-3" class="img-responsive"> ' +
            '</a>' +
            '<!-- Property content --> ' +
            '<div class="property-content"> ' +
            '<!-- title --> ' +
            '<h1 class="title">' +
            '<a href="properties-details.html">'+title+'</a> ' +
            '</h1> ' +
            '<!-- Property address --> ' +
            '<h3 class="property-address"> ' +
            '<a href="properties-details.html"> ' +
            '<i class="fa fa-map-marker"></i>'+address+' ' +
            '</a> ' +
            '</h3> ' +
            '<!-- Facilities List --> ' +
            '<ul class="facilities-list clearfix"> ' +
            '<li> ' +
            '<i class="flaticon-square-layouting-with-black-square-in-east-area"></i> ' +
            '<span>'+area+' sq ft</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-bed"></i> ' +
            '<span>'+Beds+' Beds</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-monitor"></i> ' +
            '<span>TV </span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-holidays"></i> ' +
            '<span> '+bathroom+' Baths</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-vehicle"></i> ' +
            '<span>'+garage+' Garage</span> ' +
            '</li> ' +
            '<li> ' +
            '<i class="flaticon-building"></i> ' +
            '<span> '+balcony+' Balcony</span> ' +
            '</li> ' +
            '</ul> ' +
            '<!-- Property footer --> ' +
            '<div class="property-footer"> ' +
            '<span class="left"><i class="fa fa-calendar-o icon"></i> 5 days ago</span> ' +
            '<span class="right"> ' +
            '<a href="#"><i class="fa fa-heart-o icon"></i></a> ' +
            '<a href="#"><i class="fa fa-share-alt"></i></a> ' +
            '</span> ' +
            '</div>' +
            '</div>' +
            '</div>';
            '</div>';
    }
    else{
        element = '' +
            '<div class="col-md-12 property-content"> ' +
                '<!-- title --> ' +
                '<h1 class="title"> <i class="fa fa-desktop"></i>' +title+'</h1> ' +
                '<!-- Property address --> ' +
                '<h3 class="property-address"> ' +
                    '<i class="fa fa-map-marker"></i>'+address+
                '</h3>' +
                '<!-- Facilities List --> ' +
                '<ul class="facilities-list clearfix"> ' +
                    '<li> ' +
                        '<i class="fa fa-'+area+'"></i> ' +
                        '<span>'+area2+' %</span> ' +
                    '</li> ' +
                    '<li> ' +
                        '<i class="fa fa-power-off"></i> ' +
                        '<span>'+Beds+'</span> ' +
                    '</li> ' +
                '</ul> ' +
                '<!-- Property footer --> ' +
                '<div class="property-footer"> ' +
                    '<span class="left"><i class="fa fa-clock-o"></i>10/12/2017 10.00 PM</span> ' +
                    '<span class="right"> ' +
                        '<div class="title-area2">'+
                            '<a  class="show-more-options pull-right" data-toggle="collapse" data-target="#options-content">'+
                                '<i class="fa fa-plus-circle"></i> Show More'+
                            '</a>'+
                            '<div class="clearfix"></div>'+

                        '</div>' +
                    '</span> ' +
                '</div> ' +
            '</div> ' +
            '<div id="options-content" class="collapse col-md-12 property-content">'+
                '<ul class="facilities-list clearfix"> ' +
                    '<li> ' +
                        '<span>'+address+'</span> ' +
                    '</li> ' +
                    '<li> ' +
                        '<span>'+title+'</span> ' +
                    '</li> ' +
                '</ul> ' +
            '</div>';
    }
    return element;
}

function animatedMarkers(map, propertiesMarkers, properties, layout) {
    var bounds = map.getBounds();
    var propertiesArray = [];
    $.each(propertiesMarkers, function (key, value) {
        if (bounds.contains(propertiesMarkers[key].getLatLng())) {
            propertiesArray.push(insertPropertyToArray(properties.data[key], layout));
            setTimeout(function () {
                if (propertiesMarkers[key]._icon != null) {
                    propertiesMarkers[key]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable bounce-animation marker-loaded';
                }
            }, key * 50);
        }
        else {
            if (propertiesMarkers[key]._icon != null) {
                propertiesMarkers[key]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable';
            }
        }
    });
    $('.fetching-properties').html(propertiesArray);
}

function generateMap(latitude, longitude, mapProvider, layout) {

    var map = L.map('map', {
        center: [latitude, longitude],
        zoom: 14,
        scrollWheelZoom: false
    });

    L.tileLayer.provider(mapProvider).addTo(map);
    var markers = L.markerClusterGroup({
        showCoverageOnHover: false,
        zoomToBoundsOnClick: false
    });
    var propertiesMarkers = [];

    $.each(properties.data, function (index, property) {
        var icon = '<img src="img/logos/logo.png">';
        if (property.type_icon) {
            icon = '<img src="' + property.type_icon + '">';
        }
        var color = '';
        var markerContent =
            '<div class="map-marker ' + color + '">' +
            '<div class="icon">' +
            icon +
            '</div>' +
            '</div>';

        var _icon = L.divIcon({
            html: markerContent,
            iconSize: [36, 46],
            iconAnchor: [18, 32],
            popupAnchor: [130, -28],
            className: ''
        });

        var marker = L.marker(new L.LatLng(property.latitude, property.longitude), {
            title: property.title,
            icon: _icon
        });

        propertiesMarkers.push(marker);
        marker.bindPopup(drawInfoWindow(property));
        markers.addLayer(marker);
        marker.on('popupopen', function () {
            this._icon.className += ' marker-active';
        });
        marker.on('popupclose', function () {
            this._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
        });
    });

    map.addLayer(markers);
    animatedMarkers(map, propertiesMarkers, properties, layout);
    map.on('moveend', function () {
        animatedMarkers(map, propertiesMarkers, properties, layout);
    });

    $('.fetching-properties .item').hover(
        function () {
            propertiesMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded marker-active';
        },
        function () {
            propertiesMarkers[$(this).attr('id') - 1]._icon.className = 'leaflet-marker-icon leaflet-zoom-animated leaflet-clickable marker-loaded';
        }
    );

    $('.geolocation').on("click", function () {
        map.locate({setView: true})
    });
    $('#map').removeClass('fade-map');
    // $('#map').css('height', '600px');

}