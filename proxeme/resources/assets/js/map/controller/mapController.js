/**
 * Created by psybo-03 on 7/11/17.
 */

app.controller('MapController', ['NgMap', '$scope', '$timeout', '$http', '$location', '$rootScope', '$window', '$location', '$ngConfirm', function (NgMap, $scope, $timeout, $http, $location, $rootScope, $window, $location, $ngConfirm) {
    $rootScope.base_url = $location.protocol() + "://" + location.host;
    $scope.states = {};

    console.log($rootScope.base_url);

    NgMap.getMap().then(function (map) {
        console.log(map.getCenter());
        console.log('markers', map.markers);
        console.log('shapes', map.shapes);
    });

    $scope.address = "SF, CA";
    $scope.onDragEnd = function (marker, $event) {
        var lat = marker.latLng.lat();
        var lng = marker.latLng.lng();
        /* GeoCoder: To convert LntLng to Formatted address with Mustaches :D */
        geocoder = new $event.google.maps.Geocoder();
        geocoder.geocode({
            'latLng': new $event.google.maps.LatLng(lat, lng)
        }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    // @url: https://developers.google.com/maps/documentation/javascript/geocoding#ReverseGeocoding
                    // We return the second result, which is less specific than the first
                    //  (in this case, a neighborhood name)
                    var formatted_address = results[0].formatted_address;
                    $timeout(function () {
                        $scope.address = formatted_address;
                    }, 300);
                } else {
                    alert('No results found');
                }
            } else {
                alert('Geocoder failed due to: ' + status);
            }
        });
    };


    $scope.loadUser = function (id) {
        $http.get('api/web/user/' + id)
            .then(function success(response) {
                $scope.user = response.data;
                console.log($scope.user.device.length);
                if ($scope.user.device.length > 0) {
                    $scope.device = {
                        deviceId: $scope.user.device[0].deviceId,
                        position: '12.971599, 77.594563'
                    };
                    $scope.states.activeDevice = $scope.user.device[0].id;
                } else {
                    $scope.device = {
                        deviceId: null,
                        position: '12.971599, 77.594563'
                    };
                }
            },
            function error() {
                console.log('count errro');
            });

    };


    $scope.reset = function () {
        $scope.addSuccess = false;
        $scope.addDeviceError = false;
        $scope.msg = '';
        $scope.newDevice = {};
    };
    $scope.addNewDevice = function () {
        $http.post($rootScope.base_url + '/api/web/add-device', $scope.newDevice)
            .then(function (response) {
                console.log(response);
                console.log($scope.user);
                $scope.addDeviceError = false;
                //$scope.user.device.push(response.data);
                $scope.user = response.data;
                $scope.newDevice = {};
                $scope.addSuccess = true;
            }, function (response) {
                console.log('error');
                console.log(response);
                $scope.addDeviceError = true;
                $scope.newDevice = {};
                $scope.msg = 'please Enter valid deviceId';
            })
    };
    $scope.temp = 0;


    $scope.setDevice = function (item) {
        $http.get($rootScope.base_url + '/api/web/check-package/' + item.id)
            .then(function (response) {
                if (response.data == false) {
                    $ngConfirm({
                        title: 'Warning',
                        content: 'No more Subscription available. Choose one?',
                        scope: $scope,
                        buttons: {
                            sayBoo: {
                                text: 'Yes',
                                btnClass: 'btn-blue',
                                action: function (scope, button) {
                                    $window.location.href = 'subscriptions/' + item.deviceId + '?r=map';
                                    return false; // prevent close;
                                }
                            },
                            close: function (scope, button) {
                                // closes the modal
                            }
                        }
                    });

                } else {
                    console.log(response.data);
                }
            }, function (response) {
                console.log('error');
                console.log(response);
            });
        $scope.loadingMap = true;
        $scope.states.activeDevice = item.id;
        var temp = ['12.971599, 77.594563', '11.120298, 76.119968', '12.968988, 77.614503'];
        $scope.device = {
            deviceId: item.deviceId,
            position: temp[$scope.temp]
        };
        if ($scope.temp <= 2) {
            $scope.temp = $scope.temp + 1;
        } else {
            $scope.temp = 0;
        }
        $scope.loadingMap = false;
    };

    $scope.openNav = function (item) {
        $scope.navStyle = {'width': "25%"};
        $scope.deviceDetails = item;
        $scope.showstatus = true;
    };

    $scope.closeNav = function () {
        $scope.navStyle = {'width': "0"};
    };


}]);