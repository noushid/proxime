/**
 * Created by Noushid on 19/10/17.
 */

angular.module('DeviceService',[]).factory('Device',['$resource',
    function($resource){
        return $resource('/api/device/:deviceId',{
            deviceId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
