/**
 * Created by Noushid on 20/10/17.
 */

angular.module('CarrierService',[]).factory('Carrier',['$resource',
    function($resource){
        return $resource('/api/carrier/:carrierId',{
            carrierId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);