/**
 * Created by psybo-03 on 24/10/17.
 */

angular.module('SubscriptionService',[]).factory('Subscription',['$resource',
    function($resource){
        return $resource('/api/package/:packageId',{
            packageId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);