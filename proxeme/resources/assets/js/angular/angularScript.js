var app = angular.module('proximeApp',
    [
        'ngRoute',
        'ui.bootstrap',
        'ngResource',
        'angularUtils.directives.dirPagination',
        'angular-loading-bar',
        'chart.js',
        'ngMap',
        'UserService',
        'DeviceService',
        'CarrierService',
        'SubscriptionService',
        'ng-file-model',
        'cp.ngConfirm',
        'ngDialog',
        'angularRandomString',
        'internationalPhoneNumber'
    ]
);


app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider','ipnConfig', function ($routeProvider, $locationProvider, cfpLoadingBarProvider,ipnConfig) {
    //cfpLoadingBarProvider.parentSelector = '#loading';
    /*
    * loading bar configuration
    * https://github.com/chieffancypants/angular-loading-bar
    * */
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    /*
    * ng-route config
    * */
    //ngIntlTelInputProvider.set({
    //    autoHideDialCode: false,
    //    formatOnDisplay: false,
    //    initialCountry: "auto",
    //    placeholderNumberType: "MOBILE",
    //    separateDialCode: true,
    //    geoIpLookup: function (callback) {
    //        $.get("http://ipinfo.io", function () {
    //        }, "jsonp").always(function (resp) {
    //            var countryCode = (resp && resp.country) ? resp.country : "";
    //            callback(countryCode);
    //        });
    //    }
    //});

    ipnConfig.defaultCountry = 'pl';
    ipnConfig.preferredCountries = ['pl', 'de', 'fr', 'uk', 'es'];
    /*
   * ng-route config
   * */
    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            templateUrl: 'template/dashboard',
            controller: 'DashboardController'
        })
        .when('/dashboard', {
            templateUrl: 'template/dashboard'
        })
        .when('/sub-admin', {
            templateUrl: 'template/sub-admin',
            controller: 'SubAdminController'
        })
        .when('/devices', {
            templateUrl: 'template/device',
            controller: 'DeviceController'
        })
        .when('/carrier', {
            templateUrl: 'template/carrier',
            controller: 'CarrierController'
        })
        .when('/package', {
            templateUrl: 'template/package',
            controller: 'PackagesController'
        })
        .when('/users', {
            templateUrl: 'template/user',
            controller: 'UserController'
        })
        .when('/profile', {
            templateUrl: 'template/profile',
            controller: 'UserProfileController'
        });

}]);



//Pagination filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});



app.directive('ngConfirmClick', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }
]);

app.config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        chartColors: ['#FF5252', '#FF8A80'],
        responsive: true
    });
    // Configure all line charts
    ChartJsProvider.setOptions('line', {
        showLines: true
    });
}]);
