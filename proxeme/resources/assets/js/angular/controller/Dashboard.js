/**
 * Created by psybo-03 on 19/10/17.
 */

app.controller('DashboardController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'Device', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document,Device) {
    $scope.error = {};
    var base_url = $scope.baseUrl = $location.protocol() + "://" + location.host + '/';
    $rootScope.base_url = base_url;
    $rootScope.public_url = $scope.baseUrl = $location.protocol() + "://" + location.host;

        $scope.currentPage = 1;
        $scope.paginations = [10, 20, 25, 50, 100, 200];
        $scope.numPerPage = 10;
        $rootScope.currentRoute = $location.path();

        $rootScope.user = {};
    $scope.curuser = {};
    $scope.newuser = {};
    $scope.formdisable = false;
    $scope.direction = false;
    $rootScope.apiUrl = 'api/virtualDevice';

        loadUser();
        function loadUser() {
            $http.get('api/getCurrentUser')
                .then(function success(response) {
                    $rootScope.user = response.data;
                },
                function error() {
                    console.log('current user get erorr');
                }
            )
        }



    $scope.sort = function (column) {
        if ($rootScope.orderProp === column) {
            $scope.direction = !$scope.direction;
        } else {
            $rootScope.orderProp = column;
            $scope.direction = false;
        }
    };

    $scope.format = 'yyyy/MM/dd';
    //$scope.date = new Date();


    /******DATE Picker start******/
    $scope.today = function () {
        $scope.date = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.date = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.date = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }


    /******DATE Picker END******/

    /******Modal END******/

    function openModal(content, size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            controllerAs: '$scope',
            size: size,
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return content;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

        loadVirtualDevice();
        //loadDevice();
        getTotalDeviceCount();
        getTotaluserCount();
        getActiveUserCount();

        function getTotalDeviceCount() {
            $http.get('api/getDeviceCount')
                .then(function success(response) {
                    $scope.deviceCount = response.data;
                },
                function error() {
                    console.log('count errro');
                }
            )
        }

        function getTotaluserCount() {
            $http.get('api/getUserCount')
                .then(function success(response) {
                    $scope.userCount = response.data;
                },
                function error() {
                    console.log('count errro');
                }
            )
        }

        function getActiveUserCount() {
            $http.get('api/getActiveUserCount')
                .then(function success(response) {
                    $scope.userActiveCount = response.data;
                },
                function error() {
                    console.log('count errro');
                }
            )
        }


    function loadVirtualDevice() {
        $http.get('api/virtualDevice')
            .then(function success(response) {
                $scope.virtualDevices = response.data;
            },
            function error(response) {
                console.log(response);
            });
    }

    function loadDevice() {
        Device.query(function(response) {
            $scope.devices = response;
        })
    }


    /*First chart (Device statitics)*/
        $scope.labels = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $scope.series = ['Series A', 'Series B'];
        $scope.data = [
            [65, 59, 80, 81, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];

/*Pie chart*/
        $scope.pielabels = ["Active Devices", "Offine Devices", "Charge less"];
        $scope.piedata = [300, 500, 100];


}]);

