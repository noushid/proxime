/**
 * Created by Noushid on 20/10/17.
 */


app.controller('CarrierController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'Carrier', '$ngConfirm', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, Carrier, $ngConfirm) {

        $rootScope.currentRoute = $location.path();
        $scope.carriers = [];
        $scope.newcarrier = {};
        $scope.showform = false;
        $scope.title = 'Carrier';
        loadCarrier();

        $scope.loadCarriers = function () {
            $scope.trashed = false;
            $scope.title = 'Carrier';
            loadCarrier();
        };

        /**
         * Load All user
         * */
        function loadCarrier() {
            Carrier.query(function (carrier) {
                $scope.carriers = carrier;
            });
        }


        /**
         * Method for New User and show the form.
         *
         * */
        $scope.newCarrier = function () {
            $scope.showform = true;
            $scope.newcarrier = {};
            $scope.validationErrors = {};
        };

        /**
         * Method for Edit User and show the form.
         * Assign user values to form
         * */
        $scope.editCarrier = function (item) {
            $scope.newcarrier = angular.copy(item);
            $scope.curcarrier = item;
            $scope.showform = true;
            $scope.validationErrors = {};
            $window.scrollTo(0, 0);
        };


        /**
         * Cancel The form Or reset form
         * */
        $scope.cancel = function () {
            $scope.showform = false;
            $scope.newcarrier = {};
        };


        /**
         * form post method
         * */
        $scope.addCarrier = function () {
            /**
             * Check if user or new user
             * Already user edit their data
             * else add new user
             *
             * */
            if ($scope.newcarrier.id) {
                Carrier.update($scope.newcarrier).
                    $promise.then(function (response) {
                        loadCarrier();
                        var curIndex = $scope.carriers.indexOf($scope.curcarrier);
                        console.log(curIndex);
                        $scope.carriers[curIndex] = response;
                        $scope.showform = false;
                        $scope.newcarrier = {};
                    }, function (response) {
                        $scope.validationErrors = response.data;
                        console.log(response.data);
                    });
            } else {
                $scope.newcarrier.password = 'admin';
                $scope.newcarrier.type = 'Carrier';
                Carrier.save($scope.newcarrier).
                    $promise.then(function (response) {
                        $scope.carriers.push(response);
                        $scope.showform = false;
                        $scope.newcarrier = {};
                    }, function (response) {
                        $scope.validationErrors = response.data;
                        console.log(response.data);
                    });
            }
        };

        /**
         * Delete Carrier
         *
         * */
        $scope.deleteCarrier = function (item) {
            $ngConfirm({
                title: 'Warning!',
                content: 'Would you like to delete this item?',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete(function (response) {
                                var curIndex = $scope.carriers.indexOf(item);
                                console.log(curIndex);
                                $scope.carriers.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        /**
         * Get Trashed Record
         *
         **/
        $scope.trash = function () {
            $scope.title = 'Trashed Carrier';
            $scope.trashed = true;
            Carrier.query({item: 'trash'}, function (carrier) {
                $scope.carriers = carrier;
            });
        };

        /**
         * Permanent Delete
         **/
        $scope.deleteTrash = function (item) {
            $ngConfirm({
                title: 'Alert',
                content: '<b>Are you sure?</b> This action cannot be undone.',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete({delete: 'permanent'}, function (response) {
                                var curIndex = $scope.carriers.indexOf(item);
                                $scope.carriers.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        $scope.restore = function (item) {
            $ngConfirm({
                title: 'Warning',
                content: 'Are you sure?',
                scope: $scope,
                buttons: {
                    submit: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            $http.post('api/carrier/restore', {id: item.id})
                                .then(function (response) {
                                    var curIndex = $scope.carriers.indexOf(item);
                                    $scope.carriers.splice(curIndex, 1);
                                }, function (response) {
                                    console.log(response.data || 'Request failed');
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });
        };

    }]);
