    /**
 * Created by psybo-03 on 19/10/17.
 */


app.controller('DeviceController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'Device','Carrier','Subscription','$ngConfirm','ngDialog','$anchorScroll', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, Device, Carrier,Subscription,$ngConfirm,ngDialog,$anchorScroll) {

        $scope.title = 'Devices';
        $scope.devices = [];
        $scope.newdevice = {};
        $scope.uploadfiles = {};
        $scope.showform = false;
        $rootScope.currentRoute = $location.path();

        loadDevice();
        loadCarrier();

        $scope.loadDevices= function () {
            $scope.trashed = false;
            $scope.title = 'Devices';
            loadDevice();
        };
        /**
         * Load All user
         * */
        function loadDevice() {
            Device.query(function (device) {
                $scope.devices = device;
            });
        }


        /**
         * Load carrier
         * */

        function loadCarrier() {
            Carrier.query(function(carrier) {
                $scope.carriers = carrier;
            })
        }
        /**
         * Method for New User and show the form.
         *
         * */
        $scope.newDevice = function () {
            $scope.showform = true;
            $scope.newdevice = {};
            $scope.validationErrors = {};
        };

        /**
         * Method for Edit User and show the form.
         * Assign user values to form
         * */
        $scope.editDevice = function (item) {
            $scope.newdevice = angular.copy(item);
            $scope.curdevice = item;
            $scope.showform = true;
            $scope.validationErrors = {};
            $window.scrollTo(0, 0);
        };


        /**
         * Cancel The form Or reset form
         * */
        $scope.cancel = function () {
            $scope.showform = false;
            $scope.newdevice = {};
        };


        /**
         * form post method
         * */
        $scope.addDevice = function () {
            /**
             * Check if user or new user
             * Already user edit their data
             * else add new user
             *
             * */

            $scope.validationErrors = [];
             if ($scope.newdevice.id) {
                 $scope.validationErrors = [];
                Device.update($scope.newdevice, function (response) {
                    loadDevice();
                    var curIndex = $scope.devices.indexOf($scope.curdevice);
                    $scope.devices[curIndex] = response;
                    $scope.showform = false;
                    $scope.newdevice = {};
                });
                 Device.update($scope.newdevice).
                     $promise.then(function(response) {
                         loadDevice();
                         var curIndex = $scope.devices.indexOf($scope.curdevice);
                         $scope.devices[curIndex] = response;
                         $scope.showform = false;
                         $scope.newdevice = {};
                     },function(response) {
                         $scope.validationErrors = response.data;

                     })
            } else {
                //Device.save($scope.newdevice, function (response) {
                //    $scope.devices.unshift(response);
                //    $scope.showform = false;
                //    $scope.newdevice = {};
                //});
                Device.save($scope.newdevice).
                    $promise.then(function(response) {
                        $scope.devices.unshift(response);
                        $scope.showform = false;
                        $scope.newdevice = {};
                    },function(response) {
                        $scope.validationErrors = response.data;
                        console.log(response.data);
                    })
            }
        };

        /**
         * Delete Device
         *
         * */
        $scope.deleteDevice = function (item) {
            $ngConfirm({
                title: 'Warning!',
                content: 'Would you like to delete this item?',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete(function (response) {
                                var curIndex = $scope.devices.indexOf(item);
                                $scope.devices.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        /**
         * Get Trashed Record
         *
         **/
        $scope.trash = function () {
            $scope.title = 'Trashed Device';
            $scope.trashed = true;
            Device.query({item: 'trash'}, function (device) {
                $scope.devices = device;
            });
        };

        /**
         * Permanent Delete
         **/
        $scope.deleteTrash=function(item) {

            $ngConfirm({
                title: 'Alert',
                content: '<b>Are you sure?</b> This action cannot be undone.',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete({delete: 'permanent'}, function (response) {
                                var curIndex = $scope.devices.indexOf(item);
                                $scope.devices.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });
        };

        $scope.restore=function(item) {
            $ngConfirm({
                title: 'Warning',
                content: 'Are you sure?',
                scope: $scope,
                buttons: {
                    submit: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            $http.post('api/device/restore', {id: item.id})
                                .then(function (response) {
                                    var curIndex = $scope.devices.indexOf(item);
                                    $scope.devices.splice(curIndex, 1);
                                }, function (response) {
                                    console.log(response.data || 'Request failed');
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        /**
         * Export to PDF
         **/
        $scope.exportToPdf = function () {


            $http.post('api/device/exportPdf', {}, {
                responseType: 'arraybuffer',
                headers: {
                    accept: 'application/pdf'
                }
            })
                .then(function (response) {
                    var file = new Blob([response.data], {type: 'application/pdf'});
                    var fileURL = window.URL.createObjectURL(file);

                    //window.open(fileURL);
                    //Create anchor
                    var a = window.document.createElement('a');
                    a.href = fileURL;
                    a.download = 'Device.pdf';
                    document.body.appendChild(a);
                    a.click();

                    // Remove anchor from body
                    document.body.removeChild(a);

                }, function (response) {
                    console.log(response.data || 'Request failed');
                });

        };


        /**
         * View Device in modal
         **/
        $scope.viewDevice = function (item,size, parentSelector) {

            var keepGoing = true;
            angular.forEach(item.device_packages, function (value,key) {
                if (keepGoing) {
                    if (value.status == 1) {
                        item['active_package'] = value;
                        keepGoing = false;
                    }
                }
            });

            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'template/viewDevice',
                controller: 'UibDeviceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return item;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.importfile = {};
        $scope.importExcel = function () {

            $scope.fileError = false;
            if ($scope.importfile.type != 'application/vnd.ms-excel') {
                //$scope.fileError = true;
                $scope.importfile = {};
                $ngConfirm('Please select valid file.');
            }else if (Object.keys($scope.importfile).length > 0) {
                $http.post('api/device/import', $scope.importfile)
                    .then(function (response) {

                        var html = '<div><h4>Imported</h4></div>';
                        if(response.data.status == 0) {
                            html += '<p> Following data not imported</p>';
                            html += '<ul>';
                            if (response.data.msg.length > 5) {
                                for(var i=0; i <= 5; i++) {
                                    html += '<li>' + response.data.msg[i] + '</li>';
                                }
                            }else{
                                angular.forEach(response.data.msg, function (value) {
                                    html += '<li>' + value + '</li>';
                                });
                            }
                            html += '</ul>';
                            html += '<a href="log/download/' + response.data.logfile + '" target="_top">View log</a>';
                        }
                        //$ngConfirm({
                        //    title: 'Encountered an error!',
                        //    content: html,
                        //    type: 'red',
                        //    typeAnimated: true,
                        //    buttons: {
                        //        close: function () {
                        //        }
                        //    }
                        //});
                        ngDialog.open({
                            template: html,
                            plain: true,
                            className: 'ngdialog-theme-default',
                            width: 250
                        });
                        loadDevice();
                        $scope.fileError = false;
                        $scope.importfile = {};
                    }, function (response) {
                        console.log(response.data || 'Request failed');
                        $scope.importfile = {};
                    });
            }else{
                ngDialog.open({
                    template: '<div>Select A file to import</div>',
                    plain: true,
                    className: 'ngdialog-theme-default',
                    width: 250
                });
            }
        };


        $scope.clickToOpen = function () {
            console.log('dd');
        };
 }]);

