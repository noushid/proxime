/**
 * Created by psybo-03 on 4/11/17.
 */
app.controller('UibDeviceCtrl', ['$uibModalInstance', 'items','$scope','$http','$rootScope', function ($uibModalInstance, items,$scope,$http,$rootScope) {

    $scope.pielabels = ["Active Devices", "Offine Devices", "Charge less"];
    $scope.piedata = [300, 500, 100];
    $scope.items = items;

    console.log(items);
    $http.post($rootScope.apiUrl, {deviceId: items.deviceId})
        .then(function (response) {
            console.log(response.data);
            $scope.onlineDevice = response.data;
        }, function (response) {
            console.log('error');
        });

    $scope.ok = function () {
        $uibModalInstance.close($scope.items);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
