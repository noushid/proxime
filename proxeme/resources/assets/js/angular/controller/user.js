/**
 * Created by psybo-03 on 24/10/17.
 */

app.controller('UserController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'User', '$ngConfirm', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, User, $ngConfirm) {
        $scope.orderProperty = true;
        $rootScope.orderProp = 'name';
        $scope.title = 'User';
        $rootScope.currentRoute = $location.path();

        loadUser();

        $scope.loadUsers = function () {
            $scope.trashed = false;
            $scope.title = 'Users';
            loadUser();
        };


        /**
         * Load All user
         * */
        function loadUser() {
            User.query({'type': 'public'}, function (user) {
                $scope.users = user;
                console.log(user);
            });
        }

        /**
         * Method for New User and show the form.
         *
         * */
        $scope.newUser = function () {
            $scope.validation = {};
            $scope.showform = true;
            $scope.newuser = {};
            $scope.validationErrors = {};
        };

        /**
         * Method for Edit User and show the form.
         * Assign user values to form
         * */
        $scope.editUser = function (item) {
            $scope.newuser = angular.copy(item);
            $scope.curuser = item;
            $scope.showform = true;
            $scope.edit = true;
            $scope.validationErrors = {};
            $window.scrollTo(0, 0);
        };


        /**
         * Cancel The form Or reset form
         * */

        $scope.cancel = function () {
            $scope.showform = false;
            $scope.newuser = {};
        };

        /**
         * form post method
         * */
        $scope.addUser = function () {
            /**
             * Check if user or new user
             * Already user edit their data
             * else add new user
             *
             * */

            if ($scope.newuser.id) {
                console.log($scope.newuser.reset_password);
                if ($scope.newuser.reset_password == true) {
                    $scope.newuser.password = randomPassword(5);
                }
                /* User.update($scope.newuser, function (response) {
                 var curIndex = $scope.users.indexOf($scope.curuser);
                 $scope.users[curIndex] = response;
                 $scope.showform = false;
                 $scope.newuser = {};
                 $scope.edit = false;
                 });*/
                User.update($scope.newuser).
                    $promise.then(function (response) {
                        var curIndex = $scope.users.indexOf($scope.curuser);
                        $scope.users[curIndex] = response;
                        $scope.showform = false;
                        $scope.newuser = {};
                        $scope.edit = false;
                    }, function (response) {
                        console.log(response.data);
                        $scope.validationErrors = response.data;
                    });
            } else {
                $scope.newuser.password = 'admin';
                $scope.newuser.type = 'public';
                User.save($scope.newuser).
                    $promise.then(function (response) {
                        $scope.users.push(response);
                        $scope.showform = false;
                        $scope.newuser = {};
                        $scope.edit = false;
                    }, function (response) {
                        console.log(response.data);
                        $scope.validationErrors = response.data;
                    });

            }
        };

        /**
         * Delete User
         *
         * */
        $scope.deleteUser = function (item) {
            $ngConfirm({
                title: 'Warning!',
                content: 'Would you like to delete this item?',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete(function (response) {
                                var curIndex = $scope.users.indexOf(item);
                                console.log(curIndex);
                                $scope.users.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        /**
         * Enable User
         *
         * */
        $scope.enableUser = function (item) {
            $ngConfirm({
                title: 'Warning!',
                content: 'Are you sure?',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.status = 1;
                            User.update(item, function (response) {
                                var curIndex = $scope.users.indexOf($scope.curuser);
                                $scope.users[curIndex] = response;
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        /**
         * Disable User
         *
         * */
        $scope.disableUser = function (item) {

            $ngConfirm({
                title: 'Warning!',
                content: 'Do you want to Disable this User?',
                scope: $scope,
                type: 'red',
                buttons: {
                    buttons: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.status = 0;
                            User.update(item, function (response) {
                                var curIndex = $scope.users.indexOf($scope.curuser);
                                $scope.users[curIndex] = response;
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        /**
         * To check username is Unique
         * @param json
         * @return boolean
         * */
        $scope.checkUsername = function (value) {
            $http.post('api/usernameCheck', {username: value}, {ignoreLoadingBar: true})
                .then(function onSuccess(response) {
                    if (response.data == 1) {
                        $scope.validation.username = true;
                    } else {
                        $scope.validation.username = false;
                    }
                },
                function onError(response) {
                    $scope.validation.username = false;
                }
            );
        };


        /**
         * To check Email is Unique
         * @param json
         * @return boolean
         * */
        $scope.checkEmail = function (value) {
            $http.post('api/emailCheck', {email: value}, {ignoreLoadingBar: true})
                .then(function onSuccess(response) {
                    if (response.data == 1) {
                        $scope.validation.email = true;
                    } else {
                        $scope.validation.email = false;
                    }
                },
                function onError(response) {
                    $scope.validation.email = false;
                }
            );
        };

        $scope.animationsEnabled = true;

        $scope.viewUser = function (item, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'template/viewUser',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return item;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.viewUserDevice = function (item, size, parentSelector) {
            var data = '';
            angular.forEach(item.device, function (value) {
                data += value.deviceId + ',';
            });

            data = data.slice(0, -1);

            $http.post('api/virtualDevice', {deviceId: data})
                .then(function success(response) {
                    var devices = item.device;
                    angular.forEach(devices, function (value, key1) {
                        angular.forEach(value, function (v, k) {
                            devices[key1]['from'] = 'db';
                        });
                        angular.forEach(response.data, function (value1, key1) {
                            if (value.deviceId == value1.deviceId) {
                                angular.forEach(value1, function (value3, key3) {
                                    devices[key1][key3] = value3;
                                    devices[key1]['from'] = 'api';
                                });
                            }
                        })
                    });
                    devicesApi = [];
                    devicesDb = [];
                    angular.forEach(devices, function (value, key) {
                        if (value.from == 'db') {
                            devicesDb.push(value);
                        } else {
                            devicesApi.push(value);
                        }
                    });
                    item.allDevices = devices;
                    item.deviceApi = devicesApi;
                    item.deviceDb = devicesDb;
                }, function error(response) {
                    console.log('error');
                });


            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'template/viewUserDevice',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return item;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


        /**
         * Get Trashed Record
         *
         **/
        $scope.trash = function () {
            $scope.title = 'Trashed Users';
            $scope.trashed = true;
            User.query({item: 'trash', type: 'public'}, function (user) {
                $scope.users = user;
            });
        };

        /**
         * Permanent Delete
         **/
        $scope.deleteTrash = function (item) {
            $ngConfirm({
                title: 'Alert',
                content: '<b>Are you sure?</b> This action cannot be undone.',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete({delete: 'permanent'}, function (response) {
                                var curIndex = $scope.users.indexOf(item);
                                $scope.users.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        $scope.restore = function (item) {
            $ngConfirm({
                title: 'Warning',
                content: 'Are you sure?',
                scope: $scope,
                buttons: {
                    submit: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            $http.post('api/user/restore', {id: item.id})
                                .then(function (response) {
                                    var curIndex = $scope.users.indexOf(item);
                                    $scope.users.splice(curIndex, 1);
                                }, function (response) {
                                    console.log(response.data || 'Request failed');
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

    }]);

