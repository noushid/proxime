/**
 * Created by psybo-03 on 24/10/17.
 */

app.controller('UserProfileController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'User', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, User) {
        $scope.validationErrors = [];

        loadUser();

        /***Load Current User *******/
        function loadUser() {
            $http.get('api/getCurrentUser')
                .then(function success(response) {
                    $rootScope.user = response.data;
                },
                function error() {
                    console.log('current user get erorr');
                }
            )
        }

        /**
         * form post method
         * */
        $scope.editUser = function () {
            /**
             * Check if user or new user
             * Already user edit their data
             * */

                //User.update($rootScope.user, function (response) {
                //    $scope.$apply($location.path('/'));
                //});
            $scope.validationErrors = [];
            User.update($rootScope.user).
                $promise.then(function (response) {
                    $scope.$apply($location.path('/'));
                }, function (response) {
                    console.log(response.data);
                    $scope.validationErrors = response.data;
                });

        };

        /**
         * To check username is Unique
         * @param json
         * @return boolean
         * */
        //$scope.checkUsername = function (value) {
        //    $http.post('api/usernameCheck', {username: value})
        //        .then(function onSuccess(response) {
        //            if (response.data == 1) {
        //                $scope.validation.username = true;
        //            }else{
        //                $scope.validation.username = false;
        //            }
        //        },
        //        function onError(response) {
        //            $scope.validation.username = false;
        //        }
        //    );
        //};


        /**
         * To check Email is Unique
         * @param json
         * @return boolean
         * */
            //$scope.checkEmail = function (value) {
            //    $http.post('api/emailCheck', {email: value})
            //        .then(function onSuccess(response) {
            //            if (response.data == 1) {
            //                $scope.validation.email = true;
            //            }else{
            //                $scope.validation.email = false;
            //            }
            //        },
            //        function onError(response) {
            //            $scope.validation.email = false;
            //        }
            //    );
            //};
        $scope.match = true;
        $scope.checkPassword=function() {
            $scope.match = ($scope.user.password != $scope.user.confirmPassword ? false : true);
        };

    }]);

