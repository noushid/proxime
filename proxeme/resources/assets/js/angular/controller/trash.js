/**
 * Created by psybo-03 on 30/10/17.
 */


app.controller('TrashController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'Device','Carrier','Subscription','User', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, Device, Carrier,Subscription,$user) {

        $scope.trash = [];
        $scope.newdevice = {};
        $scope.showform = false;
        $rootScope.currentRoute = $location.path();

        loadDevice();
        loadCarrier();
        loadSubscription();
        //loadUser();

        /**
         * Load All user
         * */
        function loadDevice() {
            Device.query(function (device) {
                angular.forEach(device, function (value) {
                    $scope.trash.push(value);
                });
            });
        }


        /**
         * Load carrier
         * */

        function loadCarrier() {
            Carrier.query(function(carrier) {
                angular.forEach(carrier, function (value) {
                    $scope.trash.push(value);
                });
            })
        }

        function loadSubscription() {
            Subscription.query(function(carrier) {
                angular.forEach(carrier, function (value) {
                    $scope.trash.push(value);
                });
            })
        }

        //function loadUser() {
        //    User.query(function(user) {
        //        angular.forEach(user, function (value) {
        //            $scope.trash.push(value);
        //        });
        //    })
        //}

        /**
         * Method for New User and show the form.
         *
         * */
        $scope.newDevice = function () {
            $scope.showform = true;
            $scope.newdevice = {};
        };

        /**
         * Method for Edit User and show the form.
         * Assign user values to form
         * */
        $scope.editDevice = function (item) {
            $scope.newdevice = angular.copy(item);
            $scope.curdevice = item;
            $scope.showform = true;
        };


        /**
         * Cancel The form Or reset form
         * */
        $scope.cancel = function () {
            $scope.showform = false;
            $scope.newdevice = {};
        };


        /**
         * form post method
         * */
        $scope.addDevice = function () {
            /**
             * Check if user or new user
             * Already user edit their data
             * else add new user
             *
             * */
            if ($scope.newdevice.id) {
                Device.update($scope.newdevice, function (response) {
                    loadDevice();
                    var curIndex = $scope.devices.indexOf($scope.curdevice);
                    console.log(curIndex);
                    $scope.devices[curIndex] = response;
                    $scope.showform = false;
                    $scope.newdevice = {};
                });
            } else {
                $scope.newdevice.password = 'admin';
                $scope.newdevice.type = 'Device';
                Device.save($scope.newdevice, function (response) {
                    $scope.devices.push(response);
                    $scope.showform = false;
                    $scope.newdevice = {};
                });
            }
        };

        /**
         * Delete Device
         *
         * */
        $scope.deleteDevice = function (item) {
            item.$delete(function (response) {
                var curIndex = $scope.devices.indexOf(item);
                console.log(curIndex);
                $scope.devices.splice(curIndex, 1);
            });
        };

    }]);

