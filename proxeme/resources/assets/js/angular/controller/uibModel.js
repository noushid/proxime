/**
 * Created by psybo-03 on 24/10/17.
 */
app.controller('ModalInstanceCtrl', function ($uibModalInstance, items,$scope) {

    $scope.pielabels = ["Active Devices", "Offine Devices", "Charge less"];
    $scope.piedata = [300, 500, 100];

    $scope.items = items;
    $scope.ok = function () {
        $uibModalInstance.close($scope.items);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
