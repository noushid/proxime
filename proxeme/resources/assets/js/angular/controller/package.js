/**
 * Created by psybo-03 on 24/10/17.
 */


app.controller('PackagesController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'Subscription', '$ngConfirm', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, Subscription, $ngConfirm) {

        $scope.packages = [];
        $scope.newpackage = {};
        $scope.showform = false;
        $rootScope.currentRoute = $location.path();

        $rootScope.orderProp = '';
        $scope.title = 'Package';

        $scope.loadPackages = function () {
            $scope.trashed = false;
            $scope.title = 'Package';
            loadPackage();
        };

        /**
         * Load All user
         * */
        function loadPackage() {
            Subscription.query(function (item) {
                $scope.packages = item;
            });
        }

        loadPackage();

        /**
         * Method for New User and show the form.
         *
         * */
        $scope.newPackage = function () {
            $scope.showform = true;
            $scope.newpackage = {};
            $scope.validationErrors = {};
        };

        /**
         * Method for Edit User and show the form.
         * Assign user values to form
         * */
        $scope.editPackage = function (item) {
            $scope.newpackage = angular.copy(item);
            $scope.curpackage = item;
            $scope.showform = true;
            $scope.validationErrors = {};
            $window.scrollTo(0, 0);
        };


        /**
         * Cancel The form Or reset form
         * */
        $scope.cancel = function () {
            $scope.showform = false;
            $scope.newpackage = {};
        };


        /**
         * form post method
         * */
        $scope.addPackage = function () {
            /**
             * Check if user or new user
             * Already user edit their data
             * else add new user
             *
             * */
            console.log($scope.newpackage);
            if ($scope.newpackage.id) {
                Subscription.update($scope.newpackage).
                    $promise.then(function (response) {
                        loadPackage();
                        var curIndex = $scope.packages.indexOf($scope.curpackage);
                        console.log(curIndex);
                        $scope.packages[curIndex] = response;
                        $scope.showform = false;
                        $scope.newpackage = {};
                    }, function (response) {
                        $scope.validationErrors = response.data;
                    });
            } else {
                $scope.newpackage.password = 'admin';
                $scope.newpackage.type = 'Package';
                Subscription.save($scope.newpackage).
                    $promise.then(function (response) {
                        $scope.packages.push(response);
                        $scope.showform = false;
                        $scope.newpackage = {};
                    }, function (response) {
                        $scope.validationErrors = response.data;
                    });
            }
        };

        /**
         * Delete Package
         *
         * */
        $scope.deletePackage = function (item) {
            $ngConfirm({
                title: 'Warning!',
                content: 'Would you like to delete this item?',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete(function (response) {
                                var curIndex = $scope.packages.indexOf(item);
                                $scope.packages.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });
        };

        /**
         * Get Trashed Record
         *
         **/
        $scope.trash = function () {
            $scope.title = 'Trashed Packages';
            $scope.trashed = true;
            Subscription.query({item: 'trash'}, function (subscription) {
                $scope.packages = subscription;
            });
        };

        /**
         * Permanent Delete
         **/
        $scope.deleteTrash = function (item) {
            $ngConfirm({
                title: 'Alert',
                content: '<b>Are you sure?</b> This action cannot be undone.',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete({delete: 'permanent'}, function (response) {
                                var curIndex = $scope.packages.indexOf(item);
                                $scope.packages.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        $scope.restore = function (item) {
            $ngConfirm({
                title: 'Warning',
                content: 'Are you sure?',
                scope: $scope,
                buttons: {
                    submit: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            $http.post('api/package/restore', {id: item.id})
                                .then(function (response) {
                                    var curIndex = $scope.packages.indexOf(item);
                                    $scope.packages.splice(curIndex, 1);
                                }, function (response) {
                                    console.log(response.data || 'Request failed');
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        $scope.exportPdf = function () {
            $http.post('api/package/exportPdf', {}, {
                responseType: 'arraybuffer',
                headers: {
                    accept: 'application/pdf'
                }
            })
                .then(function (response) {
                    var file = new Blob([response.data], {type: 'application/pdf'});
                    var fileURL = window.URL.createObjectURL(file);

                    //window.open(fileURL);
                    //Create anchor
                    var a = window.document.createElement('a');
                    a.href = fileURL;
                    a.download = 'Packages.pdf';
                    document.body.appendChild(a);
                    a.click();

                    // Remove anchor from body
                    document.body.removeChild(a);

                }, function (response) {
                    console.log(response.data || 'Request failed');
                });
        };
    }]);
