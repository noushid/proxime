/**
 * Created by psybo-03 on 19/10/17.
 */


app.controller('SubAdminController',
    ['$scope', '$location', '$http', '$rootScope', '$filter', '$window', 'uibDateParser', '$uibModal', '$log', '$document', 'User', '$ngConfirm', 'randomString', function ($scope, $location, $http, $rootScope, $filter, $window, uibDateParser, $uibModal, $log, $document, User, $ngConfirm, randomString) {
        $scope.users = [];
        $scope.newuser = {};
        $scope.showform = false;
        $rootScope.currentRoute = $location.path();

        $scope.title = 'Sub Admin';

        $scope.loadUsers = function () {
            $scope.trashed = false;
            $scope.title = 'Device';
            loadUser();
        };

        /**
         * Load All user
         * */
        function loadUser() {
            User.query({'type': 'subAdmin'}, function (user) {
                $scope.users = user;
            });
        }

        loadUser();


        /**
         * Method for New User and show the form.
         *
         * */
        $scope.newUser = function () {
            $scope.validation = {};
            $scope.showform = true;
            $scope.newuser = {};
            $scope.validationErrors = {};
            $scope.edit = false;
        };

        /**
         * Method for Edit User and show the form.
         * Assign user values to form
         * */
        $scope.editUser = function (item) {
            $scope.newuser = angular.copy(item);
            $scope.curuser = item;
            $scope.showform = true;
            $scope.edit = true;
            $scope.validationErrors = {};
            $window.scrollTo(0, 0);
        };


        /**
         * Cancel The form Or reset form
         * */

        $scope.cancel = function () {
            $scope.showform = false;
            $scope.newuser = {};
            $scope.edit = false;
        };

        /**
         * form post method
         * */
        $scope.addUser = function () {
            /**
             * Check if user or new user
             * Already user edit their data
             * else add new user
             *
             * */

            console.log($scope.newuser);
            console.log($scope.newuser.phone);


            if ($scope.newuser.id) {
                if ($scope.newuser.password == "") {
                    delete $scope.newuser.password;
                }
                //User.update($scope.newuser, function (response) {
                //    var curIndex = $scope.users.indexOf($scope.curuser);
                //    $scope.users[curIndex] = response;
                //    $scope.showform = false;
                //    $scope.newuser = {};
                //    $scope.edit = false;
                //});

                $scope.validationErrors = [];
                User.update($scope.newuser).
                    $promise.then(function (response) {
                        var curIndex = $scope.users.indexOf($scope.curuser);
                        $scope.users[curIndex] = response;
                        $scope.showform = false;
                        $scope.newuser = {};
                        $scope.edit = false;
                    }, function (response) {
                        console.log(response.data);
                        $scope.validationErrors = response.data;
                    });
            } else {
                //$scope.newuser.password = 'admin';
                $scope.newuser.type = 'subAdmin';
                User.save($scope.newuser).
                    $promise.then(function (response) {
                        $scope.users.push(response);
                        $scope.showform = false;
                        $scope.newuser = {};
                        $scope.edit = false;
                    }, function (response) {
                        console.log(response.data);
                        $scope.validationErrors = response.data;
                    });
            }
        };

        /**
         * Delete User
         *
         * */
        $scope.deleteUser = function (item) {
            $ngConfirm({
                title: 'Warning!',
                content: 'Would you like to delete this item?',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete(function (response) {
                                var curIndex = $scope.users.indexOf(item);
                                $scope.users.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

        /**
         * Enable User
         *
         * */
        $scope.enableUser = function (item) {
            item.status = 1;
            item.action = 'enable';
            item.password = 'admin';
            User.update(item, function (response) {
                var curIndex = $scope.users.indexOf($scope.curuser);
                $scope.users[curIndex] = response;
            });
        };


        /**
         * Disable User
         *
         * */
        $scope.disableUser = function (item) {
            item.status = 0;
            item.action = 'disable';
            User.update(item, function (response) {
                var curIndex = $scope.users.indexOf($scope.curuser);
                $scope.users[curIndex] = response;
            });
        };

        /**
         * To check username is Unique
         * @param json
         * @return boolean
         * */
        $scope.checkUsername = function (value) {
            $http.post('api/usernameCheck', {username: value}, {
                ignoreLoadingBar: true
            })
                .then(function onSuccess(response) {
                    if (response.data == 1) {
                        $scope.validation.username = true;
                    } else {
                        $scope.validation.username = false;
                    }
                },
                function onError(response) {
                    $scope.validation.username = false;
                }
            );
        };


        /**
         * To check Email is Unique
         * @param json
         * @return boolean
         * */
        $scope.checkEmail = function (value) {
            $http.post('api/emailCheck', {email: value}, {
                ignoreLoadingBar: true
            })
                .then(function onSuccess(response) {
                    if (response.data == 1) {
                        $scope.validation.email = true;
                    } else {
                        $scope.validation.email = false;
                    }
                },
                function onError(response) {
                    $scope.validation.email = false;
                }
            );
        };


        /**
         * Get Trashed Record
         *
         **/
        $scope.trash = function () {
            $scope.title = 'Trashed User';
            $scope.trashed = true;
            User.query({item: 'trash', type: 'subAdmin'}, function (user) {
                $scope.users = user;
            });
        };

        /**
         * Permanent Delete
         **/
        $scope.deleteTrash = function (item) {
            $ngConfirm({
                title: 'Alert',
                content: '<b>Are you sure?</b> This action cannot be undone.',
                scope: $scope,
                buttons: {
                    sayBoo: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            item.$delete({delete: 'permanent'}, function (response) {
                                var curIndex = $scope.users.indexOf(item);
                                $scope.users.splice(curIndex, 1);
                            });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };
        /**
         * Restore trashed data
         **/
        $scope.restore = function (item) {
            $ngConfirm({
                title: 'Warning',
                content: 'Are you sure?',
                scope: $scope,
                buttons: {
                    submit: {
                        text: 'Yes',
                        btnClass: 'btn-blue',
                        action: function (scope, button) {
                            $http.post('api/user/restore', {id: item.id})
                                .then(function (response) {
                                    var curIndex = $scope.users.indexOf(item);
                                    $scope.users.splice(curIndex, 1);
                                }, function (response) {
                                    console.log(response.data || 'Request failed');
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });
        };


        /**
         * Create random password
         **/
        $scope.createPassword = function () {
            return randomString(6);
        };

    }]);
