<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body style="font-family: 'Nunito', sans-serif;margin: 50px;">
	<div style="    border: 2px solid #ef8629;    min-height: 400px;    border-radius: 5px 5px 0 0;width: 100%">
		<img src="http://proxime.surebot.co/img/logos/logo-2.png" alt="Proxime Logo" 
		style="    text-align: center;    width: 100px;    height: auto;    display: block;    margin: auto;    padding-bottom: 25px; margin-top: 30px;">
		<div style="    width: 90%;    display: table;    margin: 0 auto;">
			<p style="    font-size: 19px;    text-transform: capitalize;">Hi, {{$user}}</p>
			<p style=" font-size: 20px; margin-left: 40px;">Your E-mail id: 
				<span style="letter-spacing: 3px;    margin-left: 15px;">{{$email}}</span>
			</p>
			<p style=" font-size: 20px; margin-bottom: 70px; margin-left: 40px;">Click to verify
				<span style="letter-spacing: 3px;margin-left: 15px;text-decoration: underline;cursor: pointer;color: #7474e0;"><a href="{{$verification_link}}">Verify</a></span>
			</p>
			<p>If you are having any issues with your account, please don't hesitate to contact us by replaying to this mail.</p>
			<div>
				<p>Thanks!</p>
				<a href="#" style="text-decoration: none;color: #000;font-weight: 700;">www.ProxiMe.com</a>
			</div>
		</div>
	</div>	
	<div style="  border: 2px solid #ef8629;  height: 60px;    width: 100%;    background-color: #ef8629; border-radius: 0 0 5px 5px;width: 100%">
		<p style="    margin-top: 0px;    line-height: 60px;    text-align: center;    color: #fff;    font-weight: 700;">&copy; ProxiME <span id="year"></span>. All Rights Reserved.</p>
	</div>

<!-- year scrpt -->
<script type="text/javascript">
    n =  new Date();
    y = n.getFullYear();
    document.getElementById("year").innerHTML = y;
</script>
</body>
</html>