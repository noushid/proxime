<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body style="font-family: 'Nunito', sans-serif;margin: 50px">
<div style="    border: 2px solid #ef8629;    min-height: 400px;    border-radius: 5px 5px 0 0; width: 100%;">
    <img src="http://proxime.surebot.co/img/logos/logo-2.png" alt="Proxime Logo"
         style="    text-align: center;    width: 100px;    height: auto;    display: block;    margin: auto;    padding-bottom: 25px; margin-top: 30px;">
    <div style="    width: 90%;    display: table;    margin: 0 auto;">
        <div style="margin-left: 15px;">
            <p>Dear <b>{{$name}}</b> . Your account has been Dismissed by the Administrator.</p>
        </div>
        <p>If you are having any Query, please don't hesitate to contact us <a href="mailto:{{ env('MAIL_SUPPORT_EMAIL') }}">{{ env('MAIL_SUPPORT_EMAIL') }}</a>.</p>
        <div>
            <p>Thanks!</p>
            <a href="#" style="text-decoration: none;color: #000;font-weight: 700;">ProxiME Team</a>
        </div>
    </div>
</div>
<div style=" border: 2px solid #ef8629;   height: 60px;    width: 100%;    background-color: #ef8629; border-radius: 0 0 5px 5px;width: 100%;">
    <p style="    margin-top: 0px;    line-height: 60px;    text-align: center;    color: #fff;    font-weight: 700;">&copy; ProxiME <span id="year"></span>. All Rights Reserved.</p>
</div>

<!-- year scrpt -->
<script type="text/javascript">
    n =  new Date();
    y = n.getFullYear();
    document.getElementById("year").innerHTML = y;
</script>
</body>
</html>