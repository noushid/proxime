<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
</head>
<body style="font-family: 'Nunito', sans-serif;margin: 50px;">
	<div style="    border: 2px solid #ef8629;    min-height: 400px;    border-radius: 5px 5px 0 0;width: 100%">
		<div style="    width: 90%;    display: table;    margin: 0 auto;">
			<div>
				<p style="width: 15%;display: inline-block;">Name</p>:
				<p style="width: 80%;display: inline-block;">{{$name}}</p>
			</div>
			<div>
				<p style="width: 15%;display: inline-block;">Email Address</p>:
				<p style="width: 80%;display: inline-block;">{{$email}}</p>
			</div>
			<div>
				<p style="width: 15%;display: inline-block;">Phone Number</p>:
				<p style="width: 80%;display: inline-block;">{{$phone}}</p>
			</div>
			<div>
				<p style="width: 15%;display: inline-block;">Mail Subject</p>
				<p style="width: 80%;display: inline-block;">{{$subject}}</p>
			</div>
			<p style="border: 1px solid #cecece;height: 40px;background-color: #f3f3f3;line-height: 40px;border-radius: 3px;padding: 0px 10px;">Message Content</p>
			<p style="    padding: 0px 20px;    margin-bottom: 50px;">{{$content}}</p>
			<div>
				<p>From : </p>
				<a href="#" style="text-decoration: none;color: #000;font-weight: 700;">www.ProxiME.com</a>
			</div>
		</div>
	</div>	
	<div style="border: 2px solid #ef8629;    height: 60px;    width: 100%;    background-color: #ef8629; border-radius: 0 0 5px 5px;width: 100%">
		<p style="    margin-top: 0px;    line-height: 60px;    text-align: center;    color: #fff;    font-weight: 700;">&copy; ProxiME <span id="year"></span>. All Rights Reserved.</p>
	</div>

<!-- year scrpt -->
<script type="text/javascript">
    n =  new Date();
    y = n.getFullYear();
    document.getElementById("year").innerHTML = y;
</script>
</body>
</html>