<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/dropzone.css')}}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('css/skins/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/ie10-viewport-bug-workaround.css')}}">
    <script type="text/javascript" src="{{asset('js/ie-emulation-modes-warning.js')}}"></script>
</head>
<body class="body">
<div class="page_loader"></div>

<!-- Content area start -->
<div class="content-area signup-content" style="padding: 0;background-image: url('img/map.png');">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Account Details</span></h1>
                        </div>
                        <!-- Form start-->
                        <form action="{{route('signup.submit')}}" method="POST" id="signupForm">
                            {{ csrf_field() }}
                            <div id="perDetails">
                                <div class="form-group">
                                    <select name="bank_name" id="bank_name" class="input-text" required="">
                                        <option value="" selected disabled>Select</option>
                                        <option value="sbi">sbi</option>
                                        <option value="sbi">sbi</option>
                                        <option value="sbi">sbi</option>
                                        <option value="sbi">sbi</option>
                                        <option value="sbi">sbi</option>
                                        <option value="sbi">sbi</option>
                                        <option value="sbi">sbi</option>
                                        <option value="sbi">sbi</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" class="input-text" placeholder="Full Name" value="{{ old('name') }}" required="">
                                </div>
                                <div class="form-group {{$errors->first('account_no') ? 'has-error' : ''}}" id="form-account_no">
                                    <input type="text" name="account_no" class="input-text" placeholder="Account No" value="{{ old('account_no') }}" required="" id="email">
                                    <div class="error">{{ $errors->first('account_no') }}</div>
                                </div>
                                <div class="form-group {{$errors->first('phone') ? 'has-error' : ''}}" id="form-phone">
                                    <input type="text" class="input-text" name="phone" id="iphone" required="" placeholder="Phone Number" value="{{ old('phone') }}">
                                    {{--<input type="text" name="phone" class="input-text" placeholder="Phone Number" value="{{ old('phone') }}" required="">--}}
                                    <div class="error">{{ $errors->first('phone') }}</div>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<input type="password" name="password" class="input-text" placeholder="Password" required="">--}}
                                    {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="password" name="confirm_password" class="input-text" placeholder="Confirm Password" required="">--}}
                                    {{--</div>--}}

                                <div class="form-group otp" id="otpSendTo">
                                    <label class="otp-info">Send to OTP via</label>
                                    <!-- Phone<input type="radio" class="" name="sendTo" value="phone" checked/>
                                    Email<input type="radio" class="" name="sendTo" value="email"/> -->
                                    <label class="radio inline">
                                        <input type="radio" class="" name="sendTo" value="phone" checked>
                                        <span> Phone </span>
                                    </label>
                                    <label class="radio inline">
                                        <input type="radio" class="" name="sendTo" value="email">
                                        <span>Email </span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="border-button-sm btn-block" id="otpBtn" onclick="generateOtp('otp')">Generate OTP</button>
                                    <div class="load"></div>
                                </div>
                            </div>
                            <div id="otpForm">
                                <div class="form-group hidden" id="otpIntput">
                                    <input type="number" name="otp" class="input-text" placeholder="OTP" id="otp">
                                    <span class="help-block">OTP send to  <span id="sentTo-inf"></span>. could not receive OTP <a id="resendOtp" onclick="resendOTP()">Click to resend.</a></span>
                                </div>

                                <div class="form-group hidden" id="submitBtn">
                                    <button type="button" class="button-md button-theme btn-block" id="signupBtn">Signup</button>
                                </div>
                            </div>
                        </form>
                        <!-- Form end-->
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- Footer -->
                    <div class="footer">
                        <span>
                            I want to <a href="{{'login'}}">return to login</a>
                        </span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->

<script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-submenu.js')}}"></script>
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet-providers.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet.markercluster.js')}}"></script>
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.filterizr.js')}}"></script>
<script type="text/javascript" src="{{asset('js/maps.js')}}"></script>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.maskedinput.min.js')}}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>
</body>
</html>