@extends('app.layouts.user')
    @section('content')

        <div class="col-lg-8 col-md-8 col-sm-12 yield-content">
            <div class="option-bar">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>
                                <span class="heading-icon">
                                    <i class="fa fa-th-large"></i>
                                </span>
                                <span class="hidden-xs">My Devices</span>
                            </h4>
                        </div>
                        <div class="col-md-6">
                            <div class="sorting-options">
                            <form action="{{route('user.devices')}}" id="deviceOrderForm" method="GET">
                                <select class="sorting" name="sort" id="deviceOrder">
                                    <option value="">--- Select and Sort ---</option>
                                    <option value="active" {{(app('request')->input('sort') == 'active' ? 'selected' : '')}}>Active</option>
                                    <option value="inactive" {{(app('request')->input('sort') == 'inactive' ? 'selected' : '')}}>Inactive</option>
                                    <option value="all" {{(app('request')->input('sort') == 'all' ? 'selected' : '')}}>All</option>
                                    {{--<option value="highToLow">Devices Plan (High To Low)</option>--}}
                                    {{--<option value="lowToHigh">Devices Plan (Low To High)</option>--}}
                                </select>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- option bar end -->
                <div class="clearfix"></div>

                <div class="row">
                     @if($devices)
                        @foreach($devices as $device)
                         <?php $i = 0; ?>
                            <div class="col-md-4 wow fadeInRight delay-0{{$i= $i+2}}s">
                                <div class="agent-box">
                                    <!-- box content -->
                                    <div class="agent-content">
                                        <!-- title -->
                                        <h1 class="title">
                                            <a href="#" id="show{{$device->id}}" onclick="showForm({{$device->id}})">{{$device->name}} <span>Edit Name</span></a>
                                            <div class="nm-ed form-group userform hidden" id="edit{{$device->id}}">
                                                <input type="text" class="input-text" name="name" value="" id="device{{$device->id}}">
                                                <div class="userform-btn">
                                                    <button class="btn submit" onclick="changeDeviceName({{$device->id}})"><i class="fa fa-check"></i></button>
                                                    <button class="btn cancel" onclick="cancel({{$device->id}})"><i class="fa fa-times"></i></button>
                                                </div>
                                                <div id="error{{$device->id}}"></div>
                                            </div>
                                        </h1>
                                        <!-- Contact -->
                                        <div class="contact">
                                            <p>
                                                <a><i class="fa fa-desktop"></i>{{$device->deviceId}}</a>
                                            </p>
                                            <p>
                                                <a><i class="fa fa-map-marker"></i>123 Kathal St. Tampa City,</a>
                                            </p>
                                            <p>
                                                <a><i class="fa fa-credit-card"></i>23 Days Remaining</a>
                                            </p>
                                        </div>
                                        <!-- Sociallist -->
                                        <div class="plan featured mw">
                                            @if($device->current_device_package)
                                                @if($device->current_device_package->package != null and $device->current_device_package->package->featured == 1)
                                                    <div class="listing-badges">
                                                        <span class="featured dvc-tag">Featured</span>
                                                    </div>
                                                @endif
                                                @if($device->current_device_package->status == 1)
                                                    <div class="price-header p15">
                                                        <h3 class="fs15">{{($device->current_device_package->package ? $device->current_device_package->package->name : '')}}</h3>
                                                        <h1 class="fs15">{{($device->current_device_package->package ? $device->current_device_package->package->price : '')}}</h1>
                                                    </div>
                                                @else
                                                    <div class="price-header p15 header-deat">
                                                        <h3 class="fs15">{{($device->current_device_package->package ? $device->current_device_package->package->name : '')}}</h3>
                                                        <h1 class="fs15">{{($device->current_device_package->package ? $device->current_device_package->package->price : '')}}</h1>
                                                    </div>
                                                @endif
                                                <div class="plan-features listomi">
                                                    <ul class="dvc-list">
                                                        <li>15 Projects</li>
                                                        <li>30GB Storage</li>
                                                        <li>Unilimited Data Transfer</li>
                                                        <li>50 GB Bandwith</li>
                                                        <li>Enhanced Security</li>
                                                        <li>Unilimited Data</li>
                                                    </ul>
                                                    <a onclick="openHistory(this)" class="histroy" data-deviceid="{{$device->deviceId}}" >View History</a>
                                                </div>
                                            @else
                                                <div class="price-header p15 header-deat w">
                                                    No Pacakge found
                                                </div>
                                                <div class="plan-features listomi">
                                                    <a href="{{route('device.subscription',['device_id'=>$device->deviceId,'token' => csrf_token()])}}" class="btn button-sm button-theme nofound btn-color">Pick a Package</a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- box end -->
                         @endforeach
                    @endif
                </div>

                <div id="pageMessages"></div>
                {{--<div class="container">--}}
                    {{--<button class="btn btn-danger" onclick="createAlert('Opps!','Something went wrong','Here is a bunch of text about some stuff that happened.','danger',true,false,'pageMessages');">Add Danger Alert</button>--}}
                    {{--<button class="btn btn-success" onclick="createAlert('','Nice Work!','Here is a bunch of text about some stuff that happened.','success',true,true,'pageMessages');">Add Success Alert</button>--}}
                    {{--<button class="btn btn-info" onclick="createAlert('BTDubs','','Here is a bunch of text about some stuff that happened.','info',false,true,'pageMessages');">Add Info Alert</button>--}}
                    {{--<button class="btn btn-warning" onclick="createAlert('','','Here is a bunch of text about some stuff that happened.','warning',false,true,'pageMessages');">Add Warning Alert</button>--}}
                {{--</div>--}}


            </div>
        </div>
<!-- My profile end -->
<div id="hisSidenav" class="historynav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeHistory()">&times;</a>
    <div class="p10" id="history_data"></div>

</div>

@stop


<script>
function openHistory(e) {
    var deviceId=$(e).data('deviceid');
    $.get('package-history/'+deviceId)
            .done(function(response){
                var html = '<p><code>'+deviceId+'</code></p>';
                html +='<table class="table">'+
                      '<thead class="thead-dark">'+
                        '<tr>'+
                          '<th scope="col">#</th>'+
                          '<th scope="col">Package Name</th>'+
                          '<th scope="col">Recharge Date</th>'+
                          '<th scope="col">Start Date</th>'+
                          '<th scope="col">End Date</th>'+
                        '</tr>'+
                      '</thead>'+
                      '<tbody>'+
                      '<tr>';
                      for(var i= 0; i < response.length;i++ ){
                        html += '<td>'+i+'</td>'+
                                '<td>'+response[i].order_no+'</td>'+
                                '<td>'+response[i].recharge_date+'</td>'+
                                '<td>'+response[i].recharge_date+'</td>'+
                                '<td>'+response[i].recharge_date+'</td>'+
                                '</tr>';
                      }
                      html +='</tbody>'+
                         '</html>';
                      $('#history_data').html(html);

            })
            .fail(function(response){
                $('#history_data').html('No History Found');
            });
    document.getElementById("hisSidenav").style.width = "50%";
//    $(this).attr("data-id")
}

function closeHistory() {
    document.getElementById("hisSidenav").style.width = "0";
}

// $('body,html').click(function(e){
//     $('#hisSidenav').style.width = "0";
// });

function changeDeviceName(item){
    $.post('update-device-name/'+item,$('#device'+item).serialize())
        .done(function(response){
            if($('#device' + item).val() != '') {
                $('#show'+item).html($('#device'+item).val());
            }
            $('#edit'+item).addClass('hidden');
            $('#device' + item).val('');
        })
        .fail(function(response){
            $('#error'+item).addClass('error');
            $('#error'+item).html(response.responseJSON['name']);
        });
}

function showForm(item){
    $('#edit'+item).removeClass('hidden');
    $('#error'+item).removeClass('error');
    $('#error'+item).html('');
    $('#device' + item).val('');
}


function cancel(item){
    $('#device' + item).val('');
    $('#edit'+item).addClass('hidden');
}

</script>
