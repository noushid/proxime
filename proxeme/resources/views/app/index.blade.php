<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="css/dropzone.css">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="css/skins/default.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="css/ie10-viewport-bug-workaround.css">
    <script type="text/javascript" src="js/ie-emulation-modes-warning.js"></script>
</head>
<body class="body">
<div class="page_loader"></div>


<!-- Main header start -->
<header class="main-header">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="home" class="logo logo-vis">
                    <img src="img/logos/logo.png" alt="logo">
                </a>
            </div>
            <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                <ul class="nav navbar-nav">
                    <li><a href="#why">Why Choose this</a></li>
                    <li><a href="#pricing">Pricing</a></li>
                    <li><a href="#contact">Contact Us</a></li>
                    <li><a href="#download">Download Now</a></li>
                    <li><a href="login">Login</a></li>
                    @if(auth()->check())
                    <li class="rightside-navbar">
                        <button class="button user-btn" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            <i class="fa fa-user"></i>{{auth()->user()->name}}
                        </button>
                        <ul class="dropdown-menu" id="pani" style="left: -112px;">
                            <li><a href="{{route('user.profile')}}"><i class="fa fa-user"></i>View Profile</a></li>
                            <li><a href="{{route('user.devices')}}"><i class="fa fa-desktop"></i>My Devices</a></li>
                            <li><a href="{{route('user.password')}}"><i class="fa fa-cog"></i>Change Password</a></li>
                            <li>
                                <a onclick="document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i>Log Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                     {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->


<!-- Categories strat -->
<div class="categories">
    <div class="container">
        <div class="row wall-banner wow">
            <div class="col-lg-7 col-md-6">
                <img src="img/logo.png">
                <div class="mh">
                    <h1>Showcase your beautiful app with ProxiME</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                </div>
                <div class="device-download">
                    <a href="#"><i class="fa fa-android"></i></a>
                    <a href="#"><i class="fa fa-apple"></i></a>
                    <a href="#"><i class="fa fa-windows"></i></a>
                </div>

                    <div class="button-group">
                        <a href="#" class="btn button-md border-button-theme">Download Now</a>
                          @if(!auth()->check())
                            <a href="{{route('user.auth')}}" class="btn button-md button-theme">Sign In</a>
                        @else
                            {{--<a href="{{route('user.auth')}}" class="btn button-md button-theme">Logout</a>--}}
                            <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="button button-md button-theme"> Logout</a>
                        @endif
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         {{ csrf_field() }}
                    </form>

            </div>

            <div class="col-lg-5 col-md-5 col-sm-12 col-pad wow fadeInRight delay-04s">
                <div class="mock-group mock-group-1">
                    <img class="front-mock wow  fadeInLeft animated" data-wow-delay="1.5s" data-wow-duration="1.5s" src="img/1-front.png" alt="...">
                    <img class="back-mock wow  fadeInUp animated" data-wow-duration="1.5s" src="img/1-back.png" alt="...">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Categories end-->

<!-- section-4 start -->
<div class="content-area">
    <div class="container wow fadeInUp delay-03s">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <i class="flaticon-people"></i>
                    </div>
                    <div class="detail">
                        <h3>Agent Finder</h3>
                        <p>See who specializes in your area, has the most reviews and the right experience to meet your needs.</p>
                        <div class="clearfix"></div>
                        <a href="#" class="button-sm button-theme">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <i class="flaticon-technology"></i>
                    </div>
                    <div class="detail">
                        <h3>Modern Technology</h3>
                        <p>More than 10,000 customers buy or sell a home with us each year. We help people and homes find each together.</p>
                        <a href="#" class="button-sm button-theme">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="service-item">
                    <div class="icon">
                        <i class="flaticon-technology-1"></i>
                    </div>
                    <div class="detail">
                        <h3>Home Designs Ideas</h3>
                        <p>Our specialists can help you get started on that home project. Find paint colors, that perfect tile and more.</p>
                        <a href="#" class="button-sm button-theme">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- section-4 end -->

<!-- What are you looking for? strat -->
<div id="why" class="our-service our-service-two content-area-2">
    <div class="our-service-inner">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>Why Choos Us?</h1>
            </div>

            <div class="row mrg-btm-30 wow animated" style="visibility: visible;">
                <div class="col-md-4">
                    <div class="col-md-12 wow fadeInLeft delay-04s" style="visibility: visible; animation-name: fadeInLeft;">
                        <div class="content">
                            <i class="flaticon-apartment"></i>
                            <h4>Tons of API Callback</h4>
                            <p>The Internet of Things has the capability to alter the world as we know it; it is the key to rapid change and progress. In fact, will have a greater impact in revolutionizing the world than the internet did!</p>
                        </div>
                    </div>
                    <div class="col-md-12 wow fadeInLeft delay-04s" style="visibility: visible; animation-name: fadeInLeft;">
                        <div class="content">
                            <i class="flaticon-internet"></i>
                            <h4>Responsive Support</h4>
                            <p>The Internet of Things has the capability to alter the world as we know it; it is the key to rapid change and progress. In fact, will have a greater impact in revolutionizing the world than the internet did!</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="img/2.png" alt="...">
                </div>
                <div class="col-md-4">
                    <div class="col-md-12 wow fadeInRight delay-04s" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="content">
                            <i class="flaticon-vehicle"></i>
                            <h4>Rebust Demo Package</h4>
                            <p>The Internet of Things has the capability to alter the world as we know it; it is the key to rapid change and progress. In fact, will have a greater impact in revolutionizing the world than the internet did!</p>
                        </div>
                    </div>
                    <div class="col-md-12 wow fadeInRight delay-04s" style="visibility: visible; animation-name: fadeInRight;">
                        <div class="content">
                            <i class="flaticon-symbol"></i>
                            <h4>Custom Scrollbar</h4>
                            <p>The Internet of Things has the capability to alter the world as we know it; it is the key to rapid change and progress. In fact, will have a greater impact in revolutionizing the world than the internet did!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- What are you looking for? strat -->

<!-- Pricing Tables start -->
<div id="pricing" class="pricing-tables content-area">
    <div class="container">
        <!-- Main title 2 -->
        <div class="main-title">
            <h1>Pricing Tables</h1>
        </div>
        <div class="row">
            <div class="pricing-container margin-top-40">
                @foreach($packages as $package)
                    @if($package->featured == 1 )
                         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-pad wow fadeInUp delay-03s" >
                            <div class="plan featured">
                                <div class="listing-badges">
                                    <span class="featured">Featured</span>
                                </div>

                                <div class="price-header">
                                    <h3>{{$package->name}}</h3>
                                    <h1>&#x20b9;{{$package->price}}</h1>
                                </div>
                                <div class="plan-features">
                                    <ul>
                                        <li>{{$package->duration}} Days</li>
                                        <li>{{$package->usage}}MB Usage</li>
                                        <li>Enhanced Security</li>
                                        <li>Unilimited Data</li>
                                    </ul>
                                    <a href="{{route('user.devices')}}" class="btn button-sm button-theme">Get Started</a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-pad wow fadeInLeft delay-03s" >
                            <div class="plan">
                                <div class="price-header">
                                    <h3>{{$package->name}}</h3>
                                    <h1>&#x20b9;{{$package->price}}</h1>
                                </div>

                                <div class="plan-features">
                                    <ul>
                                        <li>{{$package->duration}} Days</li>
                                        <li>{{$package->usage}}MB Usage</li>
                                        <li>Enhanced Security</li>
                                        <li>Unilimited Data</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    <a href="{{route('user.devices')}}" class="btn button-sm button-theme btn-color">Get Started</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
         {{ $packages->fragment('pricing')->links() }}
    </div>
</div>
<!-- Pricing Tables end -->

<!-- Listings parallax start -->
<div id="download" class="listings-parallax clearfix content-area-2">
    <div class="listings-parallax-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-sm-8 col-xs-12">
                    <h1>Download Now...</h1>
                    <h3>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsa necsagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate.</h3>
                    <div class="button-group text-center mt50">
                        <a href="#" class="btn button-sm dld-btn"><i class="fa fa-apple" aria-hidden="true"></i>App Store</a>
                        <a href="#" class="btn button-sm dld-btn"><i class="fa fa-android" aria-hidden="true"></i>Play Store</a>
                        <a href="#" class="btn button-sm dld-btn"><i class="fa fa-windows" aria-hidden="true"></i>Windows Store</a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-4 col-xs-12">
                    <div class="contect-agent-photo">
                        <img src="img/1-front.png" alt="avatar-6" class="dld-img">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Listings parallax end -->

<!-- Contact body start -->
<div id="contact" class="contact-body content-area">
    <div class="container">
        <div class="main-title">
            <h1>Contact Us</h1>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-8 text-center">
                <!-- Contact form start -->
                <div class="contact-form">
                    <form id="contact_form" action="message/sent" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group fullname">
                                    <input type="text" name="name" class="input-text" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group enter-email">
                                    <input type="email" name="email" class="input-text" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group subject">
                                    <input type="text" name="subject" class="input-text" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group number">
                                    <input type="text" name="phone" class="input-text" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                                <div class="form-group message">
                                    <textarea class="input-text" name="message" placeholder="Write message"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group send-btn">
                                    <button type="submit" class="button-md button-theme">Send Message</button>
                                </div>
                            </div>
                        </div>
                    </form>     
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact body end -->

<!-- Footer start -->
<footer class="main-footer clearfix mrg-top-60">
    <div class="container">
        <!-- Footer top -->
        <div class="footer-top">
            <div class="row text-center">
                <div class="col-md-offset-4 col-md-4 text-center">
                    <div class="logo-2"><img src="img/logo-footer.png" alt="footer-logo"></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->



<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        &copy; ProxiME 2017. All Rights Reserved.
    </div>
</div>
<!-- Copy end right-->

<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-submenu.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.scrollUp.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/leaflet.js"></script>
<script type="text/javascript" src="js/leaflet-providers.js"></script>
<script type="text/javascript" src="js/leaflet.markercluster.js"></script>
<script type="text/javascript" src="js/dropzone.js"></script>
<script type="text/javascript" src="js/jquery.filterizr.js"></script>
{{--<script type="text/javascript" src="js/maps.js"></script>--}}
<script type="text/javascript" src="js/app.js"></script>


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
<!-- FOR PREVENT ZOOMING -->
<script>
    $(document).keydown(function(event) {
    if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
            event.preventDefault();
         }
        // 107 Num Key  +
        // 109 Num Key  -
        // 173 Min Key  hyphen/underscor Hey
        // 61 Plus key  +/= key
    });

    $(window).bind('mousewheel DOMMouseScroll', function (event) {
           if (event.ctrlKey == true) {
           event.preventDefault();
           }
    });
</script>
</body>
</html>