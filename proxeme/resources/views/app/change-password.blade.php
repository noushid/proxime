@extends('app.layouts.user')
    @section('content')
        <div class="col-lg-8 col-md-8 col-sm-7 yield-content">
            <div class="my-address">
                <div class="main-title-2">
                    <h1><span>Change</span> Password</h1>
                </div>
                <form action="{{route('user.password')}}" name="editForm" method="POST">
                  {{ csrf_field() }}
                    <div class="form-group">
                        <label>Current Password</label>
                        <input class="input-text" type="password" name="curPassword" placeholder="Enter Current Password here" required=""/>
                         @if ($errors->has('curPassword'))
                            <div class="error">{{ $errors->first('curPassword') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" class="input-text" name="password" placeholder="Enter New Password here"  required="">
                        @if ($errors->has('password'))
                            <div class="error">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        {{--<input type="password" class="input-text" name="passwordConfirm" placeholder="*****" pattern="^.{6,10}.*$" >--}}
                        <input type="password" class="input-text" name="passwordConfirm" placeholder="Confirm Password" required="">
                        @if ($errors->has('passwordConfirm'))
                            <div class="error">{{ $errors->first('passwordConfirm') }}</div>
                        @endif
                    </div>
                    <input type="hidden" name="user_id" value="{{$user->id}}"/>
                    <button class="btn button-md button-theme float-r">Save Changes</button>
                </form>
            </div>
        </div>
    @stop