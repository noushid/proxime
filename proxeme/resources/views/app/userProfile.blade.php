@extends('app.layouts.user')
@section('content')
    <div class="col-lg-8 col-md-8 col-sm-12 yield-content">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="my-address" id="showProfileSection">
            <div class="main-title-2">
                <h1><span>Your</span> Profile</h1> <a href="#" id="editProfileBtn"></a>
            </div>
            <div class="form-group myform border-bottom">
                <label>Name</label>
                <span class="profil-lst"><span id="showname">{{$user->name}} </span><a onclick="editProfile('name')">Edit</a></span>
            </div>
            <div class="form-group userform  hidden {{$errors->first('name') ? 'has-error' : ''}}" id="editname">
                <label>Name</label>
                <input type="text" class="input-text" name="name" value="{{$user->name}}" required="" id="name">
                <div class="userform-btn">
                    <button class="btn submit" onclick="update('name',this)"><i class="fa fa-check"></i></button>
                    <button class="btn cancel" onclick="cancelProfile('name')"><i class="fa fa-times"></i></button>
                </div>
                <div id="errorname">{{ $errors->first('name') }}</div>
            </div>


            <div class="form-group myform border-bottom">
                <label>Username</label>
                <span class="profil-lst"><span id="showusername">{{$user->username}}</span> {{--<a onclick="editProfile('username')">Edit</a>--}}</span>
            </div>
            <div class="form-group hidden userform  {{$errors->first('username') ? 'has-error' : ''}}" id="editusername">
                <label>username</label>
                <input type="text" class="input-text" name="username" value="" required="" id="username">
                <div class="userform-btn">
                    <button class="btn submit" onclick="update('username',this)"><i class="fa fa-check"></i></button>
                    <button class="btn cancel" onclick="cancelProfile('username')"><i class="fa fa-times"></i></button>
                </div>
                <div class="error" id="errorusername">{{ $errors->first('username') }}</div>
            </div>


            <div class="form-group myform border-bottom">
                <label>Phone</label>
                <span class="profil-lst"><span id="showphone">{{'+'.$user->phone}}</span> <a onclick="editProfile('phone')">Edit</a></span>
            </div>
            <div class="form-group hidden userform  {{$errors->first('phone') ? 'has-error' : ''}}" id="editphone">
                <label>Phone</label>
                <!-- <input type="text" class="input-text wid100" name="phone" value="" required="" id="phone"  onkeyup="validate(this,'{{$user->phone}}')"> -->
                <button type="button" class="btn submit hidden" onclick="reGenOtpPhone('phone')" id="resendOtp" >Didn't Receive OTP?<span>Resend OTP</span></button>

                <input type="text" class="input-text wid100" name="phone" value="{{'+'.$user->phone}}" required="" id="phone"  onkeyup="validate(this,'{{$user->phone}}')">
                <!-- <button type="button" class="btn submit hidden" onclick="reGenOtpPhone('phone')" id="resendOtp" ><i class="fa fa-refresh" aria-hidden="true"></i></button> -->
                
                <div class="userform-btn">
                    <button type="button" onclick="verifyOtpPhone('phone')" id="verifyOTP" class="hidden btn submit"><i class="fa fa-check"></i></button>
                    <button type="button" onclick="cancelProfile('phone')" class="btn cancel"><i class="fa fa-times"></i></button>
                </div>
                <input type="text" class="input-text wid30 hidden" name="otp" value="" placeholder="Enter Your OTP" required="" id="otpphone">
                <div class="userform-btn">
                    {{--<button type="button" class="btn submit" onclick="getOtpPhone('phone')" id="genOTP" disabled><i class="fa fa-paper-plane" aria-hidden="true"></i></button>--}}

                    {{--fffffffffffffffffffffffffffff--}}
                    {{--<a class="vrf" onclick="getOtpPhone('phone')" id="genOTP" disabled>Verify</a>--}}
                    {{--fffffffffffffffffffffffffffff--}}
                     <a class="btn btn-info has-spinner" id="genOTP" onclick="getOtpPhone('phone')">
                        <span class="spinner"><i class="fa fa-refresh fa-spin"></i></span>
                        Verify
                      </a>
                </div>
                <div class="error hidden" id="errorphone">{{ $errors->first('phone') }}</div>
                <div class="otpError">{{ $errors->first('phone') }}</div>
                <div class="success hidden" id="successphone"></div>
            </div>

            <script>
                $("#phone").intlTelInput({
            //            allowDropdown: false,
                      autoHideDialCode: false,
                      autoPlaceholder: "on",
                      dropdownContainer: "body",
            //              excludeCountries: ["us"],
                      formatOnDisplay: false,
                      geoIpLookup: function(callback) {
                         $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                           var countryCode = (resp && resp.country) ? resp.country : "";
                           callback(countryCode);
                         });
                      },
                      initialCountry: "auto",
                      nationalMode: false,
                      placeholderNumberType: "MOBILE",
                      separateDialCode: true,
                      utilsScript: "js/utils.js"
                });
            </script>


            <div class="form-group myform border-bottom">
                <label>Email</label>
                <span class="profil-lst">
                    <span id="showemail">{{$user->email}}</span>
                    <a onclick="editProfile('email')">Edit</a>
                    <span id="showmessage" ></span>
                </span>
            </div>
            <div class="form-group hidden userform  {{$errors->first('email') ? 'has-error' : ''}}" id="editemail">
                <label>Email</label>
                <input type="text" class="input-text" name="email" required="" id="email" value="{{$user->email}}" onkeyup="validateEmail(this,'{{$user->email}}')">
                <div class="userform-btn">
                    <button type="button" id="updateEmail" class="btn submit" disabled><i class="fa fa-check"></i></button>
                    <button type="button" onclick="cancelProfile('email')" class="btn cancel"><i class="fa fa-times"></i></button>
                </div>
                <div class="error" id="erroremail"></div>
            </div>


            <div class="form-group myform border-bottom">
                <label>Address</label>
                <span class="profil-lst"><span id="showaddress">{{$user->address}} </span><a onclick="editProfile('address')">Edit</a></span>
            </div>
            <div class="form-group userform hidden {{$errors->first('address') ? 'has-error' : ''}}" id="editaddress">
                <label>Address</label>
                <input type="text" class="input-text" name="address" required="" id="address" value="{{$user->address}}">
                <div class="userform-btn">
                    <button onclick="update('address',this)" class="btn submit"><i class="fa fa-check"></i></button>
                    <button class="btn cancel" onclick="cancelProfile('address')"><i class="fa fa-times"></i></button>
                </div>
                <div class="error" id="erroraddress">{{ $errors->first('address') }}</div>
            </div>

            <div class="form-group myform border-bottom">
                <label>Country</label>
                <span class="profil-lst"><span id="showcountry">{{$user->country}} </span><a onclick="editProfile('country')">Edit</a></span>
            </div>
            <div class="form-group userform hidden {{$errors->first('country') ? 'has-error' : ''}}" id="editcountry">
                <label>Country</label>
                {{--<input type="text" class="input-text" name="country" required="" id="country">--}}
                <select name="country" id="country" class="input-text">
                    <option value="">Country</option>
                    <option @if($user->country == 'Afganistan') selected @endif value="Afganistan">Afghanistan</option>
                    <option @if($user->country == 'Albania') selected @endif value="Albania">Albania</option>
                    <option @if($user->country == 'Algeria') selected @endif value="Algeria">Algeria</option>
                    <option @if($user->country == 'American Samoa') selected @endif value="American Samoa">American Samoa</option>
                    <option @if($user->country == 'Andorra') selected @endif value="Andorra">Andorra</option>
                    <option @if($user->country == 'Angola') selected @endif value="Angola">Angola</option>
                    <option @if($user->country == 'Anguilla') selected @endif value="Anguilla">Anguilla</option>
                    <option @if($user->country == 'Antigua Barbuda') selected @endif value="Antigua Barbuda">Antigua &amp; Barbuda</option>
                    <option @if($user->country == 'Argentina') selected @endif value="Argentina">Argentina</option>
                    <option @if($user->country == 'Armenia') selected @endif value="Armenia">Armenia</option>
                    <option @if($user->country == 'Aruba') selected @endif value="Aruba">Aruba</option>
                    <option @if($user->country == 'Australia') selected @endif value="Australia">Australia</option>
                    <option @if($user->country == 'Austria') selected @endif value="Austria">Austria</option>
                    <option @if($user->country == 'Azerbaijan') selected @endif value="Azerbaijan">Azerbaijan</option>
                    <option @if($user->country == 'Bahamas') selected @endif value="Bahamas">Bahamas</option>
                    <option @if($user->country == 'Bahrain') selected @endif value="Bahrain">Bahrain</option>
                    <option @if($user->country == 'Bangladesh') selected @endif value="Bangladesh">Bangladesh</option>
                    <option @if($user->country == 'Barbados') selected @endif value="Barbados">Barbados</option>
                    <option @if($user->country == 'Belarus') selected @endif value="Belarus">Belarus</option>
                    <option @if($user->country == 'Belgium') selected @endif value="Belgium">Belgium</option>
                    <option @if($user->country == 'Belize') selected @endif value="Belize">Belize</option>
                    <option @if($user->country == 'Benin') selected @endif value="Benin">Benin</option>
                    <option @if($user->country == 'Bermuda') selected @endif value="Bermuda">Bermuda</option>
                    <option @if($user->country == 'Bhutan') selected @endif value="Bhutan">Bhutan</option>
                    <option @if($user->country == 'Bolivia') selected @endif value="Bolivia">Bolivia</option>
                    <option @if($user->country == 'Bonaire') selected @endif value="Bonaire">Bonaire</option>
                    <option @if($user->country == 'Bosnia Herzegovina') selected @endif value="Bosnia Herzegovina">Bosnia &amp; Herzegovina</option>
                    <option @if($user->country == 'Botswana') selected @endif value="Botswana">Botswana</option>
                    <option @if($user->country == 'Brazil') selected @endif value="Brazil">Brazil</option>
                    <option @if($user->country == 'British Indian Ocean Ter') selected @endif value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                    <option @if($user->country == 'Brunei') selected @endif value="Brunei">Brunei</option>
                    <option @if($user->country == 'Bulgaria') selected @endif value="Bulgaria">Bulgaria</option>
                    <option @if($user->country == 'Burkina Faso') selected @endif value="Burkina Faso">Burkina Faso</option>
                    <option @if($user->country == 'Burundi') selected @endif value="Burundi">Burundi</option>
                    <option @if($user->country == 'Cambodia') selected @endif value="Cambodia">Cambodia</option>
                    <option @if($user->country == 'Cameroon') selected @endif value="Cameroon">Cameroon</option>
                    <option @if($user->country == 'Canada') selected @endif value="Canada">Canada</option>
                    <option @if($user->country == 'Canary Islands') selected @endif value="Canary Islands">Canary Islands</option>
                    <option @if($user->country == 'Cape Verde') selected @endif value="Cape Verde">Cape Verde</option>
                    <option @if($user->country == 'Cayman Islands') selected @endif value="Cayman Islands">Cayman Islands</option>
                    <option @if($user->country == 'Cayman Islands') selected @endif value="Central African Republic">Central African Republic</option>
                    <option @if($user->country == 'Chad') selected @endif value="Chad">Chad</option>
                    <option @if($user->country == 'Channel Islands') selected @endif value="Channel Islands">Channel Islands</option>
                    <option @if($user->country == 'Chile') selected @endif value="Chile">Chile</option>
                    <option @if($user->country == 'China') selected @endif value="China">China</option>
                    <option @if($user->country == 'Christmas Island') selected @endif value="Christmas Island">Christmas Island</option>
                    <option @if($user->country == 'Cocos Island') selected @endif value="Cocos Island">Cocos Island</option>
                    <option @if($user->country == 'Colombia') selected @endif value="Colombia">Colombia</option>
                    <option @if($user->country == 'Comoros') selected @endif value="Comoros">Comoros</option>
                    <option @if($user->country == 'Congo') selected @endif value="Congo">Congo</option>
                    <option @if($user->country == 'Cook Islands') selected @endif value="Cook Islands">Cook Islands</option>
                    <option @if($user->country == 'Costa Rica') selected @endif value="Costa Rica">Costa Rica</option>
                    <option @if($user->country == 'Cote DIvoire') selected @endif value="Cote DIvoire">Cote D'Ivoire</option>
                    <option @if($user->country == 'Croatia') selected @endif value="Croatia">Croatia</option>
                    <option @if($user->country == 'Cuba') selected @endif value="Cuba">Cuba</option>
                    <option @if($user->country == 'Curaco') selected @endif value="Curaco">Curacao</option>
                    <option @if($user->country == 'Cyprus') selected @endif value="Cyprus">Cyprus</option>
                    <option @if($user->country == 'Czech Republic') selected @endif value="Czech Republic">Czech Republic</option>
                    <option @if($user->country == 'Denmark') selected @endif value="Denmark">Denmark</option>
                    <option @if($user->country == 'Djibouti') selected @endif value="Djibouti">Djibouti</option>
                    <option @if($user->country == 'Dominica') selected @endif value="Dominica">Dominica</option>
                    <option @if($user->country == 'Dominican Republic') selected @endif value="Dominican Republic">Dominican Republic</option>
                    <option @if($user->country == 'East Timor') selected @endif value="East Timor">East Timor</option>
                    <option @if($user->country == 'Ecuador') selected @endif value="Ecuador">Ecuador</option>
                    <option @if($user->country == 'Egypt') selected @endif value="Egypt">Egypt</option>
{{--                    <option @if($user->country == 'El Salvador) selected @endif value="El Salvador">El Salvador</option>--}}
                    <option @if($user->country == 'Equatorial Guinea') selected @endif value="Equatorial Guinea">Equatorial Guinea</option>
                    <option @if($user->country == 'Eritrea') selected @endif value="Eritrea">Eritrea</option>
                    <option @if($user->country == 'Estonia') selected @endif value="Estonia">Estonia</option>
                    <option @if($user->country == 'Ethiopia') selected @endif value="Ethiopia">Ethiopia</option>
                    <option @if($user->country == 'Falkland Islands') selected @endif value="Falkland Islands">Falkland Islands</option>
                    <option @if($user->country == 'Faroe Islands') selected @endif value="Faroe Islands">Faroe Islands</option>
                    <option @if($user->country == 'Fiji') selected @endif value="Fiji">Fiji</option>
                    <option @if($user->country == 'Finland') selected @endif value="Finland">Finland</option>
                    <option @if($user->country == 'France') selected @endif value="France">France</option>
                    <option @if($user->country == 'French Guiana') selected @endif value="French Guiana">French Guiana</option>
                    <option @if($user->country == 'French Polynesia') selected @endif value="French Polynesia">French Polynesia</option>
                    <option @if($user->country == 'French Southern Ter') selected @endif value="French Southern Ter">French Southern Ter</option>
                    <option @if($user->country == 'Gabon') selected @endif value="Gabon">Gabon</option>
                    <option @if($user->country == 'Gambia') selected @endif value="Gambia">Gambia</option>
                    <option @if($user->country == 'Georgia') selected @endif value="Georgia">Georgia</option>
                    <option @if($user->country == 'Germany') selected @endif value="Germany">Germany</option>
                    <option @if($user->country == 'Ghana') selected @endif value="Ghana">Ghana</option>
                    <option @if($user->country == 'Gibraltar') selected @endif value="Gibraltar">Gibraltar</option>
                    <option @if($user->country == 'Great Britain') selected @endif value="Great Britain">Great Britain</option>
                    <option @if($user->country == 'Greece') selected @endif value="Greece">Greece</option>
                    <option @if($user->country == 'Greenland') selected @endif value="Greenland">Greenland</option>
                    <option @if($user->country == 'Grenada') selected @endif value="Grenada">Grenada</option>
                    <option @if($user->country == 'Guadeloupe') selected @endif value="Guadeloupe">Guadeloupe</option>
                    <option @if($user->country == 'Guam') selected @endif value="Guam">Guam</option>
                    <option @if($user->country == 'Guatemala') selected @endif value="Guatemala">Guatemala</option>
                    <option @if($user->country == 'Guinea') selected @endif value="Guinea">Guinea</option>
                    <option @if($user->country == 'Guyana') selected @endif value="Guyana">Guyana</option>
                    <option @if($user->country == 'Haiti') selected @endif value="Haiti">Haiti</option>
                    <option @if($user->country == 'Hawaii') selected @endif value="Hawaii">Hawaii</option>
                    <option @if($user->country == 'Honduras') selected @endif value="Honduras">Honduras</option>
                    <option @if($user->country == 'Hong Kong') selected @endif value="Hong Kong">Hong Kong</option>
                    <option @if($user->country == 'Hungary') selected @endif value="Hungary">Hungary</option>
                    <option @if($user->country == 'Iceland') selected @endif value="Iceland">Iceland</option>
                    <option @if($user->country == 'India') selected @endif value="India">India</option>
                    <option @if($user->country == 'Indonesia') selected @endif value="Indonesia">Indonesia</option>
                    <option @if($user->country == 'Iran') selected @endif value="Iran">Iran</option>
                    <option @if($user->country == 'Iraq') selected @endif value="Iraq">Iraq</option>
                    <option @if($user->country == 'Ireland') selected @endif value="Ireland">Ireland</option>
                    <option @if($user->country == 'Isle of Man') selected @endif value="Isle of Man">Isle of Man</option>
                    <option @if($user->country == 'Israel') selected @endif value="Israel">Israel</option>
                    <option @if($user->country == 'Italy') selected @endif value="Italy">Italy</option>
                    <option @if($user->country == 'Jamaica') selected @endif value="Jamaica">Jamaica</option>
                    <option @if($user->country == 'Japan') selected @endif value="Japan">Japan</option>
                    <option @if($user->country == 'Jordan') selected @endif value="Jordan">Jordan</option>
                    <option @if($user->country == 'Kazakhstan') selected @endif value="Kazakhstan">Kazakhstan</option>
                    <option @if($user->country == 'Kenya') selected @endif value="Kenya">Kenya</option>
                    <option @if($user->country == 'Kiribati') selected @endif value="Kiribati">Kiribati</option>
                    <option @if($user->country == 'Korea North') selected @endif value="Korea North">Korea North</option>
                    <option @if($user->country == 'Korea Sout') selected @endif value="Korea Sout">Korea South</option>
                    <option @if($user->country == 'Kuwait') selected @endif value="Kuwait">Kuwait</option>
                    <option @if($user->country == 'Kyrgyzstan') selected @endif value="Kyrgyzstan">Kyrgyzstan</option>
                    <option @if($user->country == 'Laos') selected @endif value="Laos">Laos</option>
                    <option @if($user->country == 'Latvia') selected @endif value="Latvia">Latvia</option>
                    <option @if($user->country == 'Lebanon') selected @endif value="Lebanon">Lebanon</option>
                    <option @if($user->country == 'Lesotho') selected @endif value="Lesotho">Lesotho</option>
                    <option @if($user->country == 'Liberia') selected @endif value="Liberia">Liberia</option>
                    <option @if($user->country == 'Libya') selected @endif value="Libya">Libya</option>
                    <option @if($user->country == 'Liechtenstein') selected @endif value="Liechtenstein">Liechtenstein</option>
                    <option @if($user->country == 'Lithuania') selected @endif value="Lithuania">Lithuania</option>
                    <option @if($user->country == 'Luxembourg') selected @endif value="Luxembourg">Luxembourg</option>
                    <option @if($user->country == 'Macau') selected @endif value="Macau">Macau</option>
                    <option @if($user->country == 'Macedonia') selected @endif value="Macedonia">Macedonia</option>
                    <option @if($user->country == 'Madagascar') selected @endif value="Madagascar">Madagascar</option>
                    <option @if($user->country == 'Malaysia') selected @endif value="Malaysia">Malaysia</option>
                    <option @if($user->country == 'Malawi') selected @endif value="Malawi">Malawi</option>
                    <option @if($user->country == 'Maldives') selected @endif value="Maldives">Maldives</option>
                    <option @if($user->country == 'Mali') selected @endif value="Mali">Mali</option>
                    <option @if($user->country == 'Malta') selected @endif value="Malta">Malta</option>
                    <option @if($user->country == 'Marshall Islands') selected @endif value="Marshall Islands">Marshall Islands</option>
                    <option @if($user->country == 'Martinique') selected @endif value="Martinique">Martinique</option>
                    <option @if($user->country == 'Mauritania') selected @endif value="Mauritania">Mauritania</option>
                    <option @if($user->country == 'Mauritius') selected @endif value="Mauritius">Mauritius</option>
                    <option @if($user->country == 'Mayotte') selected @endif value="Mayotte">Mayotte</option>
                    <option @if($user->country == 'Mexico') selected @endif value="Mexico">Mexico</option>
                    <option @if($user->country == 'Midway Islands') selected @endif value="Midway Islands">Midway Islands</option>
                    <option @if($user->country == 'Moldova') selected @endif value="Moldova">Moldova</option>
                    <option @if($user->country == 'Monaco') selected @endif value="Monaco">Monaco</option>
                    <option @if($user->country == 'Mongolia') selected @endif value="Mongolia">Mongolia</option>
                    <option @if($user->country == 'Montserrat') selected @endif value="Montserrat">Montserrat</option>
                    <option @if($user->country == 'Morocco') selected @endif value="Morocco">Morocco</option>
                    <option @if($user->country == 'Mozambique') selected @endif value="Mozambique">Mozambique</option>
                    <option @if($user->country == 'Myanmar') selected @endif value="Myanmar">Myanmar</option>
                    <option @if($user->country == 'Nambia') selected @endif value="Nambia">Nambia</option>
                    <option @if($user->country == 'Nauru') selected @endif value="Nauru">Nauru</option>
                    <option @if($user->country == 'Nepal') selected @endif value="Nepal">Nepal</option>
                    <option @if($user->country == 'Netherland Antilles') selected @endif value="Netherland Antilles">Netherland Antilles</option>
                    <option @if($user->country == 'Netherlands') selected @endif value="Netherlands">Netherlands (Holland, Europe)</option>
                    <option @if($user->country == 'Nevis') selected @endif value="Nevis">Nevis</option>
                    <option @if($user->country == 'New Caledonia') selected @endif value="New Caledonia">New Caledonia</option>
                    <option @if($user->country == 'New Zealand') selected @endif value="New Zealand">New Zealand</option>
                    <option @if($user->country == 'Nicaragua') selected @endif value="Nicaragua">Nicaragua</option>
                    <option @if($user->country == 'Niger') selected @endif value="Niger">Niger</option>
                    <option @if($user->country == 'Nigeria') selected @endif value="Nigeria">Nigeria</option>
                    <option @if($user->country == 'Niue') selected @endif value="Niue">Niue</option>
                    <option @if($user->country == 'Norfolk Island') selected @endif value="Norfolk Island">Norfolk Island</option>
                    <option @if($user->country == 'Norway') selected @endif value="Norway">Norway</option>
                    <option @if($user->country == 'Oman') selected @endif value="Oman">Oman</option>
                    <option @if($user->country == 'Pakistan') selected @endif value="Pakistan">Pakistan</option>
                    <option @if($user->country == 'Palau Island') selected @endif value="Palau Island">Palau Island</option>
                    <option @if($user->country == 'Palestine') selected @endif value="Palestine">Palestine</option>
                    <option @if($user->country == 'Panama') selected @endif value="Panama">Panama</option>
                    <option @if($user->country == 'Papua New Guinea') selected @endif value="Papua New Guinea">Papua New Guinea</option>
                    <option @if($user->country == 'Paraguay') selected @endif value="Paraguay">Paraguay</option>
                    <option @if($user->country == 'Peru') selected @endif value="Peru">Peru</option>
                    <option @if($user->country == 'Phillipines') selected @endif value="Phillipines">Philippines</option>
                    <option @if($user->country == 'Pitcairn Island') selected @endif value="Pitcairn Island">Pitcairn Island</option>
                    <option @if($user->country == 'Poland') selected @endif value="Poland">Poland</option>
                    <option @if($user->country == 'Portugal') selected @endif value="Portugal">Portugal</option>
                    <option @if($user->country == 'Puerto Rico') selected @endif value="Puerto Rico">Puerto Rico</option>
                    <option @if($user->country == 'Qatar') selected @endif value="Qatar">Qatar</option>
                    <option @if($user->country == 'Republic of Montenegro') selected @endif value="Republic of Montenegro">Republic of Montenegro</option>
                    <option @if($user->country == 'Republic of Serbia') selected @endif value="Republic of Serbia">Republic of Serbia</option>
                    <option @if($user->country == 'Reunion') selected @endif value="Reunion">Reunion</option>
                    <option @if($user->country == 'Romania') selected @endif value="Romania">Romania</option>
                    <option @if($user->country == 'Russia') selected @endif value="Russia">Russia</option>
                    <option @if($user->country == 'Rwanda') selected @endif value="Rwanda">Rwanda</option>
                    <option @if($user->country == 'St Barthelemy') selected @endif value="St Barthelemy">St Barthelemy</option>
                    <option @if($user->country == 'St Eustatius') selected @endif value="St Eustatius">St Eustatius</option>
                    <option @if($user->country == 'St Helena') selected @endif value="St Helena">St Helena</option>
                    <option @if($user->country == 'St Kitts-Nevis') selected @endif value="St Kitts-Nevis">St Kitts-Nevis</option>
                    <option @if($user->country == 'St Lucia') selected @endif value="St Lucia">St Lucia</option>
                    <option @if($user->country == 'St Maarten') selected @endif value="St Maarten">St Maarten</option>
                    <option @if($user->country == 'St Pierre Miquelon') selected @endif value="St Pierre Miquelon">St Pierre &amp; Miquelon</option>
                    <option @if($user->country == 'St Vincent Grenadines') selected @endif value="St Vincent Grenadines">St Vincent &amp; Grenadines</option>
                    <option @if($user->country == 'Saipan') selected @endif value="Saipan">Saipan</option>
                    <option @if($user->country == 'Samoa') selected @endif value="Samoa">Samoa</option>
                    <option @if($user->country == 'Samoa American') selected @endif value="Samoa American">Samoa American</option>
                    <option @if($user->country == 'San Marino') selected @endif value="San Marino">San Marino</option>
                    <option @if($user->country == 'Sao Tome Principe') selected @endif value="Sao Tome Principe">Sao Tome &amp; Principe</option>
                    <option @if($user->country == 'Saudi Arabia') selected @endif value="Saudi Arabia">Saudi Arabia</option>
                    <option @if($user->country == 'Senegal') selected @endif value="Senegal">Senegal</option>
                    <option @if($user->country == 'Serbia') selected @endif value="Serbia">Serbia</option>
                    <option @if($user->country == 'Seychelles') selected @endif value="Seychelles">Seychelles</option>
                    <option @if($user->country == 'Sierra Leone') selected @endif value="Sierra Leone">Sierra Leone</option>
                    <option @if($user->country == 'Singapore') selected @endif value="Singapore">Singapore</option>
                    <option @if($user->country == 'Slovakia') selected @endif value="Slovakia">Slovakia</option>
                    <option @if($user->country == 'Slovenia') selected @endif value="Slovenia">Slovenia</option>
                    <option @if($user->country == 'Solomon Islands') selected @endif value="Solomon Islands">Solomon Islands</option>
                    <option @if($user->country == 'Somalia') selected @endif value="Somalia">Somalia</option>
                    <option @if($user->country == 'South Africa') selected @endif value="South Africa">South Africa</option>
                    <option @if($user->country == 'Spain') selected @endif value="Spain">Spain</option>
                    <option @if($user->country == 'Sri Lanka') selected @endif value="Sri Lanka">Sri Lanka</option>
                    <option @if($user->country == 'Sudan') selected @endif value="Sudan">Sudan</option>
                    <option @if($user->country == 'Suriname') selected @endif value="Suriname">Suriname</option>
                    <option @if($user->country == 'Swaziland') selected @endif value="Swaziland">Swaziland</option>
                    <option @if($user->country == 'Sweden') selected @endif value="Sweden">Sweden</option>
                    <option @if($user->country == 'Switzerland') selected @endif value="Switzerland">Switzerland</option>
                    <option @if($user->country == 'Syria') selected @endif value="Syria">Syria</option>
                    <option @if($user->country == 'Tahiti') selected @endif value="Tahiti">Tahiti</option>
                    <option @if($user->country == 'Taiwan') selected @endif value="Taiwan">Taiwan</option>
                    <option @if($user->country == 'Tajikistan') selected @endif value="Tajikistan">Tajikistan</option>
                    <option @if($user->country == 'Tanzania') selected @endif value="Tanzania">Tanzania</option>
                    <option @if($user->country == 'Thailand') selected @endif value="Thailand">Thailand</option>
                    <option @if($user->country == 'Togo') selected @endif value="Togo">Togo</option>
                    <option @if($user->country == 'Tokelau') selected @endif value="Tokelau">Tokelau</option>
                    <option @if($user->country == 'Tonga') selected @endif value="Tonga">Tonga</option>
                    <option @if($user->country == 'Trinidad Tobago') selected @endif value="Trinidad Tobago">Trinidad &amp; Tobago</option>
                    <option @if($user->country == 'Tunisia') selected @endif value="Tunisia">Tunisia</option>
                    <option @if($user->country == 'Turkey') selected @endif value="Turkey">Turkey</option>
                    <option @if($user->country == 'Turkmenistan') selected @endif value="Turkmenistan">Turkmenistan</option>
                    <option @if($user->country == 'Turks Caicos Is') selected @endif value="Turks Caicos Is">Turks &amp; Caicos Is</option>
                    <option @if($user->country == 'Tuvalu') selected @endif value="Tuvalu">Tuvalu</option>
                    <option @if($user->country == 'Uganda') selected @endif value="Uganda">Uganda</option>
                    <option @if($user->country == 'Ukraine') selected @endif value="Ukraine">Ukraine</option>
                    <option @if($user->country == 'United Arab Erimates') selected @endif value="United Arab Erimates">United Arab Emirates</option>
                    <option @if($user->country == 'United Kingdom') selected @endif value="United Kingdom">United Kingdom</option>
                    <option @if($user->country == 'United States of America') selected @endif value="United States of America">United States of America</option>
                    <option @if($user->country == 'Uraguay') selected @endif value="Uraguay">Uruguay</option>
                    <option @if($user->country == 'Uzbekistan') selected @endif value="Uzbekistan">Uzbekistan</option>
                    <option @if($user->country == 'Vanuatu') selected @endif value="Vanuatu">Vanuatu</option>
                    <option @if($user->country == 'Vatican City State') selected @endif value="Vatican City State">Vatican City State</option>
                    <option @if($user->country == 'Venezuela') selected @endif value="Venezuela">Venezuela</option>
                    <option @if($user->country == 'Vietnam') selected @endif value="Vietnam">Vietnam</option>
                    <option @if($user->country == 'Virgin Islands (Brit)') selected @endif value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                    <option @if($user->country == 'Virgin Islands (USA)') selected @endif value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                    <option @if($user->country == 'Wake Island') selected @endif value="Wake Island">Wake Island</option>
                    <option @if($user->country == 'Wallis Futana Is') selected @endif value="Wallis Futana Is">Wallis &amp; Futana Is</option>
                    <option @if($user->country == 'Yemen') selected @endif value="Yemen">Yemen</option>
                    <option @if($user->country == 'Zaire') selected @endif value="Zaire">Zaire</option>
                    <option @if($user->country == 'Zambia') selected @endif value="Zambia">Zambia</option>
                    <option @if($user->country == 'Zimbabwe') selected @endif value="Zimbabwe">Zimbabwe</option>
                </select>
                <div class="userform-btn">
                    <button class="btn submit" onclick="update('country',this)"><i class="fa fa-check"></i></button>
                    <button class="btn cancel" onclick="cancelProfile('country')"><i class="fa fa-times"></i></button>
                </div>
                <div class="error" id="errorcountry">{{ $errors->first('country') }}</div>
            </div>
        </div>

        <!-- My address end -->
        {{--@if (count($errors))
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif--}}
    </div>
@stop


<script type="text/javascript">
    
    function myFunction() {
        // document.getElementById("panel").style.display = "block";
         $(".userform").addClass("userform-open");
    }
    function myFunction2() {
         $(".userform").removeClass("userform-open");
    }
</script>
