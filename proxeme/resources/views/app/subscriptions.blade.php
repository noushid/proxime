@extends('app.layouts.user')
    @section('content')
        <div class="col-lg-8 col-md-8 col-sm-12 yield-content">
            <!-- @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if(session()->has('errors'))
                <div class="alert alert-danger">
                    {{ session()->get('errors') }}
                </div>
            @endif
             -->


            <div class="option-bar">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>
                                <span class="heading-icon">
                                    <i class="fa fa-th-large"></i>
                                </span>
                                <span class="hidden-xs">Our Subscriptions</span>
                            </h4>
                        </div>
                        <div class="col-md-6">
                            <div class="sorting-options">
                            <form action="{{route('subscriptions')}}" id="subscriptionOrderForm" method="GET">
                                <select class="sorting" name="sort" id="subscriptionOrder">
                                    <option value="newToOld" {{(app('request')->input('sort') == 'newToOld' ? 'selected' : '')}}>New To Old</option>
                                    <option value="oldToNew" {{(app('request')->input('sort') == 'oldToNew' ? 'selected' : '')}}>Old To New</option>
                                </select>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- option bar end -->
                <div class="clearfix"></div>

                <div class="row">
                    <div class="pricing-container argin-top-40">
                         @if($packages)
                            @foreach($packages as $package)
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-pad wow fadeInLeft delay-03s" >
                                    @if($package->featured ==1)
                                        <div class="plan featured">
                                            <div class="listing-badges">
                                                <span class="featured">Featured</span>
                                            </div>

                                            <div class="price-header p20">
                                                <h3>Bussiness</h3>
                                                <h1>&#8377; {{$package->price}}</h1>
                                            </div>
                                            <div class="plan-features p15">
                                                <ul>
                                                    <li>15 Projects</li>
                                                    <li>30GB Storage</li>
                                                    <li>Unilimited Data Transfer</li>
                                                    <li>50 GB Bandwith</li>
                                                    <li>Enhanced Security</li>
                                                    <li>Unilimited Data</li>
                                                </ul>
                                                <a href="{{(request()->segment(2) ? route('device.package.confirm',['package_id' => $package->id,'device' => request()->segment(2), 'token' => csrf_token()]) : '')}}" class="btn button-sm button-theme">Pick Now</a>
                                            </div>
                                        </div>

                                    @else
                                        <div class="plan">
                                            <div class="price-header p20">
                                                <h3>Standard</h3>
                                                <h1>&#8377; {{$package->price}}</h1>
                                            </div>

                                            <div class="plan-features p15">
                                                <ul>
                                                    <li>15 Projects</li>
                                                    <li>30GB Storage</li>
                                                    <li>Unilimited Data Transfer</li>
                                                    <li>50 GB Bandwith</li>
                                                    <li>Enhanced Security</li>
                                                </ul>
                                                <div class="clearfix"></div>
                                                {{--<a href="submit-property.html" class="btn button-sm button-theme btn-color">Pick</a>--}}
                                                <a href="{{(request()->segment(2) ? route('device.package.confirm',['package_id' => $package->id,'device' => request()->segment(2), 'token' => csrf_token(), 'reference' => request()->get('r')]) : '')}}" class="btn button-sm button-theme">Pick Now</a>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                

                <div id="pageMessages"></div>
                {{--<div class="container">--}}
                    {{--<button class="btn btn-danger" onclick="createAlert('Opps!','Something went wrong','Here is a bunch of text about some stuff that happened.','danger',true,false,'pageMessages');">Add Danger Alert</button>--}}
                    {{--<button class="btn btn-success" onclick="createAlert('','Nice Work!','Here is a bunch of text about some stuff that happened.','success',true,true,'pageMessages');">Add Success Alert</button>--}}
                    {{--<button class="btn btn-info" onclick="createAlert('BTDubs','','Here is a bunch of text about some stuff that happened.','info',false,true,'pageMessages');">Add Info Alert</button>--}}
                    {{--<button class="btn btn-warning" onclick="createAlert('','','Here is a bunch of text about some stuff that happened.','warning',false,true,'pageMessages');">Add Warning Alert</button>--}}
                {{--</div>--}}


            </div>
        </div>
<!-- My profile end -->
<div id="hisSidenav" class="historynav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="pricing-container margin-top-40">

        <div class="col-md-12" >
            <div class="plan">
                <div class="price-header">
                    <h3>Standard</h3>
                    <h1>$19.99</h1>
                </div>
                <div class="plan-features p15">
                    <ul class="mb0">
                        <li>15 Projects</li>
                        <li>30GB Storage</li>
                        <li>Unilimited Data Transfer</li>
                        <li>50 GB Bandwith</li>
                        <li>Enhanced Security</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-12" >
            <div class="plan featured" style="transform: none;">
                <div class="listing-badges">
                    <span class="featured">Featured</span>
                </div>
                <div class="price-header">
                    <h3>Standard</h3>
                    <h1>$19.99</h1>
                </div>
                <div class="plan-features p15">
                    <ul class="mb0">
                        <li>15 Projects</li>
                        <li>30GB Storage</li>
                        <li>Unilimited Data Transfer</li>
                        <li>50 GB Bandwith</li>
                        <li>Enhanced Security</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-12" >
            <div class="plan">
                <div class="price-header">
                    <h3>Standard</h3>
                    <h1>$19.99</h1>
                </div>
                <div class="plan-features p15">
                    <ul class="mb0">
                        <li>15 Projects</li>
                        <li>30GB Storage</li>
                        <li>Unilimited Data Transfer</li>
                        <li>50 GB Bandwith</li>
                        <li>Enhanced Security</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

</div>

@stop


<script>
function openNav() {
    document.getElementById("hisSidenav").style.width = "300px";
}

function closeNav() {
    document.getElementById("hisSidenav").style.width = "0";
}

// $('body,html').click(function(e){
//     $('#hisSidenav').style.width = "0";
// });
</script>
