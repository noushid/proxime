<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME | SignUp</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="css/dropzone.css">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="css/skins/default.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="css/ie10-viewport-bug-workaround.css">

    <!-- Phone number filter -->
    <link rel="stylesheet" type="text/css" href="css/intlTelInput.css">

    <script type="text/javascript" src="js/ie-emulation-modes-warning.js"></script>

</head>
<body class="body">
<div class="page_loader"></div>
<div id="pageMessages"></div>

<!-- Content area start -->
<div class="content-area no-pad signup-content" style="background-image: url('img/map.jpg');background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Signup</span></h1>
                        </div>
                        <!-- Form start-->
                        <form action="{{route('signup.submit')}}" method="POST" id="signupForm">
                            {{ csrf_field() }}
                            <div id="perDetails">
                                <div class="form-group" id="form-name">
                                    <input type="text" name="name" class="input-text" placeholder="Full Name" value="{{ old('name') }}" required="">
                                    {{--<div class="error wid100 margin-0">{{ $errors->first('name') }}</div>--}}
                                </div>
                                <div class="form-group {{$errors->first('email') ? 'has-error' : ''}}" id="form-email">
                                    <input type="email" name="email" class="input-text" placeholder="Email Address" value="{{ old('email') }}" required="" id="email">
                                    {{--<div class="error">{{ $errors->first('email') }}</div>--}}
                                </div>
                                <div class="form-group thenga {{$errors->first('phone') ? 'has-error' : ''}}" id="form-phone">
                                    <input type="text" class="input-text" name="iphone" id="iphone" required="" placeholder="Phone Number" value="{{ old('iphone') }}">
                                    <input type="hidden" class="input-text" name="phone" id="phone" value="">
                                    {{--<input type="text" name="phone" class="input-text" placeholder="Phone Number" value="{{ old('phone') }}" required="">--}}
                                    {{--<div class="error">{{ $errors->first('phone') }}</div>--}}
                                </div>
                                <div class="form-group {{$errors->first('password') ? 'has-error' : ''}}" id="form-password">
                                    <input type="password" name="password" class="input-text" placeholder="Password" required="" >
                                </div>
                                <div class="form-group {{$errors->first('confirm_password') ? 'has-error' : ''}}" id="form-confirm_password">
                                    <input type="password" name="confirm_password" class="input-text" placeholder="Confirm Password" required="" >
                                </div>

                                {{--<div class="form-group otp" id="otpSendTo">--}}
                                    {{--<label class="otp-info">Send to OTP via</label>--}}
                                    {{--<!-- Phone<input type="radio" class="" name="sendTo" value="phone" checked/>--}}
                                    {{--Email<input type="radio" class="" name="sendTo" value="email"/> -->--}}
                                    {{--<label class="radio inline"> --}}
                                        {{--<input type="radio" class="" name="sendTo" value="phone" checked>--}}
                                        {{--<span> Phone </span> --}}
                                    {{--</label>--}}
                                    {{--<label class="radio inline"> --}}
                                        {{--<input type="radio" class="" name="sendTo" value="email">--}}
                                        {{--<span>Email </span> --}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    
                                    <button type="button" class="border-button-sm btn-block" id="otpBtn" onclick="generateOtp('otp')"><div class="load"></div><span id="btname">Submit</span></button>
                                </div>
                            </div>
                            <div id="otpForm">
                                <div class="form-group hidden mb-30" id="otpIntput">
                                    <input type="text" name="otp" class="input-text" placeholder="OTP" id="otp" pattern="[0-9]{1,5}">
                                    <span id="otpError"></span>
                                    {{--<span class="help-block">OTP send to  <span id="sentTo-inf"></span>. <span id="resendOtp" >could not receive OTP <a onclick="resendOTP()">Click to resend.</a></span></span>--}}
                                    <span id="resendOtp" class="crst">could not receive OTP <a onclick="resendOTP()">Click to resend.</a></span></span>
                                </div>

                                <div class="form-group hidden" id="submitBtn">
                                    <button type="button" class="button-md button-theme btn-block" id="signupBtn">Verify</button>
                                </div>
                            </div>
                        </form>
                        <!-- Form end-->
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>
                            Return to <a href="{{'login'}}">Login</a>
                        </span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->

<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-submenu.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.scrollUp.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/leaflet.js"></script>
<script type="text/javascript" src="js/leaflet-providers.js"></script>
<script type="text/javascript" src="js/leaflet.markercluster.js"></script>
<script type="text/javascript" src="js/dropzone.js"></script>
<script type="text/javascript" src="js/jquery.filterizr.js"></script>
<script type="text/javascript" src="js/maps.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
<!-- Phone number filter-->
<script type="text/javascript" src="js/intlTelInput.js"></script>


<script>
    $("#iphone").intlTelInput({
      autoHideDialCode: false,
      autoPlaceholder: "on",
      dropdownContainer: "body",
      formatOnDisplay: false,
      geoIpLookup: function(callback) {
         $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
           var countryCode = (resp && resp.country) ? resp.country : "";
           callback(countryCode);
         });
      },
      initialCountry: "auto",
      nationalMode: false,
      placeholderNumberType: "MOBILE",
      separateDialCode: true,
      utilsScript: "js/utils.js"
    });

    function generateOtp(el){
        var isValid = $("#iphone").intlTelInput("isValidNumber");

        /***Remove existing error if exist*/
        $('.form-group').removeClass('has-error');
        $('.error').remove();


        /*****Validate phone number ****/
        if(isValid == false) {
//            $('#form-phone').addClass('has-error');
//            $('#form-phone').append('<div class="error">Invalid Phone Number</div>');
            $('#iphone').val('');
//            return false;
        }else if(isValid == true){
            /****Append country based phone number to phone number field ****/
//            $("#iphone").val($("#iphone").intlTelInput("getNumber"));
            $("#phone").val($("#iphone").intlTelInput("getNumber"));
        }

        /****Loading****/
        $('.load').addClass('loading');
        $('#otpBtn').prop('disabled', true);
        $('#btname').hide();

//        var email=$('email').val();
//        var sentTo= $('input[name=sendTo]:checked').val();
        var sentTo= 'phone';

        $.post('generateOtp',$('#signupForm').serialize())
            .done(function(response){
                if(sentTo == 'phone'){
                    $('#sentTo-inf').append($('#iphone').val());
                }else{
                    $('#sentTo-inf').append($('#email').val());
                }
                $('.load').removeClass('loading');
                $('#otpBtn').hide();
                $("#resendOtp").hide();
                setTimeout(function () {
                    $("#resendOtp").show();
                }, 30000);
                $('#submitBtn').removeClass("hidden");

                $('#perDetails').hide();
    //            $('#otpSendTo').addClass("hidden");
    //            $('#iphone').prop("disabled",true);
    //            $('#email').prop("disabled",true);
                $('#otpIntput').removeClass("hidden");
                $('#otpBtn').prop('disabled', false);
    //            $("input").prop('required',true);
    //            $('<input>').attr({
    //                type: 'hidden',
    //                id: 'email',
    //                name: 'email'
    //            }).appendTo('#signupForm');
                    createAlert('', 'Success!', 'OTP send to ' + $('#iphone').val(), 'success', true, true, 'pageMessages');
                    $('#btname').show();
            })
        .fail(function(response){
            $('#btname').show();
            $('#otpBtn').prop('disabled', false);
            $('.load').removeClass('loading');
            if(response.responseJSON.name != undefined){
                $('#form-name').addClass('has-error');
                $('#form-name').append('<div class="error wid100 margin-0">'+response.responseJSON.name+'</div>');
            }
            if(response.responseJSON.email != undefined){
                $('#form-email').addClass('has-error');
                $('#form-email').append('<div class="error wid100 margin-0">'+response.responseJSON.email+'</div>');
            }
            if(response.responseJSON.phone != undefined){
                $('#form-phone').addClass('has-error');
                $('#form-phone').append('<div class="error wid100 margin-0">'+response.responseJSON.phone+'</div>');
            }
            if(response.responseJSON.password != undefined){
                $('#form-password').addClass('has-error');
                $('#form-password').append('<div class="error wid100 margin-0">'+response.responseJSON.password+'</div>');
            }
            if(response.responseJSON.confirm_password != undefined){
                $('#form-confirm_password').addClass('has-error');
                $('#form-confirm_password').append('<div class="error wid100 margin-0">'+response.responseJSON.confirm_password+'</div>');
            }
        });
    }

    function resendOTP(){
        $.post('resendOTP',$('#signupForm').serialize())
            .done(function(response){
                console.log('succes');
            })
            .fail(function(response){
                console.log('error');
            });
    }

    $('#signupBtn').click(function(){
        if($('#otp').val() == ''){
            $('#otpIntput').addClass('has-error');
        }else{
            $.post('verifyOtp',$('#signupForm').serialize())
                .done(function(response){
                    $('#signupForm').submit()
                })
                .fail(function(response){
                    console.log('verify error');
                    $('#otpError').html('<span> OTP Does not Match</span>');
                    $('#otpIntput').addClass('has-error');
                });
        }
    })

    var currentBoxNumber = 0;
    $(".input-text").keyup(function (event) {
        if (event.keyCode == 13) {
            textboxes = $("input.input-text");
            currentBoxNumber = textboxes.index(this);
            console.log(textboxes.index(this));
            if (textboxes[currentBoxNumber + 1] != null) {
                nextBox = textboxes[currentBoxNumber + 1];
                nextBox.focus();
                nextBox.select();
                event.preventDefault();
                return false;
            }
        }
    });

</script>


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>