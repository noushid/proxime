<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <meta Http-Equiv="Cache-Control" Content="no-cache">
     <meta Http-Equiv="Pragma" Content="no-cache">
     <meta Http-Equiv="Expires" Content="0">


    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="css/dropzone.css">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="css/skins/default.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="css/ie10-viewport-bug-workaround.css">
    <script type="text/javascript" src="js/ie-emulation-modes-warning.js"></script>
</head>
<body class="body">
<input type="hidden" id="refresh" value="no">
<div id="pageMessages"></div>


<div class="page_loader"></div>
<!-- Content area start -->
<div class="content-area login-content" style="background-image: url('img/map.jpg');background-size: cover;">
    <div class="container-fluid">
        <div class="">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Login</span></h1>
                        </div>
                        <!-- Form start -->
                        <form action="{{ route('user.auth') }}" method="POST">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="username" class="input-text" placeholder="Username">
                                @if ($errors->first('username'))
                                    <div class="error2">{{ $errors->first('username') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="input-text" placeholder="Password" required="">
                                @if ($errors->first('password'))
                                    <div class="error2">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                            @if ($errors->first('error'))
                                <div class="error2">
                                    {{ $errors->first('error') }}
                                </div>
                            @endif
                            <div class="checkbox">
                                <div class="ez-checkbox pull-left">
                                    <label class="check-container">Remember me
                                      <input type="checkbox" name="remember" id="remember   " value="1">
                                      <span class="checkmark"></span>
                                    </label>
                                </div>
                                <a href="{{route('password.request')}}" class="link-not-important forgot-link pull-right">Forgot Password ?</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button-md button-theme btn-block">login</button>
                            </div>
                        </form>
                        <!-- Form end -->

                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>
                            New to ProxiME ? <a href="{{route('signup.submit')}}"> Sign UP</a>
                        </span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->

<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="js/app.js"></script> -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>

<script type="text/javascript">
     // Showing page loader
    $(window).load(function () {
        setTimeout(function () {
            $(".page_loader").fadeOut("fast");
        }, 100);
        $('.filters-listing-navigation li').click(function() {
            $('.filters-listing-navigation .filtr').removeClass('active');
            $(this).addClass('active');
        });
    });

    var currentBoxNumber = 0;
    $(".input-text").keyup(function (event) {
        if (event.keyCode == 13) {
            textboxes = $("input.input-text");
            currentBoxNumber = textboxes.index(this);
            console.log(textboxes.index(this));
            if (textboxes[currentBoxNumber + 1] != null) {
                nextBox = textboxes[currentBoxNumber + 1];
                nextBox.focus();
                nextBox.select();
                event.preventDefault();
                return false;
            }
        }
    });

</script>
@if(session()->has('message'))
    {{ session()->get('message') }}
    <script>
       $(document).ready(function(){
            createAlert('','Success!','{{session()->get('message')}}','success',true,false,'pageMessages');
        })
    </script>
@endif

</body>
</html>
