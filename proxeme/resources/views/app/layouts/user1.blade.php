
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="css/dropzone.css">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="css/skins/default.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="css/ie10-viewport-bug-workaround.css">
    <script type="text/javascript" src="js/ie-emulation-modes-warning.js"></script>
</head>
<body class="body">
<div class="page_loader"></div>

<a class="home2ToTop" href="home" style="display: inline;"><i class="fa fa-home"></i></a>
<a class="mapToTop" href="map" style="display: inline;"><i class="fa fa-map-marker"></i></a>

<!-- Main header start -->
<header class="main-header">
    <div class="container-fluid">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <a href="map" class="logo">
                    <img src="img/logos/logo.png" alt="logo">
                </a>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="z-index: 9999;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
       <form action="{{route('user.device')}}" name="deviceForm" method="POST" class="form-horizontal">
        {{ csrf_field() }}
             <div class="form-group">
               <label for="" class="control-label col-md-2">Device ID</label>
               <div class="col-md-8">
                   <input type="text" class="form-control" name="deviceId" id="deviceId"/>
               </div>
                <input type="hidden" name="userid"  ng-model="newDevice.user_id={{auth()->user()->id}}"/>
               <div class="col-md-2">
                   <button  type="submit" class="btn btn-success btn-xs plus"><i class="fa fa-plus-circle"></i></button>
               </div>
             </div>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- My profile start -->
<div class="content-area my-profile">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <!-- User account box start -->
                <div class="user-account-box">
                    <div class="header clearfix">
                        <h3>{{auth()->user()->name}}</h3>
                        <p>{{auth()->user()->email}}</p>
                        <div class="edit-profile-photo">
                            <img src="{{auth()->user()->img}}" alt="agent-1" class="img-responsive">
                            <div class="change-photo-btn">
                                <div class="photoUpload">
                                    <span><i class="fa fa-upload"></i> Upload Photo</span>
                                    <form action="{{route('profile.upload')}}" method="POST" name="uploadForm" id="uploadForm" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <input type="file" class="upload" name="file" id="file">
                                    </form>
                                    <script>
                                        document.getElementById("file").onchange = function() {
                                            document.getElementById("uploadForm").submit();
                                        };
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <ul>
                            <li>
                                <a href="{{route('user.profile')}}" {{Request::is('user-profile') ? 'class=active' : ''}}>
                                    <i class="flaticon-social"></i>Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.devices')}}" {{Request::is('user-devices') ? 'class=active' : ''}}>
                                    <i class="flaticon-apartment"></i>My Devices
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus"></i>Submit New Device
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.password')}}" {{Request::is('change-password') ? 'class=active' : ''}}>
                                    <i class="flaticon-security"></i>Change Password
                                </a>
                            </li>
                            <li>
                                <a onclick="document.getElementById('logout-form').submit();">
                                    <i class="flaticon-sign-out-option"></i>Log Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                     {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- User account box end -->
            </div>
            @yield('content')
        </div>
    </div>
</div>
<!-- My profile end -->

<!-- Footer start -->
<footer class="main-footer clearfix">
    <div class="container">
        <!-- Footer top -->
        <div class="footer-top">
            <div class="row text-center">
                <div class="col-md-offset-4 col-md-4 text-center">
                    <div class="logo-2"><img src="img/logo-footer.png" alt="footer-logo"></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->



<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        &copy; ProxiME 2017. All Rights Reserved.
    </div>
</div>
<!-- Copy end right-->

<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-submenu.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.scrollUp.js"></script>
<script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="js/leaflet.js"></script>
<script type="text/javascript" src="js/leaflet-providers.js"></script>
<script type="text/javascript" src="js/leaflet.markercluster.js"></script>
<script type="text/javascript" src="js/dropzone.js"></script>
<script type="text/javascript" src="js/jquery.filterizr.js"></script>
<script type="text/javascript" src="js/app.js"></script>
{{--Input Mask--}}
<script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
<script>

    $(function() {
        $.mask.definitions['~'] = "[+-]";
        $("#iphone").mask("+99 999 999 9999");
    });


    function showInput(item){
        $('#'+item).css('display','inline-block');
        $('#editDvcName'+item).hide();
    }

    function hideInput(item){
        $('#'+item).css('display','none');
        $('#editDvcName'+item).show();
    }
</script>

<script>
$('#editProfileBtn').click(function(e){
    e.preventDefault();
    $('#editProfileSection').removeClass('hidden');
    $('#showProfileSection').addClass('hidden');
});

$('#cancelEditProfile').click(function(e){
    e.preventDefault();
    $('#showProfileSection').removeClass('hidden');
    $('#editProfileSection').addClass('hidden');
})
</script>


</body>
</html>