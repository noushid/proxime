
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
     <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/dropzone.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/jquery-countryselector.css')}}">

    <!-- Phone number filter -->
    <link rel="stylesheet" type="text/css" href="css/intlTelInput.css">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    {{--<link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('css/skins/default.css')}}">--}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/skins/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/ie10-viewport-bug-workaround.css')}}">
    <script type="text/javascript" src="{{asset('js/ie-emulation-modes-warning.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
    {{--<script type="text/javascript" src="{{asset('js/jquery.filterizr.js')}}"></script>--}}
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
     <!-- Phone number field -->
    <script type="text/javascript" src="{{asset('js/intlTelInput.js')}}"></script>
     <!-- Country selector-->
    <script type="text/javascript" src="{{asset('js/jquery.countrySelector.js')}}"></script>
</head>
<body class="body">
<div class="page_loader"></div>

<!-- Option Panel -->
<div class="option-panel option-panel-collased">
    <div class="side-userside">
        <!-- User account box start -->
        <div class="user-account-box fixed-user-account">
            <div class="header clearfix">
                <div class="edit-profile-photo">
                    <img src="{{(auth()->user()->img ? auth()->user()->img : asset('img/default_user.png'))}}" alt="agent-1" class="img-responsive">
                    <div class="change-photo-btn">
                        <div class="photoUpload">
                            <span>Upload image <i class="fa fa-upload"></i></span>
                            <form class="mb0" action="{{route('profile.upload')}}" method="POST" name="uploadForm" id="uploadForm" enctype="multipart/form-data">
                              {{ csrf_field() }}
                                <input type="file" class="upload" name="file" id="file" onchange="form.submit();">
                            </form>
                        </div>
                        <span class="dl-btnimg"><a href="{{route('profile.remove')}}">Remove image <i class="fa fa-trash"></i></a></span>
                    </div>
                </div>
            </div>
            <div class="content">
                <ul>
                    <li>
                        <h3 id="usrName" class="profilename">{{auth()->user()->name}}</h3>
                    </li>
                    <li>
                        <p class="profileemail">{{auth()->user()->email}}</p>
                    </li>
                    <li>
                        <a href="{{route('user.profile')}}" {{Request::is('user-profile') ? 'class=active' : ''}}>
                            <i class="flaticon-social"></i>Profile
                        </a>
                    </li>
                    <li>
                        <a href="{{route('user.devices')}}" {{Request::is('user-devices') ? 'class=active' : ''}}>
                            <i class="flaticon-apartment"></i>My Devices
                        </a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#lab-slide-bottom-popup">
                            <i class="fa fa-plus"></i>Add New Device
                        </a>
                    </li>
                    <li>
                        <a href="{{route('subscriptions')}}" {{Request::is('subscriptions') ? 'class=active' : ''}}>
                            <i class="fa fa-shopping-cart"></i>Our Subscriptions
                        </a>
                    </li>
                    <li>
                        <a href="{{route('user.password')}}" {{Request::is('change-password') ? 'class=active' : ''}}>
                            <i class="flaticon-security"></i>Change Password
                        </a>
                    </li>
                    <li>
                        <a onclick="document.getElementById('logout-form').submit();">
                            <i class="flaticon-sign-out-option"></i>Log Out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- User account box end -->
    </div>
    <div class="setting-button">
        <i class="fa fa-gear"></i>
    </div>
</div>
<!-- /Option Panel -->



<!-- <a class="home2ToTop" href="{{route('home')}}" style="display: inline;"><i class="fa fa-home"></i></a> -->
<a title="Go To Map" class="mapToTop" href="{{route('map')}}" style="display: inline;">Go To Map <i class="fa fa-map-marker"></i></a>

<!-- Main header start -->
<header class="main-header fixed-header">
    <div class="container-fluid">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <a href="{{route('home')}}" class="logo">
                    <img src="{{asset('img/logos/logo.png')}}" alt="ProxiME">
                </a>
            </div>
        </nav>
    </div>
</header>

@if(session()->has('message'))
    {{--{{ session()->get('message') }}--}}
    <script>
           $(document).ready(function(){
                createAlert('','Success!','{{session()->get('message')}}','success',true,true,'pageMessages');
            })
    </script>
@endif
@if(session()->has('error'))
    <script>
           $(document).ready(function(){
                createAlert('Opps!','Something went wrong','{{session()->get('error')}}','danger',true,false,'pageMessages');
            })
    </script>
@endif
@if(session()->has('warning'))
    <script>
           $(document).ready(function(){
                createAlert('Warning!','Something went wrong','{{session()->get('warning')}}','warning',true,false,'pageMessages');
            })
    </script>
@endif

<div id="pageMessages"></div>
<!-- Main header end -->

<!-- My profile start -->
<div class="content-area2 my-profile">
    <div class="container">
        <div class="row">
            <div class="col-md-4 fixed-userside">
                <!-- User account box start -->
                <div class="user-account-box fixed-user-account">
                    <div class="header clearfix">
                        <div class="edit-profile-photo">
                            <img src="{{(auth()->user()->img ? auth()->user()->img : asset('img/default_user.png'))}}" alt="agent-1" class="img-responsive">
                            <div class="change-photo-btn">
                                <div class="photoUpload">
                                    <span>Upload image <i class="fa fa-upload"></i></span>
                                    <form class="mb0" action="{{route('profile.upload')}}" method="POST" name="uploadForm" id="uploadForm" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <input type="file" class="upload" name="file" id="file" onchange="form.submit();">
                                    </form>
                                </div>
                                <span class="dl-btnimg"><a href="{{route('profile.remove')}}">Remove image <i class="fa fa-trash"></i></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <ul>
                            <li>
                                <h3 id="usrNameRes" class="profilename">{{auth()->user()->name}}</h3>
                            </li>
                            <li>
                                <p class="profileemail">{{auth()->user()->email}}</p>
                            </li>
                            <li>
                                <a href="{{route('user.profile')}}" {{Request::is('user-profile') ? 'class=active' : ''}}>
                                    <i class="flaticon-social"></i>Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.devices')}}" {{Request::is('user-devices') ? 'class=active' : ''}}>
                                    <i class="flaticon-apartment"></i>My Devices
                                </a>
                            </li>
                            <li>
                                <a data-toggle="modal" data-target="#lab-slide-bottom-popup">
                                    <i class="fa fa-plus"></i>Add New Device
                                </a>
                            </li>
                            <li>
                                <a href="{{route('subscriptions')}}" {{Request::is('subscriptions') ? 'class=active' : ''}}>
                                    <i class="fa fa-shopping-cart"></i>Our Subscriptions
                                </a>
                            </li>
                            <li>
                                <a href="{{route('user.password')}}" {{Request::is('change-password') ? 'class=active' : ''}}>
                                    <i class="flaticon-security"></i>Change Password
                                </a>
                            </li>
                            <li>
                                <a onclick="document.getElementById('logout-form').submit();">
                                    <i class="flaticon-sign-out-option"></i>Log Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                     {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- User account box end -->
            </div>
            @yield('content')
        </div>
    </div>
</div>
<!-- My profile end -->


<!-- MODAL CONTENT add device STARTS HERE -->
<div class="modal fade" id="lab-slide-bottom-popup" data-keyboard="false" data-backdrop="false">
  <div class="lab-modal-body">
    {{--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>--}}
    <button type="button" class="close" id="modalDismiss"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    
    <div class="submit-address">
        <form action="{{route('user.device')}}" name="deviceAddForm" id="deviceAddForm" method="POST" class="form-horizontal">
            <div class="search-contents-sidebar">
                <div class="form-group">
                    <label><span class="not">Note : </span>Type your Device ID, and Choose your affordable packages.</label>
                    <input type="text" class="input-text mt20" placeholder="Enter Your Device ID Here" name="deviceId" id="deviceId">
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn button-md button-theme appro" id="addDeviceBtn"><i class="fa fa-check"></i></button>
                    {{--<a class="btn button-md button-theme appro" id="addDeviceBtn"><i class="fa fa-check"></i></a>--}}
                </div>
            </div>
        </form>
    </div>
     {{--<div id="pageMessages">ddddddddddddddddddddddd</div>--}}
        {{--<div class="container">--}}
            {{--<button class="btn btn-danger" onclick="createAlert('Opps!','Something went wrong','Here is a bunch of text about some stuff that happened.','danger',true,false,'pageMessages');">Add Danger Alert</button>--}}
            {{--<button class="btn btn-success" onclick="createAlert('','Nice Work!','Here is a bunch of text about some stuff that happened.','success',true,true,'pageMessages');">Add Success Alert</button>--}}
            {{--<button class="btn btn-info" onclick="createAlert('BTDubs','','Here is a bunch of text about some stuff that happened.','info',false,true,'pageMessages');">Add Info Alert</button>--}}
            {{--<button class="btn btn-warning" onclick="createAlert('','','Here is a bunch of text about some stuff that happened.','warning',false,true,'pageMessages');">Add Warning Alert</button>--}}
        {{--</div>--}}

<div class="row wow"  id="userPackage"></div>
    <div class="row wow hidden"  id="userPackage1">
       @if($packages)
            <?php $i = 0; ?>
            @foreach($packages as $package)
                <div class="col-sm-6 col-pad wow fadeInLeft delay-0{{$i= $i+2}}s">
                    <div class="category">
                        <div class="category_bg_box">
                            <div class="category-overlay">
                                <span class="category-content">
                                    <span class="category-title">{{$package->price}}</span>
                                    <span class="category-subtitle">15 Projects</span>
                                    <span class="category-subtitle">30GB Storage</span>
                                    <span class="category-subtitle">Unilimited Data Transfer</span>
                                    <span class="category-subtitle">50 GB Bandwith</span>
                                    <span class="category-subtitle">Enhanced Security</span>
                                    <span class="category-subtitle">Unilimited Data</span>
{{--                                    <a href="{{route('device.package.buy',['package_id' => $package->id, 'device'=> session('user.selected_device'),'token' => csrf_token()])}}" class="btn button-sm button-theme">Buy</a>--}}
                                    <a href="{{route('device.package.confirm',['package_id' => $package->id, 'device'=> session('user.selected_device'),'token' => csrf_token()])}}" class="btn button-sm button-theme">Buy</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

        </div>


  </div>
</div>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>

<!-- Custom javascript -->
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
<script>

    $(document).ready(function(e) {
        var $input = $('#refresh');
        $input.val() == 'yes' ? location.reload(true) : $input.val('yes');
    });

    $(document).keydown(function(event) {
    if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
            event.preventDefault();
         }
        // 107 Num Key  +
        // 109 Num Key  -
        // 173 Min Key  hyphen/underscor Hey
        // 61 Plus key  +/= key
    });

    $(window).bind('mousewheel DOMMouseScroll', function (event) {
           if (event.ctrlKey == true) {
           event.preventDefault();
           }
    });

</script>


<script>


</script>

</body>
</html>