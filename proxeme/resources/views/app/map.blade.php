<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/dropzone.css')}}">
    <!-- ng confirm -->
    <link rel="stylesheet" type="text/css"  href="{{asset('css/angular-confirm.css')}}">

    <link href="css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('css/skins/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="css/ie10-viewport-bug-workaround.css">
    <script type="text/javascript" src="js/ie-emulation-modes-warning.js"></script>


    {{--<script src="http://maps.google.com/maps/api/js"></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyB7E_gLJygbZh6q6GY0d24SPDOYrbDQU3s"></script>

    <script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
    {{--<script type="text/javascript" src="{{asset('js/maps.js')}}"></script>--}}
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->


    <!-- Angular script -->
    <script src="{{asset('js/map/angular-bootstrap.js')}}"></script>
    <script src="{{asset('js/map/angularApp.js')}}"></script>

    <!-- Custom javascript -->
    <script type="text/javascript" src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/custom.js')}}"></script>


</head>
<body class="body" ng-app="app" ng-controller="MapController" ng-init="loadUser({{$user_id}})">
<!-- <div class="page_loader"></div> -->
<div class="map_loader" ng-show="loadingMap"></div>

<!-- <a class="homeToTop" href="home" style="display: inline;"><i class="fa fa-home"></i></a> -->

@if(session()->has('message'))
    {{--{{ session()->get('message') }}--}}
    <script>
           $(document).ready(function(){
                createAlert('','Success!','{{session()->get('message')}}','success',true,true,'pageMessages');
            })
    </script>
@endif
@if(session()->has('error'))
    <script>
           $(document).ready(function(){
                createAlert('Oops!','Something went wrong','{{session()->get('error')}}','danger',true,false,'pageMessages');
            })
    </script>
@endif
@if(session()->has('warning'))
    <script>
           $(document).ready(function(){
                createAlert('Warning!','Something went wrong','{{session()->get('warning')}}','warning',true,false,'pageMessages');
            })
    </script>
@endif


<div id="pageMessages"></div>
<!-- Map content start -->
<div class="map-content content-area container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="row">
            {{--<div id="map"></div>--}}
            <div>
                 <ng-map  zoom="13" center="@{{device.position}}" default-style="false" id="map">
                      <marker id="marker1" position="@{{device.position}}"
                        on-click="map.showInfoWindow('bar')">
                      </marker>
                      <info-window id="bar">
                        <div ng-non-bindable>
                            <strong>@{{device.deviceId}}</strong>
                        </div>
                      </info-window>
                  </ng-map>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 map-content-sidebar" id="side">
        <div class="title-area-one dropdown">
            <img class="map-logo" src="img/logo-map.png" alt="logo">
            <a  tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false" class="button title-area-user"> {{auth()->user()->name}} <i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu" id="pani">
                <li><a href="{{route('user.profile')}}"><i class="fa fa-user"></i> View Profile</a></li>
                <li><a href="{{route('user.devices')}}"><i class="fa fa-desktop"></i>My Devices</a></li>
                <li><a href="{{route('user.password')}}"><i class="fa fa-cog"></i>Change Password</a></li>
                <li><a onclick="document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Log Out</a></li>
            </ul>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                </form>
        </div>
        <div class="title-area">
            <h2 class="pull-left dropdown" style="padding: 8px 5px;">My Devices</h2>
            <h2 class="userhere pull-right dropdown cl" data-toggle="modal" data-target="#lab-slide-bottom-popup" >
                <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Device
            </h2>
            <div class="clearfix"></div>
        </div>
        <div class="fetching-properties">
          <!-- when devices not added -->
          <div id="background" ng-show="user.device.length == 0">
            <p id="bg-text">No Devices Added, Please Click on 'Add Device' to Get Started !</p>
          </div>
          <!-- when devices not added -->
          <div class="map-properties">
              <div id="device" ng-repeat="device in user.device">
                  <div class="col-md-12 property-content" ng-class="{'current-device' : device.id == states.activeDevice, 'disabled-device' : device.status == 0}">
                    <a class="disabled" ng-click="setDevice(device)"><h1 class="title"><i class="fa fa-circle" style="color: @{{ device.color }};"></i>@{{device.name }}</h1></a>
                    <!--<a class="disabled" ng-click="test()"><h1 class="title"><i class="fa fa-circle" style="color: @{{ device.color }};"></i>@{{device.deviceId }}</h1></a>-->
                    <h3 class="property-address"> <i class="fa fa-map-marker"></i>123 Kathal St. Tampa City, </h3>
                    <!-- Property footer -->
                    <div class="property-footer">
                       <span class="left">
                           <i class="fa fa-battery-half fs15"></i>
                           <i class="dvc-active" ng-show="device.status == 1">Active</i>
                           <i class="dvc-inactive" ng-show="device.status == 0">Inactive</i>
                       </span>
                       <span class="right">
                           <div class="title-area2">
                               <a  class="show-more-options pull-right" ng-click="openNav(device)">
                                   <i class="fa fa-plus-circle"></i>Show More
                               </a>
                               <div class="clearfix"></div>
                           </div>
                       </span>
                    </div>
                  </div>
                  <div id="options-content" class="collapse col-md-12 property-content">
                      <ul class="facilities-list clearfix">
                          <li>
                              <span>address</span>
                          </li>
                          <li>
                              <span>title</span>
                          </li>
                      </ul>
                  </div>
                  <!-- side history start -->
                  <div id="mySidenav" class="sidenav" ng-style="navStyle">
                    <a href="javascript:void(0)" class="closebtn" ng-click="closeNav()"><i class="fa fa-arrow-right"></i></a>
                    <span class="dvc"><i class="fa fa-circle" style="color: @{{ deviceDetails.color }};"></i>@{{deviceDetails.name }}</span>
                    <span class="dvc-id">Device ID : @{{deviceDetails.deviceId }}</span>
                    <div class="tab-style-2">
                      <div class="tab-style-2-line">
                          <ul class="nav nav-tabs ">
                              <li ng-class="{'active' : showstatus}">
                                  <a ng-click="showhistory = fasle;showstatus=true;">Status </a>
                              </li>
                              <li ng-class="{'active' : !showstatus}">
                                  <a href="" ng-click="showhistory = true;showstatus=false;">History </a>
                              </li>
                          </ul>
                          <div class="tab-content">
                              <div class="active" ng-show="showstatus">
                                  <div class="property-content shadow-non">
                                        <!-- Facilities List -->
                                        <ul class="facilities-list clearfix">
                                            <li class="w100">
                                                Active Time :
                                                <span class="float-right">48:55 Hours</span>
                                            </li>
                                            <li class="w100">
                                                Signal Level :
                                                <span class="float-right">70%</span>
                                            </li>
                                            <li class="w100">
                                                Battery :
                                                <span class="float-right">10% </span>
                                            </li>
                                            <li class="w100">
                                                BlueTooth :
                                                <span class="float-right"> 0%</span>
                                            </li>
                                        </ul>
                                    </div>
                              </div>
                              <div class="active" ng-show="showhistory">
                                  <div class="property-content noshadow">
                                        <!-- Facilities List -->
                                        <ul class="facilities-list clearfix">
                                            <li class="w100"><a href="#">21 July, 2017<span class="float-right">Banglure, India</span></a></li>
                                            <li class="w100"><a href="#">21 July, 2017<span class="float-right">Banglure, India</span></a></li>
                                            <li class="w100"><a href="#">21 July, 2017<span class="float-right">Banglure, India</span></a></li>
                                            <li class="w100"><a href="#">21 July, 2017<span class="float-right">Banglure, India</span></a></li>
                                            <li class="w100"><a href="#">21 July, 2017<span class="float-right">Banglure, India</span></a></li>
                                        </ul>
                                    </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- side history end -->
              </div>
          </div>
        </div>
    </div>
</div>
<!-- Map content end -->

<!-- MODAL CONTENT add device STARTS HERE -->
<div class="modal fade" id="lab-slide-bottom-popup" data-keyboard="false" data-backdrop="false" >
  <div class="lab-modal-body">
    <button type="button" class="close" id="modalDismiss"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    
    <div class="submit-address mb20">
        <form action="{{route('user.device')}}" name="deviceAddForm" id="deviceAddForm" method="POST" class="form-horizontal">
            <div class="search-contents-sidebar">
                <div class="form-group">
                    <label><span class="not">Note : </span>Type your DEVICEID, and Choose your affordable packages.</label>
                    <input type="text" class="input-text mt20" name="deviceId"  id="deviceId" placeholder="Enter Your Device ID Here ">
                </div>
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn button-md button-theme appro" id="addDeviceBtn" ><i class="fa fa-check"></i></button>
                </div>
            </div>
        </form>
    </div>

    <div class="row wow"  id="userPackage"></div>


  </div>
</div>
<script>
$('#modalDismiss').click(function(){
    if(modalLock != 1){
        $('#deviceId').val('');
        $('#lab-slide-bottom-popup').modal('hide');
    }
});

/*model show for Add device */
var modalLock=0;
$('#addDeviceBtn').click(function() {
    $.post($('#deviceAddForm').attr('action'),$('#deviceAddForm').serialize())
        .done(function(response){
            modalLock=1;
            window.location = 'map';
            /*
            //////This code for show the all packages on the same popup
            if(response.status == 'package_null'){
                $('#addDeviceBtn').prop('disabled',true);
                //$('#userPackage').removeClass('hidden');
                //get packages and append to a div
                $.get('getPackages')
                    .done(function (response) {
                        console.log(response);
                        $('#userPackage').html(response);
                    })
                    .fail(function (response) {
                        console.log('error');
                    });

                $('#deviceId').val('');
            }else{
                //window.location = "user-devices";
                //window.location = document.referrer;
                $.post('set-session', {type: 'message', data: 'Device added'});
                window.location.reload();
                createAlert('', 'Success!', 'Device Added', 'success', true, true, 'pageMessages');
            }*/
        })
        .fail(function(response){
            modalLock=0;
            $('#deviceAddError').html(response.responseText);
            createAlert('Oops!','Something went wrong',response.responseText,'danger',true,true,'pageMessages',true);
        });
});

/************ Prevent form submission on hitting Enter key for Text  *******/
document.getElementById("deviceId").onkeypress = function (e) {
    var key = e.charCode || e.keyCode || 0;
    if (key == 13) {
        e.preventDefault();
    }
};

</script>
<script>
    $(document).keydown(function(event) {
    if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
            event.preventDefault();
         }
        // 107 Num Key  +
        // 109 Num Key  -
        // 173 Min Key  hyphen/underscor Hey
        // 61 Plus key  +/= key
    });

    $(window).bind('mousewheel DOMMouseScroll', function (event) {
           if (event.ctrlKey == true) {
           event.preventDefault();
           }
    });
</script>
</body>
</html>