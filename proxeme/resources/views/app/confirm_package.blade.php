<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/dropzone.css')}}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/skins/default.css')}}">
    {{--<link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('css/skins/default.css')}}">--}}
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/ie10-viewport-bug-workaround.css')}}">
    <script type="text/javascript" src="{{asset('js/ie-emulation-modes-warning.js')}}"></script>
</head>
<body class="body">
<div class="page_loader"></div>



<!-- Agent section start -->
<div class="agent-section content-area">
    <div class="container">
        <!-- option bar start -->
        <div class="option-bar">
            <div class="row">
                <div class="col-lg-10 col-md-2">
                    <h4 style="line-height: 20px;">
                        <span class="heading-icon">
                            <i class="fa fa-th-list"></i>
                        </span>
                        <span class="hidden-xs">Hi, {{auth()->user()->name}}</span>... Please confirm your Selected Plan and Order details !
                    </h4>
                </div>
                <div class="col-lg-2 col-md-2 cod-pad">
                    <div class="sorting-options" style="margin-top: 10px;">
                        <a href="#" class="change-view-btn ord"><i class="fa fa-user"></i></a>
                        <a href="#" class="change-view-btn active-view-btn"><i class="fa fa-home"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- option bar end -->

        <div class="row">
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="plan">
                    <div class="price-header">
                        <h3>{{$package->name}}</h3>
                        <h1>{{$package->price}}</h1>
                    </div>

                    <div class="plan-features">
                        <ul>
                            <li>15 Projects</li>
                            <li>30GB Storage</li>
                            <li>Unilimited Data Transfer</li>
                            <li>50 GB Bandwith</li>
                            <li>Enhanced Security</li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-1"></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="sidebar">
                    <div class="sidebar-widget category-posts">
                        <div class="main-title-2">
                            <h1><span>Order</span> Summery</h1>
                        </div>
                        <ul class="list-unstyled list-cat">
                            <li>Name <span>{{auth()->user()->name}}  </span></li>
                            <li>You Selected Device Id<span>{{session('cart.device')}}</span></li>
                            <li>You Selected Plan <span>{{$package->name}}  </span></li>
                            <li class="rate">Plan Rate<span>{{$package->price}} </span></li>
                            @if($package->offer_price)
                                <li class="discount">Offer Price<span>{{$package->offer_price}}  </span></li>
                            @endif
                            <li class="total">Total Amount<span>{{($package->offer_price ? $package->offer_price : $package->price) }}  </span></li>
                        </ul>
                    </div>
                    <div class="button-section pull-right">
                    <p>
                        {{--<a href="#" class="btn button-sm border-button-theme">View more plans</a>--}}
                        <a href="{{session('cart.redirect')}}" class="btn button-sm button-theme2">cancel</a>
                        {{--<a href="" class="btn button-sm button-theme">Proceed to pay</a>--}}
                        <a href="{{route('device.package.buy',['package_id' => $package->id, 'device'=> session('cart.device'),'token' => csrf_token()])}}" class="btn button-sm button-theme">Proceed to pay</a>
                    </p>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Agent section end -->



<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        &copy; ProxiME 2017. All Rights Reserved.
    </div>
</div>
<!-- Copy end right-->

<script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-submenu.js')}}"></script>
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet-providers.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet.markercluster.js')}}"></script>
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.filterizr.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('s/maps.js)}}j"></script>--}}
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>

</body>
</html>