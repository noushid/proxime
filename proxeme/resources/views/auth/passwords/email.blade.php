<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME | Forgot Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/dropzone.css')}}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('css/skins/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/ie10-viewport-bug-workaround.css')}}">

     <!-- Phone number filter -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/intlTelInput.css')}}">


    <script type="text/javascript" src="{{asset('js/ie-emulation-modes-warning.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.filterizr.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/maps.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/intlTelInput.js')}}"></script>
    <style>
        #phone .intl-tel-input{
            width: 100%;
        }
        #phone .intl-tel-input.separate-dial-code .selected-dial-code{
            padding-left: 7px !important;
        }
    </style>
</head>
<body class="body">
<div class="page_loader"></div>

<!-- Content area start -->
<div class="content-area login-content" style="background-image: url('../img/map.jpg');background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Forgot Password</span></h1>
                        </div>

                         @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <!-- Form start -->
                        {{--<form action="{{ route('password.email') }}" method="POST">--}}
{{--                        <form action="{{ route('forgot.password') }}" method="POST" id="">--}}
                        <form method="POST" id="forgotForm" action="{{route('password.phone')}}">
                            {{ csrf_field() }}

                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}" id="phone">
                                <input type="text" name="phone" class="input-text" placeholder="Phone number" value="{{ old('identity') }}" id="iphone">
                            </div>

                            <div class="form-group hidden{{ $errors->has('email') ? ' has-error' : '' }}" id="email">
                                <input type="text" name="email" class="input-text" placeholder="Email Address" value="{{ old('identity') }}" id="iemail">
                            </div>
                             <div class="form-group otp" id="otpSendTo">
                                <label class="otp-info">Using </label>
                                <label class="radio inline">
                                    <input type="radio" class="" name="sendTo" value="phone" checked onclick="using('phone')">
                                    <span> Phone </span>
                                </label>
                                <label class="radio inline">
                                    <input type="radio" class="" name="sendTo" value="email" onclick="using('email')" id="r_email">
                                    <span>Email </span>
                                </label>
                            </div>

                             @if ($errors->has('email'))
                                <script>
                                    $("#r_email").prop( "checked", true );
                                    $('#phoneError').html('');
                                    $("#iphone").val('');
                                    $('#phone').addClass('hidden');
                                    $('#email').removeClass('hidden');
                                    $('#forgotForm').attr('action', '{{route('password.email')}}');
                                </script>
                                <span class="help-block has-error" id="emailerror">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            @if(session()->has('error'))
                            <span class="help-block has-error" id="error">
                                <strong>{{session()->get('error')}}</strong>
                            </span>
                            @endif
                            <span id="phoneError"></span>
                            <div class="form-group">
                                <button type="button" id="submitBtn" class="btn button-md button-theme btn-block">Submit</button>
                                {{--<button type="submit" class="btn button-md button-theme btn-block">Submit</button>--}}
                            </div>
                        </form>
                        <!-- Form end -->
                    </div>

                    <!-- Footer -->
                    <div class="footer">
                       <span>
                            <a href="{{route('user.auth')}}">Return to Login</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->


<script>
$("#iphone").intlTelInput({
      autoHideDialCode: false,
      autoPlaceholder: "on",
      dropdownContainer: "body",
      formatOnDisplay: false,
      geoIpLookup: function(callback) {
         $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
           var countryCode = (resp && resp.country) ? resp.country : "";
           callback(countryCode);
         });
      },
      initialCountry: "auto",
      nationalMode: false,
      placeholderNumberType: "MOBILE",
      separateDialCode: true,
      utilsScript: "../js/utils.js"
    });

function using(item){
    if(item == 'email'){
        $('#error').html('');
        $('#phoneError').html('');
        $("#iphone").val('');
        $('#phone').addClass('hidden');
        $('#email').removeClass('hidden');
        $('#forgotForm').attr('action', '{{route('password.email')}}');
    }
    if(item == 'phone'){
        $('#phoneError').html('');
        $('#error').html('');
        $('#email').addClass('hidden');
        $('#phone').removeClass('hidden');
        $('#forgotForm').attr('action', '{{route('password.phone')}}');
    }
}
$('#submitBtn').click(function(e) {
    e.preventDefault();

    if($("#iphone").val() == "") {
        $('#forgotForm').submit();
    }else{
        var isValid = $("#iphone").intlTelInput("isValidNumber");
        /*****Validate phone number ****/
        if(isValid == false) {
            $('#emailerror').html('');
            $('#phoneError').addClass('help-block has-error');
            $('#phoneError').html('<strong>Invalid Phone number.</strong>');
        }else if(isValid == true){
            /****Append country based phone number to phone number field ****/
            $("#iphone").val($("#iphone").intlTelInput("getNumber"));
            $('#forgotForm').submit();
        }
    }


})

</script>
</body>
</html>
