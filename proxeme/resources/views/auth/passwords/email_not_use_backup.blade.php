<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>ProxiME | Forgot Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{asset('css/dropzone.css')}}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('css/skins/default.css')}}">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/ie10-viewport-bug-workaround.css')}}">



    <script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-submenu.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet-providers.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/leaflet.markercluster.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.filterizr.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/maps.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/ie-emulation-modes-warning.js')}}"></script>
</head>
<body class="body">
<div class="page_loader"></div>

<!-- Content area start -->
<div class="content-area login-content" style="background-image: url('../img/map.jpg');background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Forgot Password</span></h1>
                        </div>

                         @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <!-- Form start -->
                        <form action="{{ route('password.email') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" name="email" class="input-text" placeholder="Email Address" value="{{ old('email') }}" required="">
                            </div>
                             @if ($errors->has('email'))
                                <span class="help-block has-error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <div class="form-group">
                                <button type="submit" class="btn button-md button-theme btn-block">Send Password Reset link</button>
                            </div>
                        </form>
                        <!-- Form end -->
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                       <span>
                            I want <a href="{{route('user.auth')}}">Return to Login</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->




</body>
</html>
