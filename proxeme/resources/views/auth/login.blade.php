<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <title>ProxiME</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="Admin Dashboard" name="description" />
      <meta content="ThemeDesign" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="assets/images/favicon.ico">
      {{--<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
      <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
      <link href="assets/css/style.css" rel="stylesheet" type="text/css">--}}
      <!-- CSRF Token -->
          <meta name="csrf-token" content="{{ csrf_token() }}">

          <title>{{ config('app.name', 'Laravel') }}</title>

          <!-- Styles -->
          <link href="{{ asset('css/app.css') }}" rel="stylesheet">
   </head>
   <body>
      <div class="accountbg"></div>
      <div class="wrapper-page">



         <div class="panel panel-color panel-primary panel-pages">
            <div class="panel-body">
               <h3 class="text-center m-t-0 m-b-15"> <a href="" class="logo logo-admin">{{--<span>ProxiME</span>    Admin--}} <img src="../images/logo.png"></a></h3>
               <h4 class="text-muted text-center m-t-0"><b></b>  <span>Admin </span>Sign In</h4>
               @if ($errors->has('error'))
                    <div class="error">
                        {{$errors->first('error')}}
                    </div>
               @endif
               <form class="form-horizontal m-t-20" action="{{ route('admin.auth') }}" method="POST">
               {{ csrf_field() }}
                  <div class="form-group {{$errors->has('username') ? 'has-error' :''}}">
                     <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Username" name="username">
                        @if($errors->has('username'))
                            <span class="help-block">
                            <strong>{{$errors->first('username')}}</strong>
                            </span>
                        @endif
                     </div>
                  </div>
                  <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Password" name="password">
                        @if($errors->has('password'))
                            <span class="help-block">
                                <strong>{{$errors->first('password')}}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  {{--<div class="form-group">--}}
                     {{--<div class="col-xs-12">--}}
                        {{--<div class="checkbox checkbox-primary"> <input id="checkbox-signup" type="checkbox" value="1" name="remember"> <label for="checkbox-signup"> Remember me </label></div>--}}
                     {{--</div>--}}
                  {{--</div>--}}
                  <div class="form-group text-center m-t-40">
                     <div class="col-xs-12"> <button class="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Log In</button></div>
                  </div>
                  {{--<div class="form-group m-t-30 m-b-0">--}}
                     {{--<div class="col-sm-7"> <a href="{{route('password.request')}}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a></div>--}}
                     {{--<div class="col-sm-5 text-right"> <a href="{{route('password.request')}}" class="text-muted">Create an account</a></div>--}}
                  {{--</div>--}}
               </form>
            </div>
         </div>
      </div>
      <script src="js/jquery.min.js"></script> <script src="js/bootstrap.min.js"></script> <script src="js/modernizr.min.js"></script> <script src="js/detect.js"></script> <script src="js/fastclick.js"></script> <script src="js/jquery.slimscroll.js"></script> <script src="js/jquery.blockUI.js"></script> <script src="js/waves.js"></script> <script src="js/wow.min.js"></script> <script src="js/jquery.nicescroll.js"></script> <script src="js/jquery.scrollTo.min.js"></script> <script src="js/app.js"></script>
   </body>
</html>