<div class="container">
    <div class="row" ng-show="showform">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="m-t-0 m-b-30">Add Package</h4>
                    <form class="form-horizontal" role="form" ng-submit="addPackage()">
                        <div class="form-group">
                            <label class="col-md-1 control-label">Name</label>
                            <div class="col-md-5" ng-class="{'has-error' : validationErrors.name}">
                                <input type="text" class="form-control" ng-model="newpackage.name" placeholder="Package Name">
                                <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>
                            </div>

                            <label class="col-md-1 control-label">Price</label>
                            <div class="col-md-5" ng-class="{'has-error' : validationErrors.price}">
                                <input type="text" class="form-control" ng-model="newpackage.price" placeholder="Package Price">
                                <span class="help-block" ng-if="validationErrors.price"><small>{{validationErrors.price[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Offer Price</label>
                            <div class="col-md-5" ng-class="{'has-error' : validationErrors.offer_price}">
                                <input type="text" class="form-control" ng-model="newpackage.offer_price" placeholder="Package offer Price">
                                <span class="help-block" ng-if="validationErrors.offer_price"><small>{{validationErrors.offer_price[0]}}</small></span>
                            </div>

                            <label class="col-md-1 control-label">Duration</label>
                            <div class="col-md-5"  ng-class="{'has-error' : validationErrors.duration}">
                                <input type="text" class="form-control" ng-model="newpackage.duration" placeholder="Package Duration in Days">
                                <span class="help-block" ng-if="validationErrors.duration"><small>{{validationErrors.duration[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Data</label>
                            <div class="col-md-5" ng-class="{'has-error' : validationErrors.usage}">
                                <input type="text" class="form-control" ng-model="newpackage.usage" placeholder="Package Total Data in MB">
                                <span class="help-block" ng-if="validationErrors.usage"><small>{{validationErrors.usage[0]}}</small></span>
                            </div>

                            <label class="col-md-1 control-label">Description</label>
                            <div class="col-md-5"  ng-class="{'has-error' : validationErrors.description}">
                                <textarea class="form-control" ng-model="newpackage.description" maxlength="150" placeholder="Package Description"></textarea>
                                <p class="text-right">{{150 - newpackage.description.length + ' Char' + '/ 150' }}</p>
                                <span class="help-block" ng-if="validationErrors.description"><small>{{validationErrors.description[0]}}</small></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Type</label>
                            <div class="col-md-5"  ng-class="{'has-error' : validationErrors.featured}">
                                <select name="" id="" class="form-control" ng-model="newpackage.featured">
                                    <option>select</option>
                                    <option value="1">Featured</option>
                                    <option value="0">Normal</option>
                                </select>
                                <span class="help-block" ng-if="validationErrors.featured"><small>{{validationErrors.featured[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="m-b-30 m-t-0">
                        <a ng-click="loadPackages()" style="cursor: pointer">{{title}}</a>
                        <a ng-click="newPackage()" ng-show="!showform" style="cursor: pointer"ng-if="!trashed" class="btn btn-success">Add Package <i class="mdi mdi-package"></i></a>
                        <a ng-click="trash()" ng-show="!trashed" class="pull-right" style="cursor: pointer">Trash</a>
                        <a ng-click="loadPackages()" class="pull-right" ng-show="trashed" style="cursor: pointer">Go back</i></a>
                    </h4>

                    <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer" ng-show="packages.length > 0">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="dataTables_length" id="datatable_length">
                                    <label>Show
                                        <select name="datatable_length" aria-controls="datatable" class="form-control input-sm" ng-model="numPerPage"
                                                ng-options="num for num in paginations">{{num}}
                                        </select>
                                        entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="dt-buttons btn-group">
                                    <a class="btn btn-default buttons-pdf buttons-html5 btn-success" tabindex="0" aria-controls="datatable-buttons" ng-click="exportPdf()"><span>Export To PDF</span></a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div id="datatable_filter" class="dataTables_filter pull-right">
                                    <label>Search:
                                        <input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable" ng-model="search">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                                <thead>
                                <tr role="row">
                                    <th>Sl.No</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Price
                                        <div class="pull-right">
                                            <a ng-click="sort('price')">
                                                <i class="fa fa-sort" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </th>
                                    <th>Offer Price</th>
                                    <th>Duration
                                        <div class="pull-right">
                                            <a ng-click="sort('duration')">
                                                <i class="fa fa-sort" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </th>
                                    <th>Data
                                        <div class="pull-right">
                                            <a ng-click="sort('usage')">
                                                <i class="fa fa-sort" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </th>
                                    <th>Featured</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd"  dir-paginate="package in packages | filter:search | itemsPerPage:numPerPage | orderBy:orderProp:direction" current-page="currentPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <!--                                <td>{{package.id}}</td>-->
                                    <td>{{package.name}}</td>
                                    <td class="description">{{package.description}}</td>
                                    <td>{{package.price}}</td>
                                    <td>{{package.offer_price}}</td>
                                    <td>{{package.duration}}</td>
                                    <td>{{package.usage}}</td>
                                    <td><i class="fa fa-times" aria-hidden="true" ng-show="package.featured == 0"></i><i ng-show="package.featured == 1" class="fa fa-check" aria-hidden="true"></i></td>
                                    <td>
                                        <div ng-show="!trashed" class="btn-group btn-group-xs" role="group">
                                            <button type="button" class="btn btn-info" ng-click="editPackage(package)">
                                                <!--                                                <i class="fa fa-pencil"></i>-->
                                                Edit
                                            </button>
                                            <button  type="button" class="btn btn-danger" ng-click="deletePackage(package)">
                                                <!--                                                <i class="fa fa-trash-o"></i>-->
                                                Delete
                                            </button>
                                        </div>
                                        <div ng-show="trashed" class="btn-group btn-group-xs" role="group">
                                            <button  type="button" class="btn btn-danger" ng-click="deleteTrash(package)">
                                                <!--                                                <i class="fa fa-trash-o"></i>-->
                                                Delete
                                            </button>
                                            <button type="button" class="btn btn-warning" ng-click="restore(package)">
                                                <!--                                                <i class="fa fa-undo" aria-hidden="true"></i>-->
                                                Restore
                                            </button>
                                        </div>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                Showing {{currentPage}} to {{(numPerPage < packages.length  ? currentPage*numPerPage :packages.length)}} of {{packages.length}} entries
                            </div>
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                Showing {{(currentPage-1)*numPerPage+1}} to {{((currentPage*numPerPage) > packages.length ? packages.length :(currentPage*numPerPage))}} of {{packages.length}} entries
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                                <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true">
                                </dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                    <div ng-show="packages.length == 0" class="help-block text-center">
                        Nothing to display
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>