<div class="modal-header">
    <div class="col-lg-4">
        <h3 class="modal-title" id="modal-title">{{items.name}}</h3>
    </div>
</div>
<div class="modal-body" id="modal-body">
    <uib-tabset active="activeForm">
        <uib-tab index="0" heading="Home">
           <div class="row">
               <div class="col-lg-4">
                   <div class="panel panel-primary">
                       <div class="panel-body">
                           <h4 class="m-t-0">Device Statistics</h4>
                           <div class="list-group">
                               <a href="#" class="list-group-item"> <strong>Battery </strong><span class="pull-right"> <i class="fa fa-battery-{{(onlineDevice.battery >= 4 ? 'full' : onlineDevice.battery)}}"></i></span></a>
                               <a href="#" class="list-group-item"> <strong>Signal </strong><span class="pull-right"> {{onlineDevice.signal}}</span> </a>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-lg-4">
                   <div class="panel panel-primary">
                       <div class="panel-body">
                           <h4 class="m-t-0">Current Package</h4>
                           <div class="list-group">
                               <a href="#" class="list-group-item active"> <i class="ion-arrow-swap"></i> <strong>Data</strong><span class="pull-right">{{items.active_package.package.usage}}</span> </a>
                               <a href="#" class="list-group-item"><i class="ion-ios7-time"></i> <strong>Duration</strong> <span class="pull-right">{{items.active_package.package.duration}}</span> </a>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="well">
<!--                       {{onlineDevice}}-->
                       <div map-lazy-load="https://maps.google.com/maps/api/js">
                           <ng-map center="{{onlineDevice.latitude}}, {{onlineDevice.longitude}}" zoom="15">
                               <marker position="{{onlineDevice.latitude}}, {{onlineDevice.longitude}}"
                                       draggable="false">
                                       </marker>
<!--                               <custom-marker position="11.120298, 76.119968"-->
<!--                                              animation="DROP"-->
<!--                                              draggable="true">-->
<!--<!--                                   <div class="gmaps-overlay">Our Office!<div class="gmaps-overlay_arrow above"></div></div>-->
<!--                               </custom-marker>-->

                               <custom-control id="home" position="TOP_RIGHT" index="10" on-click="vm.click()">
                                   <button class="btn btn-default btn-xs pad2">Locate</button>
                               </custom-control>
                           </ng-map>
                       </div>
                   </div>
               </div>
           </div>
        </uib-tab>
        <uib-tab index="1" heading="Package History">
            <div class="row">
                <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                    <thead>
                    <tr role="row">
                        <th>Sl No</th>
                        <th>Order No</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Start data</th>
                        <th>End data</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr role="row" class="odd"  dir-paginate="package in items.device_packages | filter:search | itemsPerPage:numPerPage" ng-if="package.status != 1">
                        <td>{{$index+1}}</td>
                        <td>{{package.order_no}}</td>
                        <td>{{package.recharge_date}}</td>
                        <td><a>{{package.package.name}}</a></td>
                        <td><a>{{package.package.price}}</a></td>
                        <td><a>{{package.recharge_date}}</a></td>
                        <td><a>{{package.package.duration}}</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </uib-tab>
    </uib-tabset>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
    <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
</div>