<div class="container">
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-heading">
                    <h4 class="panel-title text-muted font-light">Active Device</h4>
                </div>
                <div class="panel-body p-t-10">
                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi-chip" style=" color:#3549e1;"></i><b>{{virtualDevices.length}}</b></h2>
<!--                    <p class="text-muted m-b-0 m-t-20"><b>48%</b> From Last 24 Hours</p>-->
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-heading">
                    <h4 class="panel-title text-muted font-light">Total Device</h4>
                </div>
                <div class="panel-body p-t-10">
                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi-chip" style=" color:#008507;"></i><b>{{deviceCount}}</b></h2>
<!--                    <p class="text-muted m-b-0 m-t-20"><b>42%</b> Orders in Last 10 months</p>-->
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-heading">
                    <h4 class="panel-title text-muted font-light" >Total Users</h4>
                </div>
                <div class="panel-body p-t-10">
                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi-account" style=" color:#008507;"></i><b>{{userCount}}</b></h2>
<!--                    <p class="text-muted m-b-0 m-t-20"><b>22%</b> From Last 24 Hours</p>-->
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="panel text-center">
                <div class="panel-heading">
                    <h4 class="panel-title text-muted font-light">Active Users</h4>
                </div>
                <div class="panel-body p-t-10">
                    <h2 class="m-t-0 m-b-15"><i class="mdi mdi-account-check" style=" color:#3549e1;"></i><b>{{userActiveCount}}</b></h2>
<!--                    <p class="text-muted m-b-0 m-t-20"><b>35%</b> From Last 1 Month</p>-->
                </div>
            </div>
        </div>
    </div>



    <!--chart strated hear -->

    <div class="page-content-wrapper ">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-body">
                            <h4 class="m-t-0 m-b-30">Device Statistics</h4>
                            <div id="website-stats" style="height: 320px" class="flot-chart">
                                <canvas class="chart chart-line" chart-data="data" chart-labels="labels"
                                        chart-series="series" chart-click="onClick"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-body">
                            <h4 class="m-t-0 m-b-30">Realtime Statistics</h4>
<!--                            <div id="flotRealTime" style="height: 320px" class="flot-chart"></div>-->
                            <canvas id="doughnut" class="chart chart-doughnut"
                                    chart-data="piedata" chart-labels="pielabels">
                            </canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-body">
                            <div id="donut-chart">
                                <h4 class="m-t-0 m-b-30">Devices</h4>
<!--                                <div id="donut-chart-container" class="flot-chart" style="height: 320px"></div>-->
                                <canvas id="pie" class="chart chart-pie"
                                        chart-data="piedata" chart-labels="pielabels" chart-options="options">
                                </canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-body">
                            <div id="pie-chart">
                                <h4 class="m-t-0 m-b-30">Device</h4>
<!--                                <div id="pie-chart-container" class="flot-chart" style="height: 320px"></div>-->
                                <canvas id="bar" class="chart chart-bar"
                                        chart-data="data" chart-labels="labels"> chart-series="series"
                                </canvas
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
