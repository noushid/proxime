<div class="container">
    <div class="row" ng-show="showform">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="m-t-0 m-b-30">Add User</h4>
                    <form class="form-horizontal" role="form" ng-submit="addUser()">
                        <div class="form-group" ng-class="{'has-error' : validationErrors.name}">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newuser.name" placeholder="Name">
                                <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationErrors.username}">
                            <label class="col-md-2 control-label">Username</label>
                            <div class="col-md-10">
<!--                                <input type="text" class="form-control" ng-model="newuser.username" placeholder="Username" ng-change="checkUsername(newuser.username)">-->
                                <input type="text" class="form-control" ng-model="newuser.username" placeholder="Username">
                                <span class="help-block" ng-if="validationErrors.username"><small>{{validationErrors.username[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationErrors.email}">
                            <label class="col-md-2 control-label" for="example-email">Email</label>
                            <div class="col-md-10">
<!--                                <input type="email" name="email" class="form-control" placeholder="Email" ng-model="newuser.email" ng-change="checkEmail(newuser.email)">-->
                                <input type="email" name="email" class="form-control" placeholder="Email" ng-model="newuser.email">
<!--                                <span class="help-block" ng-if="validation.email == true"><small>Username Already exist!.</small></span>-->
                                <span class="help-block" ng-if="validationErrors.email"><small>{{validationErrors.email[0]}}</small></span>
                            </div>
                        </div>
                        <div class="form-group" ng-class="{'has-error' : validationErrors.phone}">
                            <label class="col-md-2 control-label">Phone</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newuser.phone" placeholder="Phone Number">
                                <span class="help-block" ng-if="validationErrors.phone"><small>{{validationErrors.phone[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group" ng-show="edit">
                            <label class="col-md-2 control-label">Reset Password</label>
                            <div class="col-md-10">
                                <input type="checkbox" ng-model="newuser.reset_password" name="reset_password" checked="" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
<!--                                <button type="submit" class="btn btn-primary" ng-disabled="validation.email == true || validation.username == true">Submit</button>-->
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">

                    <h4 class="m-b-30 m-t-0">
                        <a ng-click="loadUsers()" style="cursor: pointer">{{title}}</a>
                        <a ng-click="newUser()" ng-show="!showform" style="cursor: pointer"ng-if="!trashed" class="btn btn-success">Add User <i class="mdi mdi-account-plus"></i></a>
                        <a ng-click="trash()" class="pull-right" ng-show="!trashed" style="cursor: pointer">Trash</a>
                        <a ng-click="loadUsers()" ng-show="trashed" class="pull-right" style="cursor: pointer">Go back</a>
                    </h4>

                    <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer" ng-show="users.length > 0">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length" id="datatable_length">
                                    <label>Show
                                        <select name="datatable_length" aria-controls="datatable" class="form-control input-sm" ng-model="numPerPage"
                                                ng-options="num for num in paginations">{{num}}
                                        </select>
                                        entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div id="datatable_filter" class="dataTables_filter pull-right">
                                    <label>Search:
                                        <input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable" ng-model="search">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                                <thead>
                                <tr role="row">
                                    <th>Sl.No</th>
                                    <th>Name
                                        <div class="pull-right">

                                            <a ng-click="sort('name')">
                                                <i class="fa fa-sort" aria-hidden="true"></i>
                                            </a>
                                            <!--                                        <a ng-click="orderProperty='name'" ng-show="orderProperty == '-name'">-->
                                            <!--                                            <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>-->
                                            <!--                                        </a>-->
                                            <!--                                        <a ng-click="orderProperty='-name'" ng-show="orderProperty == 'name'">-->
                                            <!--                                            <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>-->
                                            <!--                                        </a>-->
                                        </div>
                                    </th>
                                    <th>Devices
                                        <div class="pull-right">
                                            <a ng-click="sort('device')">
                                                <i class="fa fa-sort" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </th>
                                    <th>Username
                                        <div class="pull-right">
                                            <a ng-click="sort('username')">
                                                <i class="fa fa-sort" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </th>
                                    <th>Email
                                        <div class="pull-right">
                                            <a ng-click="sort('email')">
                                                <i class="fa fa-sort" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </th>
                                    <th>Phone</th>
                                    <th>Status
                                    </th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd" dir-paginate="user in users | filter:search | limitTo:pageSize | itemsPerPage:numPerPage | orderBy:orderProp:direction" ng-class="{'text-danger' : user.status==0}" current-page="currentPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td><a ng-click="viewUser(user);">{{user.name}}</a></td>
                                    <td>
                                        <a ng-click="viewUserDevice(user,'lg')"><span class="badge" ng-class="user.status == 1 ? 'badge-success' : 'badge-danger'">{{user.device.length}}</span></a>
                                    </td>
                                    <td>{{user.username}}</td>
                                    <td>{{user.email}}</td>
                                    <td>{{user.phone}}</td>
                                    <td>
                                        <i ng-show="user.status == 1" class="mdi mdi-check-circle-outline"></i>
                                        <i ng-show="user.status == 0" class="mdi mdi-close-circle-outline"></i>
                                    </td>
                                    <!-- <td>
                                         <div  class="btn-group btn-group-xs" role="group">
                                             <button type="button" class="btn btn-info" ng-click="editUser(user)">
                                                 <i class="fa fa-pencil"></i>
                                             </button>
                                             <button  type="button" class="btn btn-danger" confirmed-click="deleteUser(user)" ng-confirm-click="Would you like to delete this item?!">
                                                 <i class="fa fa-trash-o"></i>
                                             </button>

                                         </div>
                                     </td>-->
                                    <td>
                                        <div class="btn-group" ng-show="!trashed">
                                            <button type="button" class="btn btn-primary">Action</button>
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="btn" ng-click="enableUser(user)" ng-if="user.status == 0">Enable</i></a></li>
                                                <li><a class="btn" ng-click="disableUser(user)" ng-if="user.status == 1">Disable</a></li>
                                                <li><a class="btn" ng-click="editUser(user)">Edit</a></li>
                                                <li><a class="btn" ng-click="deleteUser(user)">Delete</a></li>
                                            </ul>
                                        </div>
                                        <div ng-show="trashed" class="btn-group btn-group-xs" role="group">
                                            <button  type="button" class="btn btn-danger" ng-click="deleteTrash(user)">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning" ng-click="restore(user)">
                                                <i class="fa fa-undo" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                Showing {{currentPage}} to {{(numPerPage < users.length  ? currentPage*numPerPage :users.length)}} of {{users.length}} entries
                            </div>
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                Showing {{(currentPage-1)*numPerPage+1}} to {{((currentPage*numPerPage) > users.length ? users.length :(currentPage*numPerPage))}} of {{users.length}} entries
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                                <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true">
                                </dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-show="users.length == 0" class="help-block text-center">
                    Nothing to display
                </div>
            </div>
        </div>
    </div>
</div>
