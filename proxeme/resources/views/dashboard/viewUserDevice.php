<div class="modal-header">
    <h3 class="modal-title" id="modal-title">{{items.name}}</h3>
</div>
<div class="modal-body" id="modal-body">
    <div class="row">
        <uib-tabset active="activeJustified" justified="true">
            <uib-tab index="0" heading="Online Devices({{items.deviceApi.length}})">
                <div class="row">
                    <div class="col-lg-4" dir-paginate="device in items.deviceApi | filter:search | limitTo:pageSize | itemsPerPage:6" pagination-id="userDevice">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{device.name}}</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <li class="list-group-item"> <span class="badge badge-primary">{{device.deviceId}}</span> Status</li>
                                    <li class="list-group-item"> <span class="badge badge-primary">{{device.battery}}</span> Status</li>
                                    <li class="list-group-item"> <span class="badge badge-danger">{{device.battery}}</span> Battery Status</li>
                                    <li class="list-group-item"> <span class="badge badge-warning">{{device.battery}}</span> Signal Level</li>
                                    <li class="list-group-item"> Current Location</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                        <dir-pagination-controls
                            max-size="5"
                            direction-links="true"
                            boundary-links="true"
                            pagination-id="userDevice">
                        </dir-pagination-controls>
                    </div>
                </div>
            </uib-tab>
            <uib-tab index="1" heading="Offine Devices({{items.deviceDb.length}})">
                <div class="row">
                    <div class="col-lg-4" dir-paginate="device1 in items.deviceDb | filter:search | limitTo:pageSize | itemsPerPage:6" pagination-id="userDevice1">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{device1.deviceId}}</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <li class="list-group-item"> <span class="badge badge-primary">{{device1.name}}</span> Name</li>
                                    <li class="list-group-item"> <span class="badge badge-danger">{{device1.status}}</span> Status</li>
                                    <li class="list-group-item"> <span class="badge badge-warning">{{device1.last_used_time | date: 'd-M-Y HH:MM'}}</span> Last Used</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                        <dir-pagination-controls
                            max-size="5"
                            direction-links="true"
                            boundary-links="true"
                            pagination-id="userDevice1">
                        </dir-pagination-controls>
                    </div>
                </div>
            </uib-tab>
        </uib-tabset>


    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
    <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
</div>