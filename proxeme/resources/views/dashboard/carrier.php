<div class="container">
    <div class="row" ng-show="showform">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="m-t-0 m-b-30">Add Carrier</h4>
                    <form class="form-horizontal" role="form" ng-submit="addCarrier()">
                        <div class="form-group" ng-class="{'has-error' : validationErrors.name}">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newcarrier.name" placeholder="Carrier Name">
                                <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">

                    <h4 class="m-b-30 m-t-0">
                        <a ng-click="loadCarriers()" style="cursor: pointer">{{title}}</a>
                        <a ng-click="newCarrier()" ng-show="!showform" style="cursor: pointer"ng-if="!trashed" class="btn btn-success">Add carrier <i class="mdi mdi-account-plus"></i></a>
                        <a ng-click="trash()" ng-show="!trashed" class="pull-right" style="cursor: pointer">Trash</a>
                        <a ng-click="loadCarriers()" class="pull-right" ng-show="trashed" style="cursor: pointer">Go back</a>
                    </h4>

                    <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer" ng-show="carriers.length > 0">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="dataTables_length" id="datatable_length">
                                    <label>Show
                                        <select name="datatable_length" aria-controls="datatable" class="form-control input-sm" ng-model="numPerPage"
                                                ng-options="num for num in paginations">{{num}}
                                        </select>
                                        entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
<!--                                <div class="dt-buttons btn-group">-->
<!--                                    <a class="btn btn-default buttons-copy buttons-html5 btn-success" tabindex="0" aria-controls="datatable-buttons"><span>Copy</span></a>-->
<!--                                    <a class="btn btn-default buttons-csv buttons-html5" tabindex="0" aria-controls="datatable-buttons"><span>CSV</span></a>-->
<!--                                    <a class="btn btn-default buttons-pdf buttons-html5" tabindex="0" aria-controls="datatable-buttons"><span>PDF</span></a>-->
<!--                                    <a class="btn btn-default buttons-print" tabindex="0" aria-controls="datatable-buttons"><span>Print</span></a>-->
<!--                                </div>-->
                            </div>
                            <div class="col-sm-4">
                                <div id="datatable_filter" class="dataTables_filter pull-right">
                                    <label>Search:
                                        <input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable" ng-model="search">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                                <thead>
                                <tr role="row">
                                    <th>Sl.No</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd"  dir-paginate="carrier in carriers | filter:search | itemsPerPage:numPerPage" current-page="currentPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{carrier.name}}</td>
                                    <td>
                                        <div class="btn-group btn-group-xs" role="group" ng-show="!trashed">
                                            <button type="button" class="btn btn-info" ng-click="editCarrier(carrier)">
<!--                                                <i class="fa fa-pencil"></i>-->
                                                Edit
                                            </button>
                                            <button  type="button" class="btn btn-danger" ng-click="deleteCarrier(carrier)">
<!--                                                <i class="fa fa-trash-o"></i>-->
                                                Delete
                                            </button>

                                        </div>
                                        <div ng-show="trashed" class="btn-group btn-group-xs" role="group">
                                            <button  type="button" class="btn btn-danger" ng-click="deleteTrash(carrier)">
<!--                                                <i class="fa fa-trash-o"></i>-->
                                                Delete
                                            </button>
                                            <button type="button" class="btn btn-warning" ng-click="restore(carrier)">
<!--                                                <i class="fa fa-undo" aria-hidden="true"></i>-->
                                                Restore
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                Showing {{currentPage}} to {{(numPerPage < carriers.length  ? currentPage*numPerPage :carriers.length)}} of {{carriers.length}} entries
                            </div>
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                Showing {{(currentPage-1)*numPerPage+1}} to {{((currentPage*numPerPage) > carriers.length ? carriers.length :(currentPage*numPerPage))}} of {{carriers.length}} entries
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                                <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true">
                                </dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-show="carriers.length == 0" class="help-block text-center">
                    Nothing to display
                </div>
            </div>
        </div>
    </div>
</div>