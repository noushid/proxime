<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <title>ProxiME | Dashboard</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta content="Admin Dashboard" name="description" />
      <meta content="ThemeDesign" name="author" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}">

      <script src="js/jquery.min.js"></script>
         <script src="js/bootstrap.min.js"></script>
         <script src="js/modernizr.min.js"></script>
         <script src="js/detect.js"></script>
         <script src="js/fastclick.js"></script>
         <script src="js/jquery.slimscroll.js"></script>
         <script src="js/jquery.blockUI.js"></script>
         <script src="js/waves.js"></script>
         <script src="js/wow.min.js"></script>
         <script src="js/jquery.nicescroll.js"></script>
         <script src="js/jquery.scrollTo.min.js"></script>

       <link href="{{ asset('plugins/morris/morris.css') }}" rel="stylesheet">
       <link href="{{ asset('css/app.css') }}" rel="stylesheet">

       <script src="{{ asset('js/intlTelInput.js')}}"></script>

       <script src="{{ asset('js/angular/angular-bootstrap.js')}}"></script>
       <script src="{{ asset('js/angular/angularApp.js')}}"></script>
        <script src="http://maps.google.com/maps/api/js"></script>
        <style>
            .file-error{
                border-color:red;
            }
            /* Put your css in here */
            .input-bar {
                display: table;
                width: 100%;
            }

            .input-bar-item {
                display: table-cell;
            }

            .input-bar-item > button {
                margin-left: 5px;
            }

            .width100 {
              width: 100%;
            }
        </style>
   </head>
   <body class="fixed-left" ng-app="proximeApp" ng-controller="DashboardController">
         <div id="wrapper">
            <div class="topbar">
               <div class="topbar-left">
                  <div class="text-center">
                       <div class="user-details">
                            <a href="#" class="logo page"><img src="images/logo.png"></a> <span></span>
                        </div>
               </div>
              </div>
               <div class="navbar navbar-default" role="navigation">
                  <div class="container">
                     <div class="">
                        <div class="pull-left"> <button type="button" class="button-menu-mobile open-left waves-effect waves-light"> <i class="ion-navicon"></i> </button> <span class="clearfix"></span></div>
                        <form class="navbar-form pull-left" role="search">
                           {{--<div class="form-group"> <input type="text" class="form-control search-bar" placeholder="Search..."></div>--}}
                           {{--<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>--}}
                        </form>
                        <ul class="nav navbar-nav navbar-right pull-right">
                           {{--<li class="dropdown hidden-xs">--}}
                              {{--<a data-target="#" class="dropdown-toggle waves-effect waves-light notification-icon-box" data-toggle="dropdown" aria-expanded="true"> <i class="fa fa-bell"></i> <span class="badge badge-xs badge-danger"></span> </a>--}}
                              {{--<ul class="dropdown-menu dropdown-menu-lg">--}}
                                 {{--<li class="text-center notifi-title">Notification <span class="badge badge-xs badge-success">3</span></li>--}}
                                 {{--<li class="list-group">--}}
                                    {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                       {{--<div class="media">--}}
                                          {{--<div class="media-heading">Your order is placed</div>--}}
                                          {{--<p class="m-0"> <small>Dummy text of the printing and typesetting industry.</small></p>--}}
                                       {{--</div>--}}
                                    {{--</a>--}}
                                    {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                       {{--<div class="media">--}}
                                          {{--<div class="media-body clearfix">--}}
                                             {{--<div class="media-heading">New Message received</div>--}}
                                             {{--<p class="m-0"> <small>You have 87 unread messages</small></p>--}}
                                          {{--</div>--}}
                                       {{--</div>--}}
                                    {{--</a>--}}
                                    {{--<a href="javascript:void(0);" class="list-group-item">--}}
                                       {{--<div class="media">--}}
                                          {{--<div class="media-body clearfix">--}}
                                             {{--<div class="media-heading">Your item is shipped.</div>--}}
                                             {{--<p class="m-0"> <small>It is a long established fact that a reader will</small></p>--}}
                                          {{--</div>--}}
                                       {{--</div>--}}
                                    {{--</a>--}}
                                    {{--<a href="javascript:void(0);" class="list-group-item"> <small class="text-primary">See all notifications</small> </a>--}}
                                 {{--</li>--}}
                              {{--</ul>--}}
                           {{--</li>--}}
                           <li class="hidden-xs"> <a id="btn-fullscreen" class="waves-effect waves-light notification-icon-box"><i class="mdi mdi-fullscreen"></i></a></li>
                            <li class="dropdown hidden-xs">
                              <a data-target="#" class="dropdown-toggle waves-effect waves-light notification-icon-box" data-toggle="dropdown" aria-expanded="true"> <i class="mdi mdi-account-circle"></i> Admin Name <br/> <small></small> </span>  </a>
                              <ul class="dropdown-menu">
                                 <li><a href="#profile"> Profile</a></li>
                                 {{--<li><a href="javascript:void(0)"><span class="badge badge-success pull-right">5</span> Settings </a></li>--}}
                                 {{--<li><a href="javascript:void(0)"> Lock screen</a></li>--}}
                                 <li class="divider"></li>
                                 <li>
                                     <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                         {{ csrf_field() }}
                                     </form>
                                    <a style="cursor: pointer " onclick="event.preventDefault();document.getElementById('logout-form').submit();"> Logout</a>
                                 </li>
                              </ul>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="left side-menu">
               <div class="sidebar-inner slimscrollleft">
                  <div class="user-details">
                     <div class="text-center"> </div>
                     <div class="user-info">
                        <div class="dropdown">
                           {{--<a href="#" class="dropdown-toggle text-capitalize"  aria-expanded="false">{{Auth::guard('admin')->user()->name}}</a>--}}
                           <a href="#" class="dropdown-toggle text-capitalize"  aria-expanded="false">@{{user.name}}</a>
                           <ul class="dropdown-menu">
                              <li><a href="javascript:void(0)"> Profile</a></li>
                              <li><a href="javascript:void(0)"> Settings</a></li>
                              <li><a href="javascript:void(0)"> Lock screen</a></li>
                              <li class="divider"></li>
                              <li><a href="javascript:void(0)"> Logout</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div id="sidebar-menu">
                     <ul>
                        {{--<li> <a href="#" class="waves-effect"><i class="mdi mdi-home"></i><span> Dashboard <span class="badge badge-primary pull-right">1</span></span></a></li>--}}
                        <li> <a href="#" class="waves-effect" ng-class="{'active' : currentRoute == '/'}"><i class="mdi mdi-home"></i><span> Dashboard</span></a></li>
                         @if( Auth::guard('admin')->user()->type == 'admin')
                            <li class="has_sub">
                               <a href="#sub-admin" class="waves-effect" ng-class="{'active' : currentRoute == '/sub-admin'}"><i class="mdi mdi-account-key"></i><span> Manage Sub Admin</span></a>
                            </li>
                        @endif
                        <li class="has_sub">
                           <a href="#devices" class="waves-effect" ng-class="{'active' : currentRoute == '/devices'}"><i class="mdi mdi-chip"></i> <span> Manage Device </span></a>
                        </li>

                        <li class="has_sub">
                           <a href="#carrier" class="waves-effect" ng-class="{'active' : currentRoute == '/carrier'}"><i class="mdi mdi-sim"></i><span>Manage Carrier</span><span class="pull-right"></span></a>

                        </li>
                        <li> <a href="#package" class="waves-effect" ng-class="{'active' : currentRoute == '/package'}"><i class="mdi mdi-package-variant package"></i><span> Subscriptions</span></a></li>
                       <!--  <li class="has_sub">
                           <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-table"></i><span> Tables </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                           <ul class="list-unstyled">
                              <li><a href="tables-basic.html">Basic Tables</a></li>
                              <li><a href="tables-datatable.html">Data Table</a></li>
                           </ul>
                        </li> -->
                        <li class="has_sub">
                           <a href="#users" class="waves-effect" ng-class="{'active' : currentRoute == '/users'}"><i class="mdi mdi-account-multiple"></i><span style="font-size: 14px;"> Manage Device User  </span><span class="pull-right"></span></a>

                        </li>

                        <!-- <li class="has_sub">
                           <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-map"></i><span> Maps </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                           <ul class="list-unstyled">
                              <li><a href="maps-google.html"> Google Map</a></li>
                              <li><a href="maps-vector.html"> Vector Map</a></li>
                           </ul>
                        </li> -->
                        <!-- <li> <a href="calendar.html" class="waves-effect"><i class="mdi mdi-calendar"></i><span> Calendar <span class="badge badge-primary pull-right">NEW</span></span></a></li>
                        <li class="has_sub">
                           <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-assistant"></i><span> Layouts </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                           <ul class="list-unstyled">
                              <li><a href="layouts-collapse.html">Menu Collapse</a></li>
                              <li><a href="layouts-smallmenu.html">Menu Small</a></li>
                              <li><a href="layouts-menu2.html">Menu Style 2</a></li>
                           </ul>
                        </li> -->
                        <!-- li class="has_sub">
                           <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-google-pages"></i><span> Pages </span><span class="pull-right"><i class="mdi mdi-plus"></i></span></a>
                           <ul class="list-unstyled">
                              <li><a href="pages-timeline.html">Timeline</a></li>
                              <li><a href="pages-invoice.html">Invoice</a></li>
                              <li><a href="pages-directory.html">Directory</a></li>
                              <li><a href="pages-login.html">Login</a></li>
                              <li><a href="pages-register.html">Register</a></li>
                              <li><a href="pages-recoverpw.html">Recover Password</a></li>
                              <li><a href="pages-lock-screen.html">Lock Screen</a></li>
                              <li><a href="pages-blank.html">Blank Page</a></li>
                              <li><a href="pages-404.html">Error 404</a></li>
                              <li><a href="pages-500.html">Error 500</a></li>
                           </ul>
                        </li> -->
                     </ul>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div class="content-page">
                <div class="content">
                    <div class="">
                        <div class="page-header-title">
                            {{--<h4 class="page-title">Dashboard</h4>--}}
                            <h4 class="page-title">
                            @if( Auth::check() )
                                Current user: {{ Auth::guard('admin')->user()->name }}
                            @endif
                            </h4>
                        </div>

                    </div>

                    <div class="page-content-wrapper">
                        <div ng-view></div>
                        {{--<div id="loading"></div>--}}

                    </div>
                </div>
            <footer class="footer"> © 2017 ProxiME - All Rights Reserved. </footer>
            </div>
         </div>


   {{--<script src="plugins/flot-chart/jquery.flot.min.js"></script>--}}
   {{--<script src="plugins/flot-chart/jquery.flot.time.js"></script>--}}
   {{--<script src="plugins/flot-chart/jquery.flot.tooltip.min.js"></script>--}}
   {{--<script src="plugins/flot-chart/jquery.flot.resize.js"></script>--}}

   {{--<script src="plugins/flot-chart/jquery.flot.pie.js"></script>--}}
   {{--<script src="plugins/flot-chart/jquery.flot.selection.js"></script>--}}
   {{--<script src="plugins/flot-chart/jquery.flot.stack.js"></script>--}}
   {{--<script src="plugins/flot-chart/jquery.flot.crosshair.js"></script>--}}
   {{--<script src="pages/flot.init.js"></script>--}}
   {{--<script src="js/app2.js"></script>--}}
   <script src="js/adminapp.js"></script>
</body>
   </html>