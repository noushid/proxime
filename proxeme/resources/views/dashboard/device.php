<div class="container" id="top">
    <div class="row" ng-show="showform" >
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="m-t-0 m-b-30">Add Device</h4>
                    <form class="form-horizontal" role="form" ng-submit="addDevice()">
<!--                        <div class="form-group" ng-class="{'has-error' : validationErrors.name}">-->
<!--                            <label class="col-md-2 control-label">Device Name</label>-->
<!--                            <div class="col-md-10">-->
<!--                                <input type="text" class="form-control" ng-model="newdevice.name" placeholder="Device Name" maxlength="15">-->
<!--                                <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>-->
<!--                            </div>-->
<!--                        </div>-->

                        <div class="form-group" ng-class="{'has-error' : validationErrors.deviceId}">
                            <label class="col-md-2 control-label">Device Id</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newdevice.deviceId" placeholder="Device Unique ID" maxlength="15">
                                <span class="help-block" ng-if="validationErrors.deviceId"><small>{{validationErrors.deviceId[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationErrors.model_no}">
                            <label class="col-md-2 control-label">Model No</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newdevice.model_no" placeholder="Model No" maxlength="15">
                                <span class="help-block" ng-if="validationErrors.model_no"><small>{{validationErrors.model_no[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-email">Carrier</label>
                            <div class="col-md-10">
                                <select name="" id="" class="form-control" ng-model="newdevice.carrier_id">
                                    <option value="">select</option>
                                    <option value="{{carrier.id}}" ng-repeat="carrier in carriers">{{carrier.name}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">

                    <h4 class="m-b-30 m-t-0">
                        <a ng-click="loadDevices()" style="cursor: pointer">{{title}}</a>
                        <a ng-click="newDevice()" ng-show="!showform" style="cursor: pointer" ng-if="!trashed" class="btn btn-success">Add Device <i class="mdi mdi-server-plus"></i></a>
                        <a ng-click="trash()" ng-show="!trashed" class="pull-right" style="cursor: pointer">Trash</a>
                        <a ng-click="loadDevices()" class="pull-right" ng-show="trashed" style="cursor: pointer">Go back</a>
                    </h4>

                    <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer" ng-show="devices.length > 0">
                        <div class="row">
                            <div class="col-lg-3 col-sm-12 col-xs-12 mb10">
                                <div class="dataTables_length" id="datatable_length">
                                    <label>Show
                                        <select name="datatable_length" aria-controls="datatable" class="form-control input-sm" ng-model="numPerPage"
                                                ng-options="num for num in paginations">{{num}}
                                        </select>
                                        Entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-12 col-xs-12 mb10">
                                <div class="dt-buttons btn-group">
                                    <a class="btn btn-default buttons-pdf buttons-html5 btn-success" tabindex="0" aria-controls="datatable-buttons" ng-click="exportToPdf()"><span>Export To PDF</span></a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-12 col-xs-12 mb10">
                                <form name="importForm" id="importForm" class="form-horizontal" enctype="multipart/form-data" ng-submit="importExcel()">
                                    <div class="form-group">
                                        <input type="file" class="filestyle" data-input="false" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(0px 0px 0px 0px);" ng-file-model="importfile">
                                        <div class="bootstrap-filestyle input-group">
                                            <span class="group-span-filestyle " tabindex="0">
                                                <label for="filestyle-1" class="btn btn-default mrw" ng-class="{'file-error' : fileError == true}">
                                                    <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span>
                                                    <span class="buttonText">{{(importfile.name ? importfile.name : 'Choose file')}}</span>
                                                </label>
                                            </span>
                                        </div>
                                        <button class="btn btn-success">Import</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-xs-12 mb10">
                                <a href="format/importformat.xls" target="_top" class="pull-right font-light" style="font-size: 13px;line-height: 29px;">Get Excel Format &nbsp;</a>
                                <div class="dataTables_filter">
                                    <label>
                                        <input type="search" class="form-control input-sm" placeholder="Search" aria-controls="datatable" ng-model="search">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                                <thead>
                                <tr role="row">
                                    <th>Sl.No</th>
                                    <th>Device ID</th>
                                    <th>Model No</th>
                                    <th>User Name</th>
                                    <th>Activation Time</th>
                                    <th ng-show="trashed">Deleted At</th>
                                    <th>Carrier</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd"  dir-paginate="device in devices | filter:search | itemsPerPage:numPerPage | orderBy : '-id'" current-page="currentPage">
                                <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                <td>{{device.deviceId}}</td>
                                <td>{{device.model_no}}</td>
                                <td><a ng-click="viewDevice(device,'lg')">{{device.user.name}}</a></td>
                                <td><span ng-if="device.activation_time">{{device.activation_time *1000 | date: 'dd-MM-yyyy h:mm:ss a'}}</span></td>
                                <td ng-show="trashed">{{device.deleted_at}}</td>
                                <td>{{device.carrier.name}}</td>
                                <td>
                                    <i ng-show="device.status == 1" class="mdi mdi-check-circle-outline"></i>
                                    <i ng-show="device.status == 0" class="mdi mdi-close-circle-outline"></i>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-xs" role="group" ng-show="!trashed">
                                        <button type="button" class="btn btn-info" ng-click="editDevice(device)">
<!--                                            <i class="fa fa-pencil"></i>-->
                                            Edit
                                        </button>
                                        <button type="button" class="btn btn-danger" ng-click="deleteDevice(device)">
<!--                                            <i class="fa fa-trash-o"></i>-->
                                            Delete
                                        </button>
                                    </div>

                                    <div ng-show="trashed" class="btn-group btn-group-xs" role="group">
                                        <button  type="button" class="btn btn-danger" ng-click="deleteTrash(device)">
<!--                                            <i class="fa fa-trash-o"></i>-->
                                            Delete
                                        </button>
                                        <button type="button" class="btn btn-warning" ng-click="restore(device)">
<!--                                            <i class="fa fa-undo" aria-hidden="true"></i>-->
                                            Restore
                                        </button>
                                    </div>


                        </div>
                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                Showing {{currentPage}} to {{(numPerPage < devices.length  ? currentPage*numPerPage :devices.length)}} of {{devices.length}} entries
                            </div>
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                Showing {{(currentPage-1)*numPerPage+1}} to {{((currentPage*numPerPage) > devices.length ? devices.length :(currentPage*numPerPage))}} of {{devices.length}} entries
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                                <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true">
                                </dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-show="devices.length == 0" class="help-block text-center">
                    Nothing to display
                </div>
            </div>
        </div>
    </div>
</div>
