<div class="container">
    <div class="row" ng-show="showform">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="m-t-0 m-b-30">Add Sub Admin</h4>
                    <form class="form-horizontal" role="form" ng-submit="addUser()">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newuser.name" placeholder="Name">
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationErrors.username}">
                            <label class="col-md-2 control-label">Username</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newuser.username" placeholder="Username">
                                <span class="help-block" ng-if="validationErrors.username"><small>{{validationErrors.username[0]}}</small></span>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationErrors.email}">
                            <label class="col-md-2 control-label" for="example-email">Email</label>
                            <div class="col-md-10">
                                <input type="email" name="email" class="form-control" placeholder="Email" ng-model="newuser.email">
                                <span class="help-block" ng-if="validationErrors.email"><small>{{validationErrors.email[0]}}</small></span>
                            </div>
                        </div>
                        <div class="form-group" ng-class="{'has-error' : validationErrors.phone}">
                            <label class="col-md-2 control-label">Phone</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newuser.phone" placeholder="Phone number with country code">
<!--                                <input type="text" ng-model="tel" ng-intl-tel-input>-->
<!--                                <input type="text" international-phone-number ng-model="newuser.phone">-->
<!--                                <input type="text" ng-model="model.tel" ng-intl-tel-input>-->
                                <span class="help-block" ng-if="validationErrors.phone"><small>{{validationErrors.phone[0]}}</small></span>
                            </div>
                        </div>
                        <div class="form-group" ng-class="{'has-error' : validationErrors.password}">
                            <label class="col-md-2 control-label">Password</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="newuser.password" placeholder="Password" style="width: 81%">
                                <span class="help-block" ng-if="validationErrors.password"><small>{{validationErrors.password[0]}}</small></span>
                                <button class="btn gp-button btn-warning" type="button" ng-click="newuser.password = createPassword()">generate password</button>
                            </div>
                        </div>
<!--                        <div class="form-group" ng-show="edit">-->
<!--                            <label class="col-md-2 control-label">Reset Password</label>-->
<!--                            <div class="col-md-10">-->
<!--                                <input type="checkbox" ng-model="newuser.reset_password" name="reset_password" checked="" />-->
<!--                                <span>password reset to 'admin'</span>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="form-group">
                            <div class="col-md-12 text-center">
<!--                                <button type="submit" class="btn btn-primary" ng-disabled="validationErrors.email == true || validation.username == true">Submit</button>-->
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">

                    <h4 class="m-b-30 m-t-0">
                        <a style="cursor: pointer">{{title}}</a>
                        <a ng-click="newUser()" ng-show="!showform" ng-if="!trashed" style="cursor: pointer" class="btn btn-success">Add Sub Admin <i class="mdi mdi-account-plus"></i></a>
                        <a ng-click="trash()" ng-show="!trashed" class="pull-right" style="cursor: pointer">Trash</a>
                        <a ng-click="loadUsers()" class="pull-right" ng-show="trashed" style="cursor: pointer">Go back</a>
                    </h4>

                    <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer" ng-show="users.length > 0">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length" id="datatable_length">
                                    <label>Show
                                        <select name="datatable_length" aria-controls="datatable" class="form-control input-sm" ng-model="numPerPage"
                                            ng-options="num for num in paginations">{{num}}
                                        </select>
                                        entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div id="datatable_filter" class="dataTables_filter pull-right">
                                    <label>Search:
                                        <input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable" ng-model="search">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                                <thead>
                                <tr role="row">
                                    <th>Sl.No</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th ng-show="trashed">Deleted At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd" dir-paginate="user in users | filter:search | limitTo:pageSize | itemsPerPage:numPerPage"  current-page="currentPage" ng-class="{'text-danger' : user.status==0}">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{user.name}}</td>
                                    <td>{{user.username}}</td>
                                    <td>{{user.email}}</td>
                                    <td>{{user.phone}}</td>
                                    <td>
                                        <i ng-show="user.status == 1" class="mdi mdi-check-circle-outline"></i>
                                        <i ng-show="user.status == 0" class="mdi mdi-close-circle-outline"></i>
                                    </td>
                                    <td>{{user.created_at}}</td>
                                    <td ng-show="trashed">{{user.deleted_at}}</td>
                                    <!-- <td>
                                         <div  class="btn-group btn-group-xs" role="group">
                                             <button type="button" class="btn btn-info" ng-click="editUser(user)">
                                                 <i class="fa fa-pencil"></i>
                                             </button>
                                             <button  type="button" class="btn btn-danger" confirmed-click="deleteUser(user)" ng-confirm-click="Would you like to delete this item?!">
                                                 <i class="fa fa-trash-o"></i>
                                             </button>

                                         </div>
                                     </td>-->
                                    <td>
                                        <div class="btn-group" ng-show="!trashed">
                                            <button type="button" class="btn btn-primary">Action</button>
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="btn" ng-click="enableUser(user)" ng-if="user.status == 0">Enable</i></a></li>
                                                <li><a class="btn" ng-click="disableUser(user)" ng-if="user.status == 1">Disable</a></li>
                                                <li><a class="btn" ng-click="editUser(user)">Edit</a></li>
                                                <li><a class="btn" ng-click="deleteUser(user)">Delete</a></li>
                                            </ul>
                                        </div>

                                        <div ng-show="trashed" class="btn-group btn-group-xs" role="group">
                                            <button  type="button" class="btn btn-danger" ng-click="deleteTrash(user)">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning" ng-click="restore(user)">
                                                <i class="fa fa-undo" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                Showing {{currentPage}} to {{(numPerPage < users.length  ? currentPage*numPerPage :users.length)}} of {{users.length}} entries
                            </div>
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                Showing {{(currentPage-1)*numPerPage+1}} to {{((currentPage*numPerPage) > users.length ? users.length :(currentPage*numPerPage))}} of {{users.length}} entries
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                                <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true">
                                </dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                </div>
                <div ng-show="users.length == 0" class="help-block text-center">
                    Nothing to display
                </div>
            </div>
        </div>
    </div>
</div>