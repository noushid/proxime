<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-body">

                    <h4 class="m-b-30 m-t-0">Devices <a ng-click="newDevice()" ng-show="!showform" style="cursor: pointer"><i class="mdi mdi-server-plus"></i></a> </h4>

                    <div id="datatable-buttons_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="dataTables_length" id="datatable_length">
                                    <label>Show
                                        <select name="datatable_length" aria-controls="datatable" class="form-control input-sm" ng-model="numPerPage"
                                                ng-options="num for num in paginations">{{num}}
                                        </select>
                                        entries
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div id="datatable_filter" class="dataTables_filter pull-right">
                                    <label>Search:
                                        <input type="search" class="form-control input-sm" placeholder="" aria-controls="datatable" ng-model="search">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable-buttons_info">
                            <thead>
                            <tr role="row">
                                <th>#</th>
                                <th>Name</th>
                                <th>Device ID</th>
                                <th>Activation Time</th>
                                <th>Carrier</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr role="row" class="odd"  dir-paginate="device in trash | filter:search | itemsPerPage:numPerPage">
                                <td>{{$index+1}}</td>
                                <td>{{device.id}}</td>
                                <td>{{device.deviceId}}</td>
                                <td>{{device.activation_time}}</td>
                                <td>{{device.carrier.name}}</td>
                                <td>{{device.status}}</td>
                                <td>
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button type="button" class="btn btn-info" ng-click="editDevice(device)">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button  type="button" class="btn btn-danger" confirmed-click="deleteDevice(device)" ng-confirm-click="Would you like to delete this item?!">
                                            <i class="fa fa-trash-o"></i>
                                        </button>

                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="col-sm-6">
                            <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite">
                                <!--                                Showing {{__default__currentPage}} to {{numPerPage}} of {{devices.length}} entries-->
                                Showing {{numPerPage}} of {{devices.length}} entries
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="dataTables_paginate paging_simple_numbers pull-right" id="datatable-buttons_paginate">
                                <dir-pagination-controls
                                    max-size="5"
                                    direction-links="true"
                                    boundary-links="true">
                                </dir-pagination-controls>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>