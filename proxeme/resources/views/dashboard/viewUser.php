<div class="modal-header">
    <h3 class="modal-title" id="modal-title">{{items.name}}</h3>
</div>
<div class="modal-body" id="modal-body">
    <div class="row">
        <div class="table table-responsive">
            <table id="datatable-buttons" class="table table-bordered">
                <tr>
                    <th>Name</th>
                    <td>{{items.name}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{items.email}}</td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td>{{items.phone}}</td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td>{{items.address}}</td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td>{{items.country}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
    <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
</div>