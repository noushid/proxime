<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <h4 class="m-t-0 m-b-30">Edit profile</h4>
                    <form class="form-horizontal" role="form" ng-submit="editUser()">
                        <div class="form-group" ng-class="{'has-error' :validationErrors.name }">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="user.name" placeholder="Name">
                                <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}!.</small></span>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationErrors.username}">
                            <label class="col-md-2 control-label">Username</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="user.username" placeholder="Username" ng-change="checkUsername(user.username)">
                                <span class="help-block" ng-if="validationErrors.username"><small>{{validationErrors.username[0]}}!.</small></span>
                            </div>
                        </div>

                        <div class="form-group" ng-class="{'has-error' : validationErrors.email}">
                            <label class="col-md-2 control-label" for="example-email">Email</label>
                            <div class="col-md-10">
                                <input type="email" name="email" class="form-control" placeholder="Email" ng-model="user.email" ng-change="checkEmail(user.email)">
                                <span class="help-block" ng-if="validationErrors.email"><small>{{validationErrors.email[0]}}</small></span>
                            </div>
                        </div>
                        <div class="form-group" ng-class="{'has-error' : validationErrors.phone}">
                            <label class="col-md-2 control-label">Phone</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" ng-model="user.phone" placeholder="Phone Number">
                                <span class="help-block" ng-if="validationErrors.phone"><small>{{validationErrors.phone[0]}}</small></span>
                            </div>
                        </div>
                        <div ng-show="user.type == 'admin'">
                            <div class="form-group">
                                <label class="col-md-2 control-label">password(If change)</label>
                                <div class="col-md-10">
                                    <input type="password" class="form-control" ng-model="user.password" placeholder="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Confirm password </label>
                                <div class="col-md-10">
                                    <input type="password" class="form-control" ng-model="user.confirmPassword" placeholder="Re-Enter password" ng-change="checkPassword()">
<!--                                    <span ng-show="user.password != user.confirmPassword">Password does match</span>-->
                                    <span ng-show="!match">Password does not match</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary" ng-disabled="validation.email == true || validation.username == true || user.password != user.confirmPassword">Submit</button>
<!--                                <button type="submit" class="btn btn-primary" ng-disabled="validation.email == true || validation.username == true || user.password != user.confirmPassword">Submit</button>-->
<!--                                <button type="button" class="btn btn-danger" ng-click="cancel()">Cancel</button>-->
                                <a class="btn btn-danger" href="#">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>