<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    protected $fillable = ['name', 'description', 'price', 'offer_price', 'duration', 'usage','featured'];
    use SoftDeletes;
}
