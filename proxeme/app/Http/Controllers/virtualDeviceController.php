<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator as Faker;

class virtualDeviceController extends Controller
{

    public function index(Faker $faker)
    {
        $data = [];
        for ($i = 1; $i <= 50; $i++) {
            $value['deviceId'] = $faker->unique()->numberBetween(100, 202);
            $value['longitude'] = $faker->longitude;
            $value['latitude'] = $faker->latitude;
            $value['signal'] = $faker->numberBetween(1, 5);
            $value['battery'] = $faker->numberBetween(1, 5);
            $value['status'] = $faker->numberBetween(0, 1);
            $data[] = $value;
        }
        return $data;
    }

    public function device(Request $request,Faker $faker)
    {
        $deviceId = explode(',', $request->deviceId);
        $data = [];
        $i = 0;

        if (sizeof($deviceId) == 1) {
            $data['deviceId'] = $deviceId;
            $data['longitude'] = $faker->longitude;
            $data['latitude'] = $faker->latitude;
            $data['signal'] = $faker->numberBetween(1, 5);
            $data['battery'] = $faker->numberBetween(1, 5);
            $data['status'] = $faker->numberBetween(0, 1);
        }else{
            foreach ($deviceId as $device) {
                $value['deviceId'] = $device;
                $value['longitude'] = $faker->longitude;
                $value['latitude'] = $faker->latitude;
                $value['signal'] = $faker->numberBetween(1, 5);
                $value['battery'] = $faker->numberBetween(1, 5);
                $value['status'] = $faker->numberBetween(0, 1);
                $data[] = $value;
                $i++;
                if ($i > 2) {
                    break;
                }
            }
        }
        return $data;
    }

}
