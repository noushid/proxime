<?php

namespace App\Http\Controllers\Auth;

use App\Carrier;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Device;

use Validator;
use Response;
use PDF;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Storage;
use Excel;

class DeviceController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'nullable|max:15',
            'deviceId' => 'required|unique:devices,deviceId|max:15'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->item == 'trash') {
            return Device::with('carrier')->with('user')->onlyTrashed()->orderBy('id','DESC')->get();
        }else{
            return Device::with('carrier')->with('user')->with('devicePackages.package')->orderBy('id', 'DESC')->get();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Validate the Request using own validation method
         *
         */
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $device = new Device($request->all());
        $device->status = 1;
        if ($device->save()) {
            return Device::with('carrier')->find($device->id);
//            return Device::with('carrier')->where('id', $device->id)->get();
        }
        return Response::json(['Error' => 'Server Down'], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Device::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Validate the Request using own validation method
         *
         */

        $validator = Validator::make($request->all(), [
            'deviceId' => 'required|max:15|unique:devices,deviceId,' . $request->id,
        ]);


        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $device = Device::find($id);
        if ($device->update($request->all())) {
            return Device::find($id);
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove temporarily the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if ($request->delete == 'permanent') {
            if (Device::where('id', $id)->forceDelete()) {
                return Response::json(array('msg' => 'Device record deleted'));
            }else{
                return Response::json(array('error' => 'Record Not found'), 400);
            }
        } else {
            if (Device::destroy($id)) {
                return Response::json(array('msg' => 'Device record deleted'));
            } else {
                return Response::json(array('error' => 'Record Not found'), 400);
            }
        }
    }

    /**
     * Get Total Device Count
     * @return \illuminate\http\Response
     *
    */
    public function getCount()
    {
        return Device::get()->count();
    }

    /**
     * Restore Trashed
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
    */

    public function restore(Request $request)
    {
        if (Device::withTrashed()->find($request->id)->restore()) {
            return Response::json(array('msg' => 'Device record restored'));
        }else{
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }


    public function exportToPdf()
    {
        $devices = Device::with('carrier')->with('user')->orderBy('id', 'DESC')->get();
        if ($devices->count() > 0) {
            $data = '<html><head><style>table, td, th {  border: 1px solid rgba(34, 36, 37, 0.91);  }  table {  border-collapse: collapse;  width: 100%;  }  th {  height: 50px;  }</style></head><body>';
            $data .= '<h2 style="text-align:center;"> Device List</h2>';
            $data .= '<h4 style="text-align:center;">' . Carbon::now()->format('d-m-Y') . '</h4><br/><h5 style="text-align:right;">' . date('d-M-Y h:i',time()) . '</h5><hr/>';
            $data .= '<h4 style="text-align:center;"><u> Device List</u></h4><br/>';
            $data .= '<table width="100%" border="0.5"><thead><tr><th>SlNo</th><th>Device Id</th><th>Model No</th><th>Username</th><th>Carrier</th><th>Status</th></tr></thead>';
            $data .= '<tbody>';
            $id = 1;
            foreach ($devices as $value) {
                $data .= '<tr><td>&nbsp;&nbsp;' . $id . '</td><td>' . $value->deviceId . '</td><td>' . $value->model_no. '</td><td>' . (isset($value->user->name) ? $value->user->name : '') . '</td><td>' . (isset($value->carrier->name) ? $value->carrier->name : '') . '</td><td>' . ($value->status == 1 ? 'Active' : 'Disable' ) . '</td></tr>';
                $id = $id + 1;
            }
//                $data .= '<tr style="font-size:20px;"><td colspan="4">Total Attendance</td><td>&nbsp;</td><td>&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>';
            $data .= '</tbody>';
            $data .= '</table><br/><br/>';


            $data .= '<div style="font-size:22px;"><hr>Total : ' . $devices->count() . '<hr></div>';
            $data .= '<br/><br/><h3 style="text-align:right;">Sign</h3>';
            $data .= '</body></html>';
        }


        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->output();
        return response($pdf->stream('List'), 200)->header('Content-Type', 'application/pdf');
    }

    public function addDeviceToUser(Request $request)
    {
        /**
         * Validate the Request using own validation method
         *
         */

//        $validator = $this->validator($request->all());
//        if ($validator->fails()) {
//            return Response::json($validator->errors(), 400);
//        }
        if (Device::where('deviceId', $request->deviceId)->first()->user_id == null) {
            if (DB::update('UPDATE devices SET user_id= '.$request->user_id .',activation_time='.time().' WHERE deviceId = "'.$request->deviceId.'"')) {
//                return Device::where('deviceId', $request->deviceId)->get();
                return User::with(['device' => function ($query) {
                }, 'device.devicePackages' => function ($query) {
                    $query->where('status', '=', 1);
                }])->find($request->user_id);
            }
            return Response::json(['error' => 'Server down'], 500);
        }else{
            return Response::json(['error' => 'Please Contact Admin'],400);
        }
    }


    /**
     * To import Form Excel
     *
    */
    public function import(Request $request){
        //validate the xls file
        $validator = Validator::make($request->all(), [
            'data' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $file = $this->getExcelFileFromDataUri($request->data);

        $filename = storage_path('app').'/'.$file;

        $data = Excel::load($filename, function ($reader) {
        })->get();

        //            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {}

        $logFile = time();
        /*
         * create Error log excel file
         * */
        Excel::create($logFile, function ($excel) {
            // Set the title
            $excel->setTitle('Following data are not imported');
            $excel->sheet('sheet1', function ($sheet) {
                $sheet->appendRow(['DEVICEID', 'NAME', 'CARRIER','ERROR']);
            });
        })->save('xlsx', storage_path('app/log'));

        /**
         * read data from loaded excel file
        */
        foreach ($data->toArray() as $key => $row) {
            $device = [];
            $device['deviceId'] = $row['deviceid'];
            $device['model_no'] = $row['model_no'];
            if ($row['carrier'] != null) {
                $carrier = Carrier::where('name', trim($row['carrier']))->get()->first();
                if ($carrier) {
                    $device['carrier_id'] = $carrier->id;
                }else{
                    $device['carrier_id'] = null;
                }
            }
            if(!empty($device)) {
                $validator = $this->validator($device);
                $error['msg'] = [];
                if ($validator->fails()) {
                    $error['status'] = 0;
                    $error['msg'][] = $device['deviceId'] . ' ->   ' . 'Already exist';
                    /*
                     * Prepare data to excel
                     * */
                    $temp = [];
                    $temp['DEVICEID'] = $device['deviceId'];
                    $temp['model_no'] = $device['model_no'];
                    $temp['carrier'] = $row['carrier'];
                    $temp['error'] = 'already exist';

                    /*
                     * Add a row t excel with non-inserted row*/
                    Excel::load(storage_path('app/log/') . $logFile . '.xlsx', function ($reader) use ($temp) {
                        $reader->sheet('sheet1', function ($sheet) use ($temp) {
                            $sheet->prependRow($temp);
                        });
                    })->save('xlsx', storage_path('app/log'));

                }else{
                    if (Device::insert($device)) {
                        $error['status'] = 1;
                        $error['msg'] = 'data_inserted';
                    }else{
                        $error['status'] = 0;
                        $error['msg'][] = $device['deviceId'] . ' ->   ' . 'Not Inserted';

                        $temp = [];
                        $temp['DEVICEID'] = $device['deviceId'];
                        $temp['model_no'] = $device['model_no'];
                        $temp['carrier'] = $row['carrier'];
                        $temp['error'] = 'Database insert failed';

                        /*
                         * Add a row t excel with non-inserted row*/
                        Excel::load(storage_path('app/log/') . $logFile . '.xlsx', function ($reader) use ($temp) {
                            $reader->sheet('sheet1', function ($sheet) use ($temp) {
                                $sheet->prependRow($temp);
                            });
                        })->save('xlsx', storage_path('app/log'));
                    }
                }
            }
        }

        /*
         *
         * remove uploaded excel file stored  */
        if (file_exists($filename)) {
            unlink($filename);
        }
        if ($error['status'] == 1) {
            if (file_exists(storage_path('app/log/') . $logFile . '.xlsx')) {
                unlink(storage_path('app/log/') . $logFile . '.xlsx');
            }
        }else{
            $error['logfile'] = $logFile . '.xlsx';
        }
        return Response::json($error);
    }


    /**
     * Get Excel from URI
     * Param datauri
     * @return Excel
    */
    private function getExcelFileFromDataUri($excel)
    {

        $fileName = '';
        try {
            if(strlen($excel) > 128) {
                list($ext, $data)   = explode(';', $excel);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($excel, 11, strpos($excel, ';')-11);
                $fileName = 'excel'.rand(11111,99999).'.xlsx';//.$mime_type;
                // file_put_contents('uploads/excel/'.$fileName, $data);

                Storage::disk('local')->put($fileName,$data);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    public function checkPackage($device_id)
    {
        $device = Device::where('id', $device_id)->with('current_device_package')->get()->first();
        if ($device->current_device_package_id == null) {
            return Response::json(false, 200);
        }
        return $device->current_device_package;
    }
}
