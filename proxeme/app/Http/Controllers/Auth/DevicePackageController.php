<?php

namespace App\Http\Controllers\Auth;

use App\DevicePackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\DevicePackagePackage;
use Response;
use Validator;

class DevicePackageController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'device_id' => 'required',
            'package_id' => 'required',
            'status' => 'required',
            'order_no' => 'required',

        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DevicePackage::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Validate the Request using own validation method
         *
         */
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $device_package = new DevicePackage($request->all());
        if ($device_package->save()) {
            return DevicePackage::find($device_package->id);
        }
        return Response::json(['Error' => 'Server Down'], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DevicePackage::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Validate the Request using own validation method
         *
         */

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $device_package = DevicePackage::find($id);
        if ($device_package->update($request->all())) {
            return DevicePackage::find($id);
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (DevicePackage::destroy($id)) {
            return Response::json(array('msg' => 'DevicePackage record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }
}
