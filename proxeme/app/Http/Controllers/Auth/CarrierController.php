<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Carrier;
use Response;
use Validator;


class CarrierController extends Controller
{


    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->item == 'trash') {
            return Carrier::onlyTrashed()->get();
        }else{
            return Carrier::get();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Validate the Request using own validation method
         *
        */
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $carrier = new Carrier($request->all());
        if ($carrier->save()) {
            return Carrier::find($carrier->id);
        }
        return Response::json(['Error' => 'Server Down'], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Carrier::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Validate the Request using own validation method
         *
        */

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $carrier = Carrier::find($id);
        if ($carrier->update($request->all())) {
            return Carrier::find($id);
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->delete == 'permanent') {
            if (Carrier::where('id', $id)->forceDelete()) {
                return Response::json(array('msg' => 'Carrier record deleted'));
            }else{
                return Response::json(array('error' => 'Record Not found'), 400);
            }
        }
        if (Carrier::destroy($id)) {
            return Response::json(array('msg' => 'Asset record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }


    /**
     * Restore Trashed
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function restore(Request $request)
    {
        if (Carrier::withTrashed()->find($request->id)->restore()) {
            return Response::json(array('msg' => 'Carrier record restored'));
        }else{
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

}
