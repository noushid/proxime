<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use App\User;
use Validator;
use Response;
use PDF;



class UserController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username',
            'phone' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:14|unique:users,phone',
            'password' => 'required',
//            'phone' => 'phone:AUTO,US',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->item == 'trash') {
            return User::where('type', $request->type)->with('device')->onlyTrashed()->get();
        }else
            return User::where('type', $request->type)->with('device')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Validate the Request using own validation method
         *
         */

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $password = $request->password;
        $request->merge(['password' => bcrypt($request->password)]);

        $user = new User($request->all());
        if ($user->save()) {
            if ($request->type == 'subAdmin') {
                $data = $request->all();
                $data['password'] = $password;
                Mail::send('email.adminCreate', $data, function ($message) use ($data) {
                    $message->to($data['email'], $data['name'])->cc(env('MAIL_ADMIN_EMAIL'))->subject('You where added to Admin');
                });
            }
            return User::find($user->id);
        }
        return Response::json(['Error' => 'Server Down'], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Validate the Request using own validation method
         *
         */
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'username' => 'required|unique:users,username,' . $request->id,
            'phone' => 'nullable:regex:/^([0-9\s\-\+\(\)]*)$/|max:14|unique:users,phone,' . $request->id,
        ]);

        if ($request->password) {
            $password = $request->password;
            $request->merge(['password' => bcrypt($request->password)]);
        }

//        if ($request->reset_password == true) {
//            $request->merge(['password' => bcrypt($request->password)]);
//        }
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }
        $user = User::find($id);


        if ($user->update($request->all())) {
            $data = $request->all();

            if ($request->type == 'subAdmin') {
                if ($request->action == 'enable') {
                    $data['password'] = $password;
                    Mail::send('email.adminEnable', $data, function ($message) use ($data) {
                        $message->to($data['email'], $data['name'])->cc(env('MAIL_ADMIN_EMAIL'))->subject('Account activated');
                    });
                }elseif ($request->action == 'disable') {
                    Mail::send('email.adminDisable', $data, function ($message) use ($data) {
                        $message->to($data['email'], $data['name'])->cc(env('MAIL_ADMIN_EMAIL'))->subject('Account suspended');
                    });
                }elseif($request->password){
                    $data['password'] = $password;
                    Mail::send('email.adminUpdate', $data, function ($message) use ($data) {
                        $message->to($data['email'], $data['name'])->cc(env('MAIL_ADMIN_EMAIL'))->subject('Update your profile by administrator');
                    });
                }
            } else {
                if ($request->password) {

                    $data['password'] = $password;
                    $data['name'] = $request->name;
                    $data['email'] = $request->email;
                    $data['username'] = $user->username;
                    $data['password'] = $password;
                    Mail::send('email.resetPasswordAlert', $data, function ($message) use ($data) {
                        $message->to($data['email'], $data['name'])->cc(env('MAIL_ADMIN_EMAIL'))->subject('ProxiME update profile');
                    });
                }
            }
            return User::find($id);
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data = User::where('id', $id)->withTrashed()->get()->first()->toArray();
        if ($request->delete == 'permanent') {
            if (User::where('id', $id)->forceDelete()) {
                return Response::json(array('msg' => 'User record deleted'));
            }else{
                return Response::json(array('error' => 'Record Not found'), 400);
            }
        }else{
            if (User::destroy($id)) {
                Mail::send('email.adminRemove', $data, function ($message) use ($data) {
                    $message->to($data['email'], $data['name'])->subject('Account Dismissed.');
                });
                return Response::json(array('msg' => 'User record deleted'));
            } else {
                return Response::json(array('error' => 'Record Not found'), 400);
            }
        }
    }


    /**
     * To check username is unique
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
    */

    public function checkUsername(Request $request)
    {
        return User::where('username', $request->username)->get()->count();
    }

    /**
     * To check Email is unique
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function checkEmail(Request $request)
    {
        return User::where('email', $request->email)->get()->count();
    }

    /**
     * Get Total User Count
     * @return \illuminate\http\Response
     *
     */
    public function getCount()
    {
        return User::where('type', 'public')->get()->count();
    }


    public function getActiveUserCount()
    {

        return User::where('type', 'public')->where('online', 1)->get()->count();
    }

    /**
     * Restore Trashed
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function restore(Request $request)
    {
        if (User::withTrashed()->find($request->id)->restore()) {
            return Response::json(array('msg' => 'User record restored'));
        }else{
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser($id)
    {
//        return User::where('id', $id)->with('device')->with(['device.devicePackages' => function($query){
//            $query->where('status' ,2)->first();
//        }])->first();
        return User::with(['device' => function($query){}, 'device.devicePackages' => function ($query){
            $query->where('status', '=', 1);
        }])->find($id);
    }

}
