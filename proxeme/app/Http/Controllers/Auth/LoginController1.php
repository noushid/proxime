<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Contracts\Auth\Guard;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = '/dashboard';
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    protected function redirectTo()
    {
        return '/path';
    }

    public function show()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'username' => 'required',
                'password' => 'required'
            ]
        );
        if ($validator->fails()) {
            return 'validation error';
        }

        if (Auth::attempt(['username' => $request->username,'password' =>$request->password,'status' => 1])) {
            return redirect('dashboard');
        }else{
            return redirect()->back()->withInput($request->only('username'))->withErrors(['errors', 'error']);
        }

    }

    public function logout(Request $request)
    {
//        Auth::guard('admin')->logout();
        Auth::logout();
    /*    $request->session()->flush();
        $request->session()->regenerate();*/
        //return redirect()->guest(route('login'));
        return redirect('admin/login');
    }
}
