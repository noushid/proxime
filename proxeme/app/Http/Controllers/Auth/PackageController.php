<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Response;
use App\Package;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;

class PackageController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'price' => 'required|numeric',
            'offer_price' => 'nullable|numeric',
            'duration' => 'nullable|numeric',
            'usage' => 'nullable|numeric'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->item == 'trash') {
            return Package::onlyTrashed()->get();
        }else
            return Package::orderBy('id','DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Validate the Request using own validation method
         *
         */
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $package = new Package($request->all());
        if ($package->save()) {
            return Package::find($package->id);
        }
        return Response::json(['Error' => 'Server Down'], 500);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Package::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Validate the Request using own validation method
         *
         */

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $package = Package::find($id);
        if ($package->update($request->all())) {
            return Package::find($id);
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if ($request->delete == 'permanent') {
            if (Package::where('id', $id)->forceDelete()) {
                return Response::json(array('msg' => 'Package record deleted'));
            }else{
                return Response::json(array('error' => 'Record Not found'), 400);
            }
        }else{
            if (Package::destroy($id)) {
                return Response::json(array('msg' => 'Package record deleted'));
            } else {
                return Response::json(array('error' => 'Record Not found'), 400);
            }
        }
    }

    /**
     * Restore Trashed
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function restore(Request $request)
    {
        if (Package::withTrashed()->find($request->id)->restore()) {
            return Response::json(array('msg' => 'Package record restored'));
        }else{
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }


    public function exportToPdf()
    {
        $devices = Package::orderBy('id','DESC')->get();
        if ($devices->count() > 0) {
            $data = '<html><head><style>table, td, th {  border: 1px solid rgba(34, 36, 37, 0.91);  }  table {  border-collapse: collapse;  width: 100%;  }  th {  height: 50px;  }</style></head><body>';
            $data .= '<h2 style="text-align:center;"> Device List</h2>';
            $data .= '<h4 style="text-align:center;">' . Carbon::now()->format('d-m-Y') . '</h4><br/><h5 style="text-align:right;">' . date('d-M-Y h:i',time()) . '</h5><hr/>';
            $data .= '<h4 style="text-align:center;"><u> Package List</u></h4><br/>';
            $data .= '<table width="100%" border="0.5"><thead><tr><th>SlNo</th><th>Name</th><th>Price</th><th>Offer price</th><th>Duration</th><th>Data</th><th>Featured</th><th>Description</th></tr></thead>';
            $data .= '<tbody>';
            $id = 1;
            foreach ($devices as $value) {
                $data .= '<tr><td>&nbsp;&nbsp;' . $id . '</td><td>' . $value->name . '</td><td>' . $value->price . '</td><td>' . $value->offer_price. '</td><td>' . $value->duration . '</td><td>' . $value->usage . '</td><td>' . ($value->featured == 1 ? 'Yes' : 'No' ) . '</td><td>' . $value->description . '</td></tr>';
                $id = $id + 1;
            }
//                $data .= '<tr style="font-size:20px;"><td colspan="4">Total Attendance</td><td>&nbsp;</td><td>&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>';
            $data .= '</tbody>';
            $data .= '</table><br/><br/>';


            $data .= '<div style="font-size:22px;"><hr>Total : ' . $devices->count() . '<hr></div>';
            $data .= '<br/><br/><h3 style="text-align:right;">Sign</h3>';
            $data .= '</body></html>';
        }


        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->output();
        return response($pdf->stream('List'), 200)->header('Content-Type', 'application/pdf');
    }

}
