<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\User;
use Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

//    public function validateAccount(Request $request)
//    {
//        if (filter_var($request->identity, FILTER_VALIDATE_EMAIL)) {
//
//        } else {
//            $user =User::where('phone', $request->identity)->get()->first();
//            if ($user) {
//                $phone = $request->identity;
//                $otp = mt_rand(10000, 99999);
//                $message = "Hi $user->name, Your OTP is $otp";
//                $sendOtp = sendOtp($phone, $otp, $message);
//                $result = json_decode($sendOtp);
//                if ($result->type == 'success') {
//                    $request->session()->put('user.otpTo', 'phone');
//                    $request->session()->put('user.otp', $otp);
//                    $request->session()->put('user.name', $user->name);
//                    $request->session()->put('user.phone', $phone);
//
//                    $request->session()->flash('message', 'OTP was send to ' . $phone);
//                    return redirect();
//                }
//            }else{
//                var_dump('not found');
//            }
//        }
//    }

    public function sendOtp(Request $request)
    {
        $phone = $request->phone;
        $phone = ltrim($phone, '+');

        $user = User::where('phone', $phone)->where('type', 'public')->get()->first();
        if ($user) {
            $otp = mt_rand(10000, 99999);
            $message = "Hi $user->name, Your OTP is $otp";
            $sendOtp = sendOtp($phone, $otp, $message);
            $result = json_decode($sendOtp);
            if ($result->type == 'success') {
                $request->session()->put('user.id', $user->id);
                $request->session()->put('user.otpTo', 'phone');
                $request->session()->put('user.otp', $otp);
                $request->session()->put('user.name', $user->name);
                $request->session()->put('user.phone', $phone);

                $request->session()->flash('message', 'OTP was send to ' . $phone);
                return redirect('password/phone/otp');
            }
        }else{
            return redirect('password/reset')->with('error', 'Invalid phone number.');
        }
    }

    public function showVerifyOtp()
    {
        return view('auth.passwords.verifyOtp');
    }

    public function verifyOtp(Request $request)
    {
        if (session('user.otp') == (int)$request->otp) {
            return redirect('password/reset/phone');
        }else{
            return redirect('password/phone/otp');
        }
    }

    public function resetPassword()
    {
        return view('auth.passwords.resetPhone');
    }

    public function resetPasswordPhone(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6|max:8',
            'password_confirmation' => 'required|same:password',
        ]);

        $password = $request->password;
        $data['password'] = bcrypt($password);
        $user_id = session()->get('user.id');
        if (User::where('id' ,$user_id)->update($data)) {
//            Mail::send('email.loginDetails', $data, function ($message) use ($data) {
//                $message->to($data['email'], $data['name'])->subject('proxime login details');
//            });
            return redirect('login');
        }else{
            return redirect('password.reset');
        }
    }

}
