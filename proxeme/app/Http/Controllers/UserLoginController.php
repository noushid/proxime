<?php
//http://www.expertphp.in/article/how-to-implement-multi-auth-in-laravel-5-4-with-example

namespace App\Http\Controllers;

use App\CardDetail;
use App\Device;
use App\Helper\RandomColor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Response;
use Session;
use Hash;
use Symfony\Component\HttpFoundation\Cookie;
use Validator;
use App\DevicePackage;
use App\Package;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Cache;


class UserLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {
        $prices = Package::simplePaginate(4);
//        $prices->withPath('home#pricing');
        return View::make("app.index")->with(array('packages' => $prices));
    }

    public function getUserLogin()
    {
        return view('app/login');
    }

    public function userAuth(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);
        $remember = ($request->remember) ? true : false;
        if (auth()->attempt(['username' => $request->input('username'), 'password' => $request->input('password'),'type' => 'public','status' => 1],$remember))
        {
            return redirect('map');
        }else{
            return redirect()->back()
                ->withInput($request->only('username', 'remember'))
                ->withErrors([
                    'error' => 'The Username or password invalid',
                ]);
        }
    }

    public function signup()
    {
        return view('app/signup');
    }

    public function createUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email|unique:users,username',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|min:6|max:10',
            'confirm_password' => 'required|same:password',
        ]);
        if (session('user.otp') == (int)$request->otp) {
//            $password = str_random(5);
            $password = $request->password;
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['username'] = $request->email;
            $data['type'] = 'public';
            $data['password'] = bcrypt($password);
            $data['type'] = 'public';
            $data['status'] = '1';
            $data['phone'] = str_replace(['+', ' '], '', $request->phone);
            $user = new User($data);
            $user->img='http://afsti.org/event_photos/default.png';
            if ($user->save()) {
                $data['password'] = $password;

                 //For send login details to mail
                Mail::send('email.loginDetails', $data, function ($message) use ($data) {
                    $message->to($data['email'], $data['name'])->subject('ProxiME login details');
                });

                sendsms($data['phone'], "Thank you for registration. your username :" . $data['username'] . " , password  : " . $password . ", click here " . url('/login'));
                /*if (Mail::failures()) {
                    sendsms($data['phone'], "Thank you for registration. your username :" . $data['username'] . " , password  : " . $password . ", click here " . url('/login'));
                }*/

//                return redirect('login')
//                    ->with([
//                        'message' => 'The Login details send to your registered email address. Please check your mail'
//                    ]);

                if (auth()->attempt(['email' => $request->email, 'password' => $password,'type' => 'public']))
                {
//                    $data['password'] = $password;
//                    Mail::send('email.loginDetails', $data, function ($message) use ($data) {
//                        $message->to($data['email'], $data['name'])->subject('proxime login details');
//                    });
//                    sendsms($data['phone'], "Thank you for registration. your username :" . $data['username'] . " , password  : " . $password . ", click here " . url('/login'));
                    /*if (Mail::failures()) {
                        sendsms($data['phone'], "Thank you for registration. your username :" . $data['username'] . " , password  : " . $password . ", click here " . url('/login'));
                    }*/
                    return redirect('map');
                }else{
//                    $data['password'] = $password;
//                    Mail::send('email.loginDetails', $data, function ($message) use ($data) {
//                        $message->to($data['email'], $data['name'])->subject('proxime login details');
//                    });
//                    sendsms($data['phone'], "Thank you for registration. your username :" . $data['username'] . " , password  : " . $password . ", click here " . url('/login'));
/*                    if (Mail::failures()) {
                        sendsms($data['phone'], "Thank you for registration. your username :" . $data['username'] . " , password  : " . $password . ", click here " . url('/login'));
                    }*/
                    return redirect('login');
                }
            }else{
                return redirect('signup')->with('something went wrong!');
            }
        }else{
            return redirect('signup')->with(['error' => 'OTP Not Match']);
        }

    }







    public function changeMyProfile(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $name=$request->name;
        $email=$request->email;
        $username=$request->username;
        $phone=$request->phone;

        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            $user->name=$name;
            $user->username=$username;
            $user->email=$email;
            $user->phone=$phone;
            if($user->save())
                return $user;
        }
        return Response::json( ['error'=>'Server Down'],400);
    }



    public function confirm()
    {
        return view('app/confirm');
    }

    public function sendOtp()
    {
        // Sendind a message with options
        $deviceID = 1;
        $number = '+918089183628';
        $message = 'Hello World from Laravel!';

        $options = [
            'send_at' => strtotime('+10 minutes'), // Send the message in 10 minutes
            'expires_at' => strtotime('+1 hour') // Cancel the message in 1 hour if the message is not yet sent
        ];

//        $message =  SMSGateway::sendMessageToNumber($number, $message, $deviceID, $options);


// Sendind a message instantly (without options)
        $deviceID = 1;
//        $number = '+44771232343';
        $message = 'Hello World from Laravel!';

        $message =  SMSGateway::sendMessageToNumber($number, $message, $deviceID);
    }

    public function logout(Request $request)
    {
        auth()->guard('web')->logout();
//        $request->session()->flush();
        $request->session()->regenerate();
        Cache::flush();
        return redirect()->route('user.auth');

    }

    public function savingDevices(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $device1=$request->device1;
        $device2=$request->device2;
        $device3=$request->device3;

        $error=[];
        $success=[];
        $i=0;

        $count=0;

        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {

            if($device1!=null) {

                $deviceFirst = Device::where('deviceId', $device1)->where('user_id', null)->get()->first();
                if ($deviceFirst != null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $deviceFirst->name='Device'.$deviceCount;
                    $deviceFirst->user_id = $user_id;
                    $deviceFirst->color = RandomColor::one();
                    $deviceFirst->activation_time=Carbon::now();
                    $deviceFirst->save();
                } else {
                    $error[$i] = $device1 . ' not available';
                    $i++;
                    $count++;
                }
            }

            if($device2!=null) {
                $deviceSecondt = Device::where('deviceId', $device2)->where('user_id', null)->get()->first();
                if ($deviceSecondt != null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $deviceSecondt->name='Device'.$deviceCount;
                    $deviceSecondt->user_id = $user_id;
                    $deviceSecondt->color = RandomColor::one();
                    $deviceSecondt->activation_time=Carbon::now();
                    $deviceSecondt->save();
                } else {
                    $error[$i] = $device2 . ' not available';
                    $i++;
                    $count++;
                }
            }

            if($device3!=null) {
                $deviceThird = Device::where('deviceId', $device3)->where('user_id', null)->get()->first();
                if ($deviceThird != null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $deviceThird->name='Device'.$deviceCount;
                    $deviceThird->user_id = $user_id;
                    $deviceThird->color = RandomColor::one();
                    $deviceThird->activation_time=Carbon::now();
                    $deviceThird->save();
                } else {
                    $error[$i] = $device3 . ' not available';
                    $i++;
                    $count++;
                }
            }
            if ($count!=0){
                $deviceFirst1 = Device::where('deviceId', $device1)->where('user_id', $user_id)->get()->first();
                if($deviceFirst1) {
                    $deviceFirst1->user_id = null;
                    $deviceFirst1->save();
                }

                $deviceFirst2 = Device::where('deviceId', $device2)->where('user_id', $user_id)->get()->first();
                if($deviceFirst2) {
                    $deviceFirst2->user_id = null;
                    $deviceFirst2->save();
                }

                $deviceFirst3 = Device::where('deviceId', $device3)->where('user_id', $user_id)->get()->first();
                if($deviceFirst3) {
                    $deviceFirst3->user_id = null;
                    $deviceFirst3->save();
                }

                return Response::json(['error' => 'Invalid Device'], 400);
            }
            else{
                return "Okkkk";
            }

        }

    }

    public function savingDeviceEach(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $device1=$request->device1;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            if ($device1 != null) {
                $device=Device::where('deviceId',$device1)->where('user_id',null)->get()->first();
                if($device!=null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $device->name='Device'.$deviceCount;
                    $device->user_id = $user_id;
                    $device->color = RandomColor::one();
                    $device->activation_time=Carbon::now();
                    if ($device->save())
                        return $device;
                }
            }
        }
    }

    private function getDeviceCount($user_id){
        $count=Device::where('user_id',$user_id)->count();
        $count=$count+1;
        if($count<10)
            $count='0'.$count;
        return $count;
    }



    public function sendResetLinkEmailForMobile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if( ! $validator->fails() )
        {
            if( $user = User::where('email', $request->input('email') )->first() )
            {
                $token = str_random(64);

                DB::table('password_resets')->insert([
                    'email' => $user->email,
                    'token' => $token
                ]);

                Mail::send([
                    'to'      => $user->email,
                    'subject' => 'Your Password Reset Link',
                    'view'    => config('auth.passwords.users.email'),
                    'view_data' => [
                        'token' => $token,
                        'user'  => $user
                    ]
                ]);

                return "OK";
            }
        }

        return Response::json( ['error'=>'Server Down'],400);
    }
}
