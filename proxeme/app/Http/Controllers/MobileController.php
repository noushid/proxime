<?php

namespace App\Http\Controllers;

use App\DeviceHistory;
use App\Support;
use App\User;
use App\CardDetail;
use App\Device;
use App\Helper\RandomColor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\URL;
use Response;
use Session;
use Hash;
use Validator;

class MobileController extends Controller
{
    use SendsPasswordResetEmails;

    public function registerUser(Request $request){
        $user=User::whereIn('id',[1,2])->get();
        return $user;
    }

    /**
     * Generate OTP For Mobile
     * @param illuminate\http\request
     * @return response
     */
    public function generateOtpForMobile(Request $request)
    {
        $phone = str_replace(['+', ' '], '', $request->phone);
        $request->merge(['phone' => $phone]);
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users,email|unique:users,username',
            'phone' => 'required|unique:users,phone',
        ]);
        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }

        $myphone=substr($phone,2);
        $checkPhoneExist=User::where('phone','Like',$myphone)->get();
        if(count($checkPhoneExist)>0){
            return Response::json( ['error'=>'The phone has already been taken.'],400);
        }

        $user = $request->name;
        $email = $request->email;
        $phone = str_replace(['+', ' '], '', $request->phone);
        $otp = mt_rand(10000, 99999);
        $message = "Hi $user, Your verification passcode is $otp.";
        $sendOtp = sendOtp($phone, $otp, $message);
        $result = json_decode($sendOtp);
        if ($result->type == 'success') {
            //Session::put('mobileOtp', $otp);
            return response($otp);
        }
    }

    /**
     * Register User through Mobile................................
     * */
    public function createUserForMobile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required|min:6|max:10',
            'email' => 'required|email|unique:users,email|unique:users,username',
            'phone' => 'required|unique:users,phone',
        ]);

        $password = $request->password;
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['username'] = $request->email;
        $data['password'] = bcrypt($password);
        $data['type'] = 'public';
        $data['status'] = '1';
        $data['phone'] = str_replace(['+', ' '], '', $request->phone);
        $data['secret_key'] = str_random(20);

        $user = new User($data);
        $user->img='http://afsti.org/event_photos/default.png';
        if ($user->save()) {
            $data['password'] = $password;
            Mail::send('email.loginDetails', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['name'])->subject('ProxiME Login Details');
            });
            sendsms($data['phone'], "Thank you for registration. your username :" . $data['username'] . " , password  : " . $password . ", click here " . url('/login'));
            return $user;
           // return User::find($user->id);
        }else{
            return Response::json( ['error'=>'Server Down'],400);
        }

    }

    public function getDeviceHistory(Request $request){
        $device_id=$request->device_id;
        //return DeviceHistory::where('device_id',$device_id)->get();
        $histories= DeviceHistory::where('device_id',$device_id)->get();
        foreach($histories as $history){
            $history['time']=Carbon::parse($history->datetime)->format('d F,Y');
        }

        return $histories;
    }

    public function removeMyPhtoto(Request $request){
        $secret_key=$request->secret_key;
        $user_id=$request->user_id;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user){
            $user->img='http://afsti.org/event_photos/default.png';
            if($user->save()){
                return $user;
            }
        }
    }

    /**
     * SAVING CARD DETAILS THROUGH MOBILE................
     */
    public function registerCardDetails(Request $request){

        /*$this->validate($request, [
            'secret_key' => 'required',
            'user_id' => 'required',
            'nameoncard' => 'required',
            'expirydate' => 'required',
        ]);*/
        $secret_key=$request->secret_key;
        $user_id=$request->user_id;
        $cardnumber=$request->cardnumber;
        $nameoncard=$request->nameoncard;
        $expirydate=$request->expirydate;
        $cardId=$request->cardId;

        if($cardId==null) {

            $user = User::where('id', $user_id)->where('secret_key', $secret_key)->get()->first();
            if ($user) {
                $card = new CardDetail();
                $card->user_id = $user_id;
                $card->cardnumber = $cardnumber;
                $card->nameoncard = $nameoncard;
                $card->expirydate = $expirydate;
                if ($card->save()) {
                    return $card;
                }
            }
        }else{
            $user = User::where('id', $user_id)->where('secret_key', $secret_key)->get()->first();
            if ($user) {
                $card =CardDetail::findOrFail($cardId);
                if($card!=null) {
                    $card->user_id = $user_id;
                    $card->cardnumber = $cardnumber;
                    $card->nameoncard = $nameoncard;
                    $card->expirydate = $expirydate;
                    if ($card->save()) {
                        return $card;
                    }
                }
            }
        }

        //return 'Ok';
    }

    public function savingDevices(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $device1=$request->device1;
        $device2=$request->device2;
        $device3=$request->device3;

        $error=[];
        $success=[];
        $i=0;

        $count=0;

        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {

            if($device1!=null) {

                $deviceFirst = Device::where('deviceId', $device1)->where('user_id', null)->get()->first();
                if ($deviceFirst != null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $deviceFirst->name='Device'.$deviceCount;
                    $deviceFirst->user_id = $user_id;
                    $deviceFirst->color = RandomColor::one();
                    $deviceFirst->activation_time=time();
                    $deviceFirst->save();
                } else {
                    $error[$i] = $device1 . ' not available';
                    $i++;
                    $count++;
                }
            }

            if($device2!=null) {
                $deviceSecondt = Device::where('deviceId', $device2)->where('user_id', null)->get()->first();
                if ($deviceSecondt != null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $deviceSecondt->name='Device'.$deviceCount;
                    $deviceSecondt->user_id = $user_id;
                    $deviceSecondt->color = RandomColor::one();
                    $deviceSecondt->activation_time=time();
                    $deviceSecondt->save();
                } else {
                    $error[$i] = $device2 . ' not available';
                    $i++;
                    $count++;
                }
            }

            if($device3!=null) {
                $deviceThird = Device::where('deviceId', $device3)->where('user_id', null)->get()->first();
                if ($deviceThird != null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $deviceThird->name='Device'.$deviceCount;
                    $deviceThird->user_id = $user_id;
                    $deviceThird->color = RandomColor::one();
                    $deviceThird->activation_time=time();
                    $deviceThird->save();
                } else {
                    $error[$i] = $device3 . ' not available';
                    $i++;
                    $count++;
                }
            }
            if ($count!=0){
                $deviceFirst1 = Device::where('deviceId', $device1)->where('user_id', $user_id)->get()->first();
                if($deviceFirst1) {
                    $deviceFirst1->user_id = null;
                    $deviceFirst1->save();
                }

                $deviceFirst2 = Device::where('deviceId', $device2)->where('user_id', $user_id)->get()->first();
                if($deviceFirst2) {
                    $deviceFirst2->user_id = null;
                    $deviceFirst2->save();
                }

                $deviceFirst3 = Device::where('deviceId', $device3)->where('user_id', $user_id)->get()->first();
                if($deviceFirst3) {
                    $deviceFirst3->user_id = null;
                    $deviceFirst3->save();
                }

                return Response::json(['error' => 'Invalid Device'], 400);
            }
            else{
                return "Okkkk";
            }

        }

    }

    public function savingDeviceEach(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $device1=$request->device1;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            if ($device1 != null) {
                $device=Device::where('deviceId',$device1)->where('user_id',null)->get()->first();
                if($device!=null) {
                    $deviceCount=$this->getDeviceCount($user_id);
                    //$deviceCount=$deviceCount+1;
                    $device->name='Device'.$deviceCount;
                    $device->user_id = $user_id;
                    $device->color = RandomColor::one();
                    $device->activation_time=time();
                    if ($device->save())
                        return $device;
                }
            }
        }
    }

    private function getDeviceCount($user_id){
        $count=Device::where('user_id',$user_id)->count();
        $count=$count+1;
        if($count<10)
            $count='0'.$count;
        return $count;
    }

    public function getLogin(Request $request){
        $username=$request->username;
        $password=$request->password;

        $userexist=User::where('username',$username)->get()->first();
        if($userexist!=null){
            if($userexist->checkPassword($password)){
                if($userexist->type=='public') {
                    if ($userexist->secret_key == null) {
                        $userexist->secret_key = str_random(20);
                        $userexist->save();
                    }
                    return $userexist;
                }
            }
        }
        return Response::json( ['error'=>'Server Down'],400);

        /*if(Hash::check('admin','$2y$10$Wxb.0118pERUySbck08Qoey5AUPr.psE8vDy4UhTpMGPy/xRzvDAy'))
            return "SAME";

        return decrypt('$2y$10$Wxb.0118pERUySbck08Qoey5AUPr.psE8vDy4UhTpMGPy/xRzvDAy');*/
    }

    public function registerSupport(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $question=$request->question;
        $message=$request->message;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            $support=new Support();
            $support->question=$question;
            $support->message=$message;
            $support->user_id=$user_id;
            if($support->save())
                return $support;
        }
    }

    public function getMyDevices(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            return Device::where('user_id',$user_id)->get();
        }
    }

    public function changeDeviceName(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $deviceId=$request->deviceId;
        $name=$request->name;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            $device=Device::find($deviceId);
            if($device){
                $device->name=$name;
                if($device->save())
                    return $device;
            }
        }
    }

    public function getLastRegisteredCardDetails(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            return CardDetail::where('user_id',$user_id)->get();
        }
    }

    public function getLastRegisteredCardDetailsById(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $id=$request->id;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            return CardDetail::where('id',$id)->get()->first();
        }
    }

    public function changeMyProfile(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'nullable|unique:users,email,' . $request->user_id, '|email',
            //'phone' => 'nullable|unique:users,phone',
        ], [
            'phone.unique' => 'Phone number already registered.',
            'email.unique' => 'Email id already registered.',
        ]);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $name=$request->name;
        $email=$request->email;
        $username=$request->username;
        $phone=$request->phone;

        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();

        if($user) {
            $curemail = $user->email;
            if($curemail!=$email){
                $token = str_random(30);

                $url = URL::to('update-profile/verify/' . $request->email . '/' . $token . '/' .$user_id);
                $user->name = $name;
                $user->username = $username;
                $user->update_email = $email;
                $user->email_token = $token;
                $user->phone = $phone;
                if ($user->save()) {
                    $data['user'] = $user->name;
                    $data['email'] = $request->email;
                    $data['verification_link'] = $url;
                   /* Mail::send('email.verify', $data, function ($message) use ($data) {
                        $message->to($data['email'], $data['user'])->subject('proxime verification');
                    });*/
                    $subject='ProxiME Verification';
                    Mail::send('email.verifyphone', ['data' => $data], function ($message) use ($subject,$email) {
                        $message->from('no-reply@surebot.com', 'ProxiME');
                        $message->to($email, '')->subject($subject);
                    });
                    return $user;
                }
            }else {
                $user->name = $name;
                $user->username = $username;
                //$user->email = $email;
                $user->phone = $phone;
                if ($user->save())
                    return $user;
            }
        }
        return Response::json( ['error'=>'Server Down'],400);
    }

    public function changeAppPassword(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $curpassword=$request->curpassword;
        $newpassword=$request->newpassword;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            if($user->checkPassword($curpassword)){
                $user->password=bcrypt($newpassword);
                if($user->save())
                    return $user;
            }else{
                return Response::json( ['error'=>'Server Down'],400);
            }
        }else{
            return Response::json( ['error'=>'Server Down'],400);
        }
    }

    /**
     * Method For Generate PayU Hash for Mobile Application
     */
    public function generateHash(Request $request){
        $key=$request->key;
        $salt='C38e04t9rW';
        $txnId=$request->txnid;
        $amount=$request->amount;
        $productName=$request->productinfo;
        $firstName=$request->firstname;
        $email=$request->email;
        $udf1=$request->udf1;
        $udf2=$request->udf2;
        $udf3=$request->udf3;
        $udf4=$request->udf4;
        $udf5=$request->udf5;

        $payhash_str = $key . '|' . $this->checkNull($txnId) . '|' .$this->checkNull($amount)  . '|' .$this->checkNull($productName)  . '|' . $this->checkNull($firstName) . '|' . $this->checkNull($email) . '|' . $this->checkNull($udf1) . '|' . $this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '|' . $salt;

        $hash = strtolower(hash('sha512', $payhash_str));
        $arr['payment_hash'] = $hash;
        $arr['status']=0;
        $arr['errorCode']=null;
        $arr['responseCode']=null;
        $arr['hashtest']=$payhash_str;
        $output=$arr;

        return $output;
    }

    /**
     * Upload Picture from Server....................
     */
    public function uploadMyProfile(Request $request){


        $filename = time().'.'.request()->pic->getClientOriginalExtension();
        request()->pic->move(public_path('files'), $filename);

        $user_id=$request->user_id;
        $user=\App\User::find($user_id);
        if($user){
            $user->img=url('files/'.$filename);
            if($user->save())
                return $user;
        }
    }

    public function getMyProfileInfo(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            return $user;
        }
    }

    public function testing(){
        return User::find(1);
    }

    public function getLastLoginUser(Request $request){
        $phone=$request->phone;
        return User::where('phone',$phone)->get()->last();
    }

    /**
     * Reset Password Link Generator for Mobile
     */
    public function sendResetLinkEmailForMobile(Request $request){
        $email=$request->email;
        $user=User::where('email',$email)->get()->first();
        if($user==null){
            return Response::json( ['error'=>'No Email Found'],400);
        }

        $this->sendResetLinkEmail($request);
    }

    /**
     * Change Phone Number OTP Generator..................
     */
    public function generateChangePhoneOtp(Request $request){

        $phone=$request->phone;
        $otp = mt_rand(10000, 99999);
        $message = "Hi, Your verification passcode is $otp.";
        $sendOtp = sendOtp($phone, $otp, $message);
        $result = json_decode($sendOtp);
        if ($result->type == 'success') {
            //Session::put('mobileOtp', $otp);
            return response($otp);
        }else{
            return response('OTP Sending failed', 400);
        }
    }

    /**
     * Change Phone Number OTP Generator..................
     */
    public function generateChangePhoneOtpWithValidation(Request $request){

        $validator = Validator::make($request->all(), [
            'phone' => 'required|unique:users,phone',
        ], [
            'phone.unique' => 'Phone number already registered.',
        ]);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $phone=$request->phone;
        $otp = mt_rand(10000, 99999);
        $message = "Hi, Your verification passcode is $otp.";
        $sendOtp = sendOtp($phone, $otp, $message);
        $result = json_decode($sendOtp);
        if ($result->type == 'success') {
            //Session::put('mobileOtp', $otp);
            return response($otp);
        }
    }

    /**
     * Change My Phone Number
     */
    public function changeMyPhone(Request $request){
        $user_id=$request->user_id;
        $secret_key=$request->secret_key;
        $phone=$request->phone;
        $user=User::where('id',$user_id)->where('secret_key',$secret_key)->get()->first();
        if($user) {
            $user->phone=$phone;
            if($user->save())
                return $user;
        }else{
            return Response::json( ['error'=>'Please Enter Registered Phone Number'],400);
        }
    }

    /**
     * OTP Generator for reset password.
     */
    public function generateChangePasswordOtp(Request $request){
        $phone=$request->phone;
        $user = User::where('phone', $phone)->get()->first();
        if($user!=null) {
            $otp = mt_rand(10000, 99999);
            $message = "Hi, Your verification passcode is $otp.";
            $sendOtp = sendOtp($phone, $otp, $message);
            $result = json_decode($sendOtp);
            if ($result->type == 'success') {
                return response($otp);
            }else{
                return Response::json(['error' => 'OTP Not Send.'], 400);
            }
        }else{
            return Response::json(['error' => 'Invalid phone number.'], 400);
        }
    }

    public function changeMyPassword(Request $request){
        $phone=$request->phone;
        $password=$request->password;
        $user=User::where('phone',$phone)->get()->first();
        if($user) {
            $user->password=bcrypt($password);
            if($user->save())
                return $user;
        }else{
            return Response::json( ['error'=>'Server Down'],400);
        }

    }
}
