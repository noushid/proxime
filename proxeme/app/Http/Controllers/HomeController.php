<?php

namespace App\Http\Controllers;

use App\Device;
use App\DevicePackage;
use App\Package;
use App\Support;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Softon\Indipay\Facades\Indipay;
use Session;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use App\Invoice;
use App\Helper\RandomColor;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user');
    }

    /**
     * Show the Home apge.
     *
     */
//    public function index()
//    {
//        $prices = Package::simplePaginate(4);
////        $prices->withPath('home#pricing');
//        return View::make("app.index")->with(array('packages' => $prices));
//    }

    /*
     * Show map
     * @return view
     * */
    public function map()
    {
        if(Auth::check())
            return view('app.map', ['user_id' => auth()->user()->id, 'packages' => Package::get()]);
        else
            return redirect('login');
    }

    /**
     * show user profile
     * @return View
    */
    public function userProfile()
    {
        if(Auth::check())
            return view('app.userProfile', ['user' => auth()->user(), 'packages' => Package::get()]);
        else
            return redirect('login');
    }

    /**
     * edit user profile
     * @param Illuminate\Http\Request
     * @return response
     */
    public function editProfile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.auth()->user()->id, 'packages' => Package::get()
        ]);
        /*if ($request->password!= NULL) {
            $this->validate($request, [
                'password' => 'required',
                'passwordConfirm' => 'required|same:password',
            ]);
            $data['password'] = bcrypt($request->password);
        }*/
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['username'] = $request->email;

        if (User::where('id',auth()->user()->id)->update($data)) {
            return redirect('user-profile')->with(['message' => 'Updated!']);
        }
    }

    /**
     * Upload image to user profile
     * @param Illuminate\Http\Request
     * @return response
     */
    public function uploadImage(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $image = $request->file('file');

        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/files');
        $image->move($destinationPath, $input['imagename']);
        $data['img'] = url('/files/' . $input['imagename']);
        if (User::where('id',auth()->user()->id)->update($data)) {
            return back()->with('success','Image Upload successful');
        }
        return back()->with('error', 'Image Upload failed');
    }

    /**
     * Upload image to user profile
     * @param Illuminate\Http\Request
     * @return response
     */
    public function removeImage()
    {
        $data['img'] = 'http://afsti.org/event_photos/default.png';
        if (User::where('id',auth()->user()->id)->update($data)) {
            return back()->with('success','Image Removed successful');
        }
        return back()->with('error', 'Image Upload failed');
    }


    /**
     * Show User Devices
     * @return View
     */
    public function userDevices(Request $request)
    {
        if (Auth::check()) {
            session()->forget('cart');
            if ($request->sort == 'active') {
                return view('app.mydevices', ['devices' => Device::where('user_id', auth()->user()->id)->with('user')->where('status', 1)->with('current_device_package')->with('current_device_package.package')->get(), 'packages' => Package::get()]);
            }
            if ($request->sort == 'inactive') {
                return view('app.mydevices', ['devices' => Device::where('user_id', auth()->user()->id)->where('status', 0)->with('user')->with('current_device_package')->with('current_device_package.package')->get(), 'packages' => Package::get()]);
            }
            return view('app.mydevices', ['devices' => Device::where('user_id', auth()->user()->id)->with('user')->with('current_device_package.package')->orderBy('activation_time', 'desc')->get(), 'packages' => Package::get()]);
        }else{
            return redirect('login');
        }
    }

    /**
     * Show User Devices order
     * @return View
     */
    public function userDevicesOrder(Request $request)
    {
        if (Auth::check()) {
            if ($request->sort == 'newToOld') {
                return view('app.mydevices', ['devices' => Device::where('user_id', auth()->user()->id)->with('user')->orderBy('activation_time', 'desc')->get(), 'packages' => Package::get()]);
            }
            if ($request->sort == 'oldToNew') {
                return view('app.mydevices', ['devices' => Device::where('user_id', auth()->user()->id)->with('user')->orderBy('activation_time', 'asc')->get(), 'packages' => Package::get()]);
            }
        }else
            return redierct('login');

    }

    /**
     * Show Change Password form
     * @return View
     */
    public function showChangePassword()
    {
        if (Auth::check())
            return view('app.change-password', ['user' => auth()->user(), 'packages' => Package::get()]);
        else
            return redirect('login');
    }

    /**
     * Change password
     * @param illuminate\http\request
     * @return response
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'curPassword' => 'required',
            'password' => 'required|min:6|max:8',
            'passwordConfirm' => 'required|same:password',
        ]);
        if (Hash::check($request->curPassword, auth()->user()->password)) {
            if (User::where('id',auth()->user()->id)->update(['password' => bcrypt($request->password)])) {
                return back()->with(['message' => 'Updated!']);
            }else{
                return back()->with(['errors' => 'Update failed!']);
            }
        }else{
            return back()->withInput()->withErrors(['curPassword' => 'Current Password Not Match']);
        }
    }

    /**
     * Add device to user(user side)
     * @param illuminate\http\request
     * @return response
     */
    public function addDevice(Request $request)
    {
        $device = Device::where('deviceId', $request->deviceId)->with('current_package')->first();
        if (!$device) {
            return response('Please Enter valid deviceId.', 400);
        }
        if ($device and $device->user_id == null) {
            /*add user_id to device table*/
            $device_count = $this->getDeviceCount(auth()->user()->id);
            if (DB::update('UPDATE devices SET user_id= ' . auth()->user()->id . ',activation_time=' . time() . ',color="' . RandomColor::one() . '",name="device ' . $device_count . '" WHERE deviceId = "' . $request->deviceId . '"')) {
                session()->forget('user.selected_device');
                $request->session()->put('user.selected_device', $request->deviceId);
                if (session('user.selected_device') != $request->deviceId) {
                    return response('error', 400);
                }
                /*If package already exists update some data package_devices*/
                if ($device->current_package_id != null) {
                    $last_order_no = DevicePackage::orderBy('id', 'DESC')->pluck('order_no')->first();

                    $device_package_data = [
                        'recharge_date' => date('Y-m-d h:i:s', time()),
                        'order_no' => ($last_order_no ? $last_order_no + 1 : 1111),
                        'device_id' => $device->id,
                        'package_id' => $device->current_package_id,
                        'is_free' => 1,
                    ];
                    $device_package = new DevicePackage($device_package_data);
                    if($device_package->save()){
                        $device->current_device_package_id = $device_package->id;
                        if($device->save()){
                            $request->session()->flash('message', 'Device Added!');
                            return response(['status' => 'device_added', 'deviceId' => $request->deviceId]);
                        }else{
                            $request->session()->flash('warning', 'Device Added! something wen\'t wrong');
                        }
                    }

                }else{
//                    return response('package_null');
//                    return response(['status' => 'package_null', 'deviceId' => $request->deviceId]);
                    return response(['status' => 'package_null', 'deviceId' => session('user.selected_device')]);
                }
            }
            return response('device_add_error', 400);
        }else{
            return response('Please enter valid deviceId', 400);
        }
    }

    /**
     * Get user wise device count
     * @param user_id
     * @return count
     */
    private function getDeviceCount($user_id){
        $count = Device::where('user_id', $user_id)->count();
        $count=$count+1;
        if($count<10)
            $count='0'.$count;
        return $count;
    }

    /**
     * remove device from user(user side)
     * @param illuminate\http\request
     * @return response
     */

    public function removeDevice($device_id)
    {
        if ($device_id != "") {
            if (Device::where('id', $device_id)->update(['user_id' => null])) {
                return redirect('user-devices')->with(['message' => 'Device Removed']);
            }
        }
    }


    /**
     * update device from user(user side)
     * @param illuminate\http\request
     * @return response
     */
    public function updateDevice(Request $request)
    {
        $device = Device::find($request->id);
        if ($device->update($request->all())) {
            return Device::find($request->id);
        }
        return response('device_not_updated', 400);
    }

    /**
     * Show profile
     * @return view
     */
    public function showProfile()
    {
        return view('app.profile');
    }

    /**
     * Generate OTP
     * @param illuminate\http\request
     * @return response
     */
    public function generateOtp(Request $request)
    {
        $phone = str_replace(['+', ' '], '', $request->phone);
        $request->merge(['phone' => $phone]);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email|unique:users,username',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|min:6|max:10',
            'confirm_password' => 'required|same:password',
        ], [
            'phone.required' => 'Enter valid phone number.',
            'phone.unique' => 'Phone number already registered to another account.',
            'email.unique' => 'Email address already registered.',
            'password.min' => 'The password must be at least :min characters.',
            'password.max' => 'The password must have no more than :max characters.',
            'confirm_password.same' => 'Password does not match.'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }

        $user = $request->name;
        $email = $request->email;
        $phone = str_replace(['+', ' '], '', $request->phone);
        $otp = mt_rand(10000, 99999);
        $message = "Hi $user, Your OTP is $otp";
        $sendOtp = sendOtp($phone, $otp, $message);
        $result = json_decode($sendOtp);
        if ($result->type == 'success') {
            $request->session()->put('user.otpTo', 'mail');
            $request->session()->put('user.otp', $otp);
            $request->session()->put('user.email', $email);
            $request->session()->put('user.phone', $phone);
            return response('OTP was send to ' . $phone);
        }else{
            return response('otp_error', 400);
        }

        /***By choosing otp send to mobile or email*/
        /*if ($request->sendTo == 'phone') {
            $user = $request->name;
            $email = $request->email;
            $phone = str_replace(['+', ' '], '', $request->phone);
            $otp = mt_rand(10000, 99999);
            $message = "Hi $user, Your verification passcode is $otp";
            $sendOtp = sendOtp($phone, $otp, $message);
            $result = json_decode($sendOtp);
            if ($result->type == 'success') {
                $request->session()->put('user.otpTo', 'mail');
                $request->session()->put('user.otp', $otp);
                $request->session()->put('user.email', $email);
                $request->session()->put('user.phone', $phone);
                return response('OTP was send to ' . $phone);
            }else{
                return response('otp_error', 400);
            }
        } elseif ($request->sendTo == 'email') {
            $data['user'] = $request->name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['otp'] = mt_rand(100000, 999999);
            Mail::send('email.otp', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['user'])->subject('proxime verification');
            });

            if (Mail::failures()) {
                return response('unable to send OTP try again later', 400);
            }else{
                $request->session()->put('user.otpTo', 'mail');
                $request->session()->put('user.otp', $data['otp']);
                $request->session()->put('user.email', $data['email']);
                $request->session()->put('user.phone', $data['phone']);
                return response('OTP was send to ' . $request->email);
            }
        }*/
    }

    /**
     * Resend OTP
     * @param illuminate\http\request
     * @return response
     */
    public function resendOtp(Request $request)
    {
        $user = $request->name;
        $phone = str_replace(['+', ' '], '', $request->phone);
        $otp = mt_rand(10000, 99999);
        $message = "Hi $user, Your OTP is $otp";
        $sendOtp = sendOtp($phone, $otp, $message);
        $result = json_decode($sendOtp);
        if ($result->type == 'success') {
            $request->session()->put('user.otpTo', 'mail');
            $request->session()->put('user.otp', $otp);
            $request->session()->put('user.phone', $phone);
            return response('OTP was send to ' . $phone);
        }
        return response('otp_error', 400);
    }


    /**
     * Generate OTP
     * @param illuminate\http\request
     * @return response
     */
    public function generateOtpPhone(Request $request){
        $phone = str_replace(['+', ' '], '', $request->phone);
        $validator = Validator::make(['phone' => $phone], [
            'phone' => 'required|unique:users,phone',
        ], [
            'phone.required' => 'Enter valid phone number.',
            'phone.unique' => 'Phone number already registered.',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }
        $user = auth()->user()->name;
        $phone = str_replace(['+', ' '], '', $request->phone);
        $otp = mt_rand(10000, 99999);
        $message = "Hi $user, Your OTP is $otp.";
        $sendOtp = sendOtp($phone, $otp, $message);
        $result = json_decode($sendOtp);
        if ($result->type == 'success') {
            $request->session()->put('user.otpTo', 'mail');
            $request->session()->put('user.otp', $otp);
            $request->session()->put('user.phone', $phone);
//            return response('OTP was send to ' . $otp);
            return response('OTP was send to ' . $phone);
        }
    }
    /**
     * Verify OTP
     * @param illuminate\http\request
     * @return response
     */
    public function verifyOtp(Request $request)
    {
        if (session('user.otp') == (int)$request->otp) {
            return response('otp_verified');
        }else{
            return response('otp_verify_error', 400);
        }
    }

    /**
     * Generate OTP For Mobile
     * @param illuminate\http\request
     * @return response
     */
    public function generateOtpForMobile(Request $request)
    {
        $phone = str_replace(['+', ' '], '', $request->phone);
        $request->merge(['phone' => $phone]);
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users,email|unique:users,username',
            'phone' => 'required|unique:users,phone',
        ]);
        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }
        $user = $request->name;
        $email = $request->email;
        $phone = str_replace(['+', ' '], '', $request->phone);
        $otp = mt_rand(10000, 99999);
        $message = "Hi $user, Your OTP is $otp.";
        $sendOtp = sendOtp($phone, $otp, $message);
        $result = json_decode($sendOtp);
        if ($result->type == 'success') {
            Session::put('mobileOtp', $otp);
            return response($otp);
        }
    }



    /**
     * show account details form
     * @return view
     */
    public function showAccountForm()
    {
        return view('app.accountDetails');
    }

    /**
     * Edit Profile by single value
     * @param illuminate\http\request
     * @return Response
     */
    public function editUserProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'nullable|regex:/^[A-Za-z\s-_1-9]+$/',
            'country' => 'nullable|regex:/^[A-Za-z\s-_]+$/',
            'username' => 'nullable|regex:/^[A-Za-z\s-_@.]+$/',
            'address' => 'nullable|max:150',
//            'country' => 'nullable'
//            'name' => 'nullable',
        ]);


        if ($validator->fails()) {
            return response($validator->getMessageBag()->toArray(), 400);
        }

        if ($request->has('name') and  $request->input('name') == null) {
            return response('updated');
        }

        if ($request->has('username') and  $request->input('username') == null) {
            return response('updated');
        }

        if ($request->has('address') and  $request->input('address') == null) {
            return response('updated');
        }

        if ($request->has('country') and  $request->input('country') == null) {
            return response('updated');
        }

        if (User::where('id', auth()->user()->id)->update($request->all())) {
            return response('updated');
        } else {
            return response("update_error", 400);
        }

    }

    public function EmailVerify(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|unique:users,email|email',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 400);
        }

        $token = str_random(30);
        $url = URL::to('update-profile/verify/' . $request->email . '/' . $token . '/' . auth()->user()->id);

        $data['user'] = auth()->user()->name;
        $data['email'] = $request->email;
        $data['verification_link'] = $url;
        Mail::send('email.verify', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['user'])->subject('proxime verification');
        });

        if (Mail::failures()) {
            return response('unable to send OTP try again later', 400);
        }else{
            if (User::where('id', auth()->user()->id)->update(['email_token' => $token,'update_email' => $request->email])) {
                return response('email_verification_send to' . $data['email']);
            } else {
                return response("update_error", 400);
            }
        }
    }

    public function verifyEmail($email,$token,$id)
    {
        $user = User::where('id', $id)->first();
        if ($user->update_email == $email && $user->email_token == $token) {
            if (User::where('id', $id)->update(['email' => $email])) {
                return redirect('user-profile')->with(['message' => 'updated']);
            } else {
                return redirect('user-profile')->with(['error' => 'update_failed']);
            }
        }else{
            User::where('id', $id)->update(['email' => $email]);
            return redirect('user-profile')->with(['error' => 'update_failed']);
        }
    }

    /**
     *
     * Purchase Packages
     * @param id
     * @return payment_gateway
    */
    public function purchasePackage(Request $request,$package_id)
    {
        if ($request->token == Session::token()) {
            //TODO -> redirect to confirmation page

            /*get package details*/
            $package = Package::find($package_id);

            /**** get last Invoice *****/
            $invoice = Invoice::orderBy('id', 'desc')->first();

            /**** generate transaction_id based on last invoice *****/
            $t_id = $invoice ? $invoice->t_id + 1 : '1001';

            /**** generate order_id based on last invoice *****/
            $order_no = $invoice ? $invoice->order_no + 1 : '1111';

            $parameters = [
                'tid' => $t_id,
                'order_id' => $order_no,
                'firstname' => auth()->user()->name,
                'email' => auth()->user()->email,
                'phone' => auth()->user()->phone,
                'productinfo' => $package->name,
                'amount' => $package->offer_price ? $package->offer_price : $package->price,
                'udf1' => $t_id,
                'udf2' => $order_no,
                'udf3' => auth()->user()->id,
                'udf4' => $package_id,
                'udf5' => $request->device
            ];

            $order = Indipay::prepare($parameters);
            return Indipay::process($order);
        }else{
            return redirect()->back()->withInput()->with('error', 'Your session was expired');
        }
    }


    /**
     * after payment success
     * @return redirect
    */
    public function purchaseSuccess(Request $request)
    {
        $response = Indipay::response($request);
        $device = Device::where('deviceId', $response['udf5'])->get()->first();

        $device_package_data['status'] = 1;
        $device_package_data['recharge_date'] = $response['addedon'];
        $device_package_data['order_no'] = $response['udf2'];
        $device_package_data['device_id'] = $device->id;
        $device_package_data['package_id'] = (int)$response['udf4'];

        $device_package = new DevicePackage($device_package_data);
        $device_package->save();

        $device->current_device_package_id = $device_package->id;
        $up_device = Device::where('deviceId', $response['udf5'])->update(['current_device_package_id' => $device_package->id]);

        $invoice_data['t_id'] = $response['udf1'];
        $invoice_data['order_no'] = $response['udf2'];
        $invoice_data['user_id'] = $response['udf3'];
        $invoice_data['package_id'] = $response['udf4'];
        $invoice_data['amount'] = (int)$response['amount'];
        $invoice_data['date'] = $response['addedon'];

        $invoice = new Invoice($invoice_data);
        $invoice = $invoice->save();

        if (!$device_package) {
//            TODO message for add package to device manually by admin
        }

        if (!$invoice) {
//            TODO message for generate invoice manually by admin
        }

        if ($device_package && $invoice) {
//            TODO request for refund to admin
        }
        $redirectTo = session('cart.redirect');
        session()->forget('cart');
        session()->forget('user');
        return redirect($redirectTo)->with(['message' => 'Success']);

    }


    /**
     * after payment error
     * @return redirect
     */
    public function purchaseError(Request $request)
    {
        // For default Gateway
        $response = Indipay::response($request);

        session()->forget('user');
        return redirect(session('cart.redirect'))->with(['error' => 'Payment Failed']);
    }


    /**
     * Cart page
     * @return redirect
     */
    public function confirmPackage(Request $request,$package_id)
    {
        if ($request->token == Session::token()) {
            $uri_path = parse_url(url()->previous(), PHP_URL_PATH);
            $uri_segments = explode('/', $uri_path);
            session(['cart.package_id' => $package_id]);
            session(['cart.device' => $request->device]);
//            session(['cart.redirect' => url()->previous()]);
            if ($request->reference) {
                session(['cart.redirect' => route('map')]);
            }else{
                session(['cart.redirect' => ($uri_segments[1] == 'subscriptions' ? route('user.devices') : url()->previous())]);
            }
            return view('app.confirm_package', ['package' => Package::find($package_id)]);
        }else{
            session()->forget('cart');
            return redirect(route('user.devices'))->with('error', 'Your session was expired');
        }
    }


    public function sendMessage(Request $request)
    {
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['subject'] = $request->subject;
        $data['phone'] = $request->phone;
        $data['content'] = $request->message;
        Mail::send('email.message', $data, function ($message) use ($data) {
            $message->to('proxime@surebot.co', $data['name'])->subject('comments');
        });

        if (Mail::failures()) {
            return back()->with(['error' => 'mail not sent']);
        }else{
            return back()->with(['message' => 'mail sent']);
        }
    }

    /**
     * Get device History
     * @param deviceId
     * @return response
     */

    public function packageHistory($deviceId)
    {
        $device = Device::where('deviceId', $deviceId)->get()->first();
        $package_history = DevicePackage::where('device_id', $device->id)->with('package')->get();
        if (count($package_history) > 0) {
            return response($package_history);
        } else {
            return response('no_history_found', 400);
        }
    }


    public function test(){
//        $sub=substr('918089183628',2);
//        return User::where('phone','Like',$sub)->get();
//        $data['name'] = 'kjk';
//        return view('app.subscriptions', $data);
        // $sub=substr('918089183628',2);
        // return User::where('phone','Like',$sub)->get();
        $data['email'] = '350042';
        $data['user'] = 'shammas pk';
        $data['phone'] = 'shammas pk';
        $data['verification_link'] = 'shammas pk';
        $data['password'] = 'shammas pk';
        return view('email.verify', $data);
    }


    public function pay()
    {
        /* All Required Parameters by your Gateway */

        $parameters = [

            'tid' => '1233221223322',
            'order_id' => '321213213',
            'firstname' => 'massum',
            'email' => 'pnoushid@gmail.com',
            'phone' => '8089183628',
            'productinfo' => 'testprodu',
            'amount' => '.50',

        ];

        $order = Indipay::prepare($parameters);
//        return Indipay::process($order);

//        $order = Indipay::gateway('PayUMoney')->prepare($parameters);
        return Indipay::process($order);
    }





    public function indipaySuccess()
    {
        var_dump('succ');
    }

    public function indipayError()
    {
        var_dump('errr');
    }

    public function subscriptions(Request $request,$device_id="")
    {
        if(Auth::check()){

            if ($device_id != "") {
            }
            if ($request->sort == 'newToOld') {
                return view('app.subscriptions', ['packages' => Package::orderBy('id', 'desc')->get()]);
            } elseif ($request->sort == 'oldToNew') {
                return view('app.subscriptions', ['packages' => Package::orderBy('id', 'asc')->get()]);
            }else{
                return view('app.subscriptions', ['packages' => Package::orderBy('id', 'dsc')->get()]);
            }
        }else{
            return redirect('login');
        }
    }



    public function setSession(Request $request)
    {
        $request->session()->flash($request->type, $request->data);
        return response($request->data);
    }


    /**
     *
     *ajax method for get all packages
     */
    public function getPackages()
    {
        $packages = Package::get();
        if (count($packages) > 0) {
            $html = '';
            $i = 0;
            foreach ($packages as $package) {
                $i = $i + 2;
                $html .= '<div class="col-sm-6 col-pad fadeInLeft delay-0' . $i . 's">
                    <div class="category">
                        <div class="category_bg_box">
                            <div class="category-overlay">
                                <span class="category-content">
                                    <span class="category-title">' . $package->price . '</span>
                                    <span class="category-subtitle">15 Projects</span>
                                    <span class="category-subtitle">30GB Storage</span>
                                    <span class="category-subtitle">Unilimited Data Transfer</span>
                                    <span class="category-subtitle">50 GB Bandwith</span>
                                    <span class="category-subtitle">Enhanced Security</span>
                                    <span class="category-subtitle">Unilimited Data</span>
                                    <a href="' . route('device.package.confirm', ['package_id' => $package->id, 'device' => session('user.selected_device'), 'token' => csrf_token()]) . '" class="btn button-sm button-theme">Buy</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>';
            }
            return response($html);
        }
        return response('no package found', 400);
    }



    private function checkNull($value) {
        if ($value == null) {
            return '';
        } else {
            return $value;
        }
    }

    public function UpdateDeviceName(Request $request,$device_id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'nullable|regex:/^[A-Za-z\s-_1-9@#$%&]+$/'
        ]);

        if ($validator->fails()) {
            return response($validator->getMessageBag()->toArray(), 400);
        }

        if ($request->has('name') and  $request->input('name') == null) {
            return response('updated');
        }

        if (Device::where('id', $device_id)->update($request->all())) {
            return response('updated');
        } else {
            return response("update_error", 400);
        }
    }

}
