<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = 'login';


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web', ['except' => 'logout']);
    }

    public function getAdminLogin()
    {
        if (auth()->guard('admin')->user())
            return redirect()->route('dashboard');
        return view('auth.login');
    }

    public function adminAuth(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);
        if (auth()->guard('admin')->attempt(['username' => $request->input('username'), 'password' => $request->input('password'),'status' => 1],$request->input('remember')))
        {
            $user = auth()->guard('admin')->user();
            if ($user->type == 'admin' || $user->type == 'subAdmin' ) {
                return redirect()->route('dashboard');
            }else{
                Auth::guard('admin')->logout();
                return redirect()->back()
                    ->withInput($request->only('username', 'remember'))
                    ->withErrors([
                        'error' => 'Your Username and password are wrong!',
                    ]);
            }
        }else{
            return redirect()->back()
                ->withInput($request->only('username', 'remember'))
                ->withErrors([
                    'error' => 'Your Username and password are wrong!',
                ]);
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
//        $request->session()->flush();
//        $request->session()->regenerate();
        return redirect()->route('admin.auth');
    }
}
