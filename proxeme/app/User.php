<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\CanResetPassword;
use Hash;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'email', 'phone', 'address', 'pin', 'place', 'district', 'state', 'country', 'status', 'activation_code', 'type', 'forgot_password_time', 'remember_code', 'last_login', 'remember_token','secret_key'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function device()
    {
        return $this->hasMany('App\Device');
    }

    public function checkPassword($oldPassword)
    {
        //if(Hash::check('admin',$this->attributes['password'])){
        if(Hash::check($oldPassword,$this->attributes['password'])){
            return true;
        }
        return false;
    }

}
