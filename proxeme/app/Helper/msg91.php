<?php
/**
 * msg91.php
 * User: Noushid P
 * Date: 16/11/17
 * Time: 6:00 PM
 */

function sendOtp($number,$otp,$message="")
{
    $auth_key = env('MSG91_KEY');
    if ($message == "") {
        $message = 'Your OTP is ' . $otp;
    }

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://control.msg91.com/api/sendotp.php?otp_length=5&authkey=$auth_key&message=$message&sender=SUREBOT&mobile=$number&otp=$otp&otp_expiry=5&email=",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return $err;
    } else {
        return $response;
    }
}


function verifyOtp($number,$otp)
{
//    var_dump('183863A6ZNDu535a0d6f1d');
    $auth_key = env('MSG91_KEY');
//    var_dump($auth_key);
//    exit;
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://control.msg91.com/api/verifyRequestOTP.php?authkey=$auth_key&mobile=$number&otp=$otp",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }
    exit;

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://control.msg91.com/api/verifyRequestOTP.php?authkey=$auth_key&mobile=9656535292&otp=96323",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return $err;
    } else {
        return $response;
    }
}


function sendsms($number, $message)
{
    $auth_key = env('MSG91_KEY');

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://api.msg91.com/api/sendhttp.php?sender=MSGIND&route=4&mobiles=$number&authkey=$auth_key&country=0&message=$message",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return $err;
    } else {
        return $response;
    }
}