<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceHistory extends Model
{
    protected $fillable = ['datetime', 'latitude', 'longitude', 'range_status', 'device_id'];

    public function device()
    {
        return $this->belongsTo('App\Device');
    }
}
