<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    protected $fillable = ['name', 'deviceId', 'activation_time', 'last_use_time', 'status', 'carrier_id', 'user_id', 'current_device_package_id', 'current_package_id', 'model_no'];
    use SoftDeletes;


    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function devicePackages()
    {
        return $this->hasMany('App\DevicePackage');
    }
    public function current_device_package()
    {
        return $this->belongsTo('App\DevicePackage', 'current_device_package_id');
    }

    public function current_package()
    {
        return $this->belongsTo('App\Package', 'current_package_id');
    }

}
