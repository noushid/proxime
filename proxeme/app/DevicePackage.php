<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevicePackage extends Model
{
    protected $fillable = ['status', 'recharge_date', 'order_no', 'device_id', 'package_id', 'is_free'];

    public function device()
    {
        return $this->belongsTo('device');
    }

    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
