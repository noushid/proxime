<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'HomeController@index');
//Route::get('home', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::post('generateOtp', 'HomeController@generateOtp');
Route::post('resendOTP', 'HomeController@resendOtp');
Route::post('verifyOtp', 'HomeController@verifyOtp');
Route::get('update-profile/verify/{email}/{token}/{id}', 'HomeController@verifyEmail');

Route::post('message/sent', 'HomeController@sendMessage');

Route::post('set-session', 'HomeController@setSession');


Route::post('search-account', ['as' => 'forgot.password', 'uses' => 'Auth\ForgotPasswordController@validateAccount']);

Route::post('password/phone', ['as' => 'password.phone', 'uses' => 'Auth\ForgotPasswordController@sendOtp']);
Route::get('password/phone/otp', ['as' => 'password.phone.otp', 'uses' => 'Auth\ForgotPasswordController@showVerifyOtp']);
Route::post('password/phone/otp', ['as' => 'password.phone.otp', 'uses' => 'Auth\ForgotPasswordController@verifyOtp']);
Route::get('password/reset/phone', ['as' => 'password.reset.phone', 'uses' => 'Auth\ForgotPasswordController@resetPassword']);
Route::post('password/phone/request', ['as' => 'password.phone.request', 'uses' => 'Auth\ForgotPasswordController@resetPasswordPhone']);


Route::group(['middleware' => ['user']], function () {
    Route::get('map', ['as'=>'map','uses'=>'HomeController@map']);
    Route::get('user-profile', ['as' => 'user.profile', 'uses' => 'HomeController@userProfile']);
    Route::post('edit-profile', ['as' => 'user.submit', 'uses' => 'HomeController@editProfile']);
    Route::post('upload', ['as' => 'profile.upload', 'uses' => 'HomeController@uploadImage']);
    Route::get('removepic', ['as' => 'profile.remove', 'uses' => 'HomeController@removeImage']);

    Route::get('user-devices', ['as' => 'user.devices', 'uses' => 'HomeController@userDevices']);
    Route::post('user-devices', ['as' => 'user.devices', 'uses' => 'HomeController@userDevicesOrder']);

    /*purchase start*/
    Route::get('confirm-package/{package_id}', ['as' => 'device.package.confirm', 'uses' => 'HomeController@confirmPackage']);
    Route::get('buy-package/{package_id}', ['as' => 'device.package.buy', 'uses' => 'HomeController@purchasePackage']);
    Route::post('buy-package/success', ['as' => 'device.package.success', 'uses' => 'HomeController@purchaseSuccess']);
    Route::post('buy-package/failure', ['as' => 'device.package.failure', 'uses' => 'HomeController@purchaseError']);
    /*purchase end*/

    /*Device History*/
    Route::get('package-history/{deviceId}', 'HomeController@packageHistory');

    Route::get('change-password', ['as' => 'user.password', 'uses' => 'HomeController@showChangePassword']);
    Route::post('change-password', ['as' => 'user.password', 'uses' => 'HomeController@changePassword']);
    Route::post('add-device', ['as' => 'user.device', 'uses' => 'HomeController@addDevice']);
    Route::get('remove-device/{device_id}', ['as' => 'device.delete', 'uses' => 'HomeController@removeDevice']);
    Route::get('profile', ['as' => 'user.profil', 'uses' => 'HomeController@showProfile']);
    Route::post('update-device', ['as' => 'user.device.update', 'uses' => 'HomeController@updateDevice']);

    Route::post('update-profile', ['as' => 'user.update', 'uses' => 'HomeController@editUserProfile']);
    Route::post('getOtpPhone', ['as' => 'user.otp.get', 'uses' => 'HomeController@generateOtpPhone']);
    Route::post('verify-email', ['as' => 'user.verify.email', 'uses' => 'HomeController@EmailVerify']);

    Route::get('signup/account-details', ['as' => 'signup.account-details', 'uses' => 'HomeController@showAccountForm']);

    Route::get('subscriptions', ['as' => 'subscriptions', 'uses' => 'HomeController@subscriptions']);
    Route::get('subscriptions/{package_id}', ['as' => 'device.subscription', 'uses' => 'HomeController@subscriptions']);

    Route::get('getPackages', 'HomeController@getPackages');

    Route::post('update-device-name/{device_id}', ['as' => 'user.device.update', 'uses' => 'HomeController@UpdateDeviceName']);
});


Route::get('test', 'HomeController@test');
Route::post('indipay/success', 'HomeController@indipaySuccess');
Route::post('indipay/error', 'HomeController@indipayError');
Route::get('indipay/pay', 'HomeController@pay');
//    temp end

Route::get('test', 'HomeController@test');

Route::group(['middleware' => ['web']], function () {
    Route::get('home', ['as' => 'home', 'uses' => 'UserLoginController@index']);
    Route::get('/', 'UserLoginController@index');

    Route::get('signup', ['as' => 'signup', 'uses' => 'UserLoginController@signup']);
    Route::post('signup', ['as' => 'signup.submit', 'uses' => 'UserLoginController@createUser']);
    Route::get('signup/confirm', ['as' => 'signup.confirm', 'uses' => 'UserLoginController@confirm']);
    Route::post('signup/confirm', ['as' => 'signup.confirm', 'uses' => 'UserLoginController@sendOtp']);
    Route::get('login', 'UserLoginController@getUserLogin');
    Route::post('login', ['as' => 'user.auth', 'uses' => 'UserLoginController@userAuth']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'UserLoginController@logout']);
    Route::get('admin/login', 'AdminLoginController@getAdminLogin');
    Route::post('admin/login', ['as' => 'admin.auth', 'uses' => 'AdminLoginController@adminAuth']);
    Route::post('admin/logout', ['as' => 'admin.logout', 'uses' => 'AdminLoginController@logout']);

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');


    Route::post('admin/logout', ['as' => 'admin.logout', 'uses' => 'AdminLoginController@logout']);

    Route::group(['middleware' => ['admin']], function () {
        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'AdminController@dashboard']);
//        Route::post('login', ['as' => 'user.auth', 'uses' => 'UserLoginController@userAuth']);
    });

});


Route::group(['prefix' => 'api', 'middleware' => ['admin']], function () {

    /**
     * API ROUTES START
     */

    /*Temp create online device*/
    Route::resource('virtualDevice', 'virtualDeviceController');
    Route::post('virtualDevice', 'virtualDeviceController@device');


    Route::resource('user', 'Auth\UserController');
    Route::resource('carrier', 'Auth\CarrierController');
    Route::resource('device', 'Auth\DeviceController');
    Route::resource('devicehistory', 'Auth\DeviceHistoryController');
    Route::resource('devicepackage', 'Auth\DevicePackageController');
    Route::resource('loginhistory', 'Auth\LoginHistoryController');
    Route::resource('package', 'Auth\PackageController');
    Route::resource('userdevice', 'Auth\UserDeviceController');

    Route::post('device/restore', 'Auth\DeviceController@restore');
    Route::post('carrier/restore', 'Auth\CarrierController@restore');
    Route::post('package/restore', 'Auth\PackageController@restore');
    Route::post('user/restore', 'Auth\UserController@restore');

    Route::post('usernameCheck', 'Auth\UserController@checkUsername');
    Route::post('emailCheck', 'Auth\UserController@checkEmail');

    Route::post('device/exportPdf', 'Auth\DeviceController@exportToPdf');
    Route::post('package/exportPdf', 'Auth\PackageController@exportToPdf');


    Route::get('getDeviceCount', 'Auth\DeviceController@getCount');
    Route::get('getUserCount', 'Auth\UserController@getCount');
    Route::get('getActiveUserCount', 'Auth\UserController@getActiveUserCount');

    Route::get('getCurrentUser', 'Auth\DashboardController@getCurrentUser');

    Route::post('device/import', 'Auth\DeviceController@import');

});



/**
 * Angular Template start
*/
Route::get('template/{name}', ['as' => 'templates', function ($name) {
    return view('dashboard.' . $name);
}]);


Route::group(['middleware'=>'webcors'],function() {

    Route::get('mobile/registeruser','MobileController@registerUser');
    Route::get('mobile/generateOtp', 'MobileController@generateOtpForMobile');
    Route::get('mobile/createuserformobile', 'MobileController@createUserForMobile');
    Route::get('mobile/getdevicehistory','MobileController@getDeviceHistory');
    Route::get('mobile/registercarddetails','MobileController@registerCardDetails');
    Route::get('mobile/savingdevices','MobileController@savingDevices');
    Route::get('mobile/savingdeviceeach','MobileController@savingDeviceEach');
    Route::get('mobile/getlogin','MobileController@getLogin');
    Route::get('mobile/registersupport','MobileController@registerSupport');
    Route::get('mobile/getmydevices','MobileController@getMyDevices');
    Route::get('mobile/changedevicename','MobileController@changeDeviceName');
    Route::get('mobile/getcarddetails','MobileController@getLastRegisteredCardDetails');
    Route::get('mobile/changemyprofile','MobileController@changeMyProfile');
    Route::get('mobile/changepassword','MobileController@changeAppPassword');
    Route::get('mobile/generateHash','MobileController@generateHash');

    Route::get('mobile/getmyprofileinfo','MobileController@getMyProfileInfo');
    Route::get('mobile/getlastloginuser','MobileController@getLastLoginUser');

    Route::get('mobile/testing','MobileController@testing');

    Route::post('mobile/registeruser','MobileController@registerUser');
    Route::post('mobile/generateOtp', 'MobileController@generateOtpForMobile');
    Route::post('mobile/createuserformobile', 'MobileController@createUserForMobile');
    Route::post('mobile/getdevicehistory','MobileController@getDeviceHistory');
    Route::post('mobile/registercarddetails','MobileController@registerCardDetails');
    Route::post('mobile/savingdevices','MobileController@savingDevices');
    Route::post('mobile/savingdeviceeach','MobileController@savingDeviceEach');
    Route::post('mobile/getlogin','MobileController@getLogin');
    Route::post('mobile/registersupport','MobileController@registerSupport');
    Route::post('mobile/getmydevices','MobileController@getMyDevices');
    Route::post('mobile/changedevicename','MobileController@changeDeviceName');
    Route::post('mobile/getcarddetails','MobileController@getLastRegisteredCardDetails');
    Route::post('mobile/getcarddetailsbyid','MobileController@getLastRegisteredCardDetailsById');
    Route::post('mobile/changepassword','MobileController@changeAppPassword');
    Route::post('mobile/generateHash','MobileController@generateHash');
    Route::post('mobile/uploadmyprofile','MobileController@uploadMyProfile');
    Route::post('mobile/getmyprofileinfo','MobileController@getMyProfileInfo');
    Route::post('mobile/changemyprofile','MobileController@changeMyProfile');
    Route::post('mobile/removemyphoto','MobileController@removeMyPhtoto');
    Route::post('mobile/getlastloginuser','MobileController@getLastLoginUser');
    Route::post('mobile/generateChangePhoneOtp','MobileController@generateChangePhoneOtp');
    Route::post('mobile/generateChangePhoneOtpWithValidation','MobileController@generateChangePhoneOtpWithValidation');
    Route::post('mobile/changeMyPhone','MobileController@changeMyPhone');
    Route::post('mobile/generateChangePasswordOtp','MobileController@generateChangePasswordOtp');
    Route::post('mobile/changeMyPassword','MobileController@changeMyPassword');

    //Route::post('mobile/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    /*Commented by noushid*/
    Route::post('mobile/password/email', 'MobileController@sendResetLinkEmailForMobile')->name('passwordmobile.email');
});




/**
 * Web API
*/
Route::group(['prefix' => 'api/web', 'middleware' => ['web']], function () {
    Route::get('user/{id}', 'Auth\UserController@getUser');
    Route::post('add-device', 'Auth\DeviceController@addDeviceToUser');
    Route::get('check-package/{device_id}', 'Auth\DeviceController@checkPackage');
});
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('format/{filename}', function ($filename)
{
    $exist=\Illuminate\Support\Facades\Storage::disk('local')->exists($filename);
    if($exist) {
        $file = \Illuminate\Support\Facades\Storage::get($filename);
        return response($file, 200)->header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }
});

Route::get('log/download/{filename}', function ($filename)
{
    /*if(file_exists(storage_path('app/log/').$filename)) {
        $file = \Illuminate\Support\Facades\Storage::get(storage_path('app/log/') . $filename);
        var_dump($file);
        return response($file, 200)->header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }*/
    $exist = \Illuminate\Support\Facades\Storage::disk('log')->exists($filename);
    if($exist) {
        $file = \Illuminate\Support\Facades\Storage::disk('log')->get($filename);
        return response($file, 200)->header('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }
});