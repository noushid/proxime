let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

var nodeDir = 'node_modules/';
var bowerDir = 'bower_components/';
var resourceDir = 'resources/';
mix.styles([
    resourceDir + '/assets/css/bootstrap.min.css',
    resourceDir + '/assets/css/icons.css',
    nodeDir + '/angular-confirm1/css/angular-confirm.css',
    resourceDir + '/assets/css/style.css',
    resourceDir + '/assets/css/custom.css',
    nodeDir + '/angular-loading-bar/build/loading-bar.css',
    nodeDir + '/ng-dialog/css/ngDialog.css',
    nodeDir + '/ng-dialog/css/ngDialog-theme-default.css',
    nodeDir + '/intl-tel-input/build/css/intlTelInput.css'
], 'public/css/app.css');


mix.copy(nodeDir + 'angular-confirm1/css/angular-confirm.css', 'public/css');


//mix.copyDirectory('resources/assets/js/', 'public/js');
mix.copyDirectory('resources/assets/images/', 'public/images');
mix.copyDirectory('resources/assets/fonts/', 'public/fonts');
mix.copyDirectory('resources/assets/pages/', 'public/pages');
mix.copyDirectory('resources/assets/plugins/', 'public/plugins');
mix.copy(resourceDir + '/assets/js/*.js', 'public/js');
mix.copy(nodeDir + '/ngmap/build/scripts/ng-map.min.js', 'public/js/map');

mix.copyDirectory('resources/assets/front-end/css', 'public/css');
mix.copyDirectory('resources/assets/front-end/fonts', 'public/fonts');
mix.copyDirectory('resources/assets/front-end/images', 'public/images');
mix.copyDirectory('resources/assets/front-end/img', 'public/img');
mix.copyDirectory('resources/assets/front-end/js', 'public/js');

mix.scripts([
    nodeDir + 'angular/angular.js',
    nodeDir + 'angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
    nodeDir + 'angular-route/angular-route.js',
    nodeDir + 'angular-resource/angular-resource.js',
    nodeDir + 'angular-utils-pagination/dirPagination.js',
    nodeDir + 'angular-loading-bar/build/loading-bar.js',
    nodeDir + 'chart.js/dist/Chart.js',
    nodeDir + 'angular-chart.js/dist/angular-chart.js',
    nodeDir + 'ngmap/build/scripts/ng-map.js',
    nodeDir + 'ng-file-model/ng-file-model.js',
    nodeDir + 'angular-confirm1/js/angular-confirm.js',
    nodeDir + 'ng-dialog/js/ngDialog.js',
    bowerDir + 'angular-random-string/src/angular-random-string.js',
    //nodeDir + 'ng-intl-tel-input/dist/ng-intl-tel-input.js'
    bowerDir + 'international-phone-number/releases/international-phone-number.js'
], 'public/js/angular/angular-bootstrap.js');

mix.scripts([
    nodeDir + 'angular/angular.js',
    nodeDir + 'angular-loading-bar/build/loading-bar.js',
    nodeDir + 'ngmap/build/scripts/ng-map.js',
    nodeDir + 'angular-confirm1/js/angular-confirm.js'
], 'public/js/map/angular-bootstrap.js');

mix.scripts([
    resourceDir + 'assets/js/angular/*.js',
    resourceDir + 'assets/js/angular/service/*.js',
    resourceDir + 'assets/js/angular/controller/*.js'
], 'public/js/angular/angularApp.js');

mix.scripts([
    resourceDir + 'assets/js/map/*.js',
    resourceDir + 'assets/js/map/controller/*.js'
], 'public/js/map/angularApp.js');