-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: proxime
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `carriers`
--

DROP TABLE IF EXISTS `carriers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carriers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carriers`
--

LOCK TABLES `carriers` WRITE;
/*!40000 ALTER TABLE `carriers` DISABLE KEYS */;
INSERT INTO `carriers` VALUES (1,'airtel','2017-11-12 14:46:10','2017-11-12 14:46:10',NULL),(2,'airtel','2017-11-12 14:46:10','2017-11-12 14:46:10',NULL),(3,'vodafone','2017-11-12 14:46:10','2017-11-12 14:46:10',NULL),(4,'airtel','2017-11-12 14:46:10','2017-11-12 14:46:10',NULL),(5,'idea','2017-11-12 14:46:10','2017-11-12 14:46:10',NULL);
/*!40000 ALTER TABLE `carriers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_histories`
--

DROP TABLE IF EXISTS `device_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `range_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_histories`
--

LOCK TABLES `device_histories` WRITE;
/*!40000 ALTER TABLE `device_histories` DISABLE KEYS */;
INSERT INTO `device_histories` VALUES (1,'1973-04-18 12:57:17','-38.251825','-95.537825','2',3,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(2,'2010-06-28 21:44:09','-2.649004','5.168887','2',6,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(3,'1992-08-14 06:57:59','31.801606','95.13429','4',1,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(4,'1977-10-08 04:33:31','78.621519','-3.765778','2',2,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(5,'2001-07-31 08:48:51','-10.279276','-111.264002','2',5,'2017-11-12 14:46:10','2017-11-12 14:46:10');
/*!40000 ALTER TABLE `device_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_packages`
--

DROP TABLE IF EXISTS `device_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '1',
  `recharge_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_packages`
--

LOCK TABLES `device_packages` WRITE;
/*!40000 ALTER TABLE `device_packages` DISABLE KEYS */;
INSERT INTO `device_packages` VALUES (1,1,'1972-01-02 13:14:13','441',7,2,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(2,1,'1988-11-01 23:47:18','973',4,5,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(3,1,'2003-05-01 16:08:32','358',4,7,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(4,2,'1996-02-08 03:01:58','754',9,2,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(5,2,'1986-11-26 23:34:56','320',8,2,'2017-11-12 14:46:12','2017-11-12 14:46:12');
/*!40000 ALTER TABLE `device_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deviceId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `carrier_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'Ms. Gabrielle Wolff PhD','156','2008-07-25 10:42:27','1972-07-14 16:03:07',1,7,53,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(2,'Roosevelt Krajcik','160','2005-12-03 22:28:42','1984-01-09 03:57:24',1,5,56,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(3,'Dr. Godfrey Lang II','174','1976-12-02 08:56:52','2000-05-05 23:27:08',1,5,34,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(4,'Ruby Spencer','190','1982-01-19 01:11:15','1985-07-14 01:05:44',1,6,38,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(5,'Prof. Benny Ebert','123','2008-05-17 00:01:06','2007-09-25 04:48:53',2,9,81,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(6,'Niko Kessler','155','1977-11-08 09:30:24','1991-02-17 08:24:34',2,4,63,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(7,'Jerome Von','173','2001-05-05 22:03:55','1991-05-02 19:45:36',1,4,7,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(8,'Justine Gleason III','170','2016-04-21 23:17:56','1982-12-30 04:40:10',2,9,14,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(9,'Destiney Grady','113','1981-09-08 13:55:27','1976-05-23 17:26:02',2,7,58,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(10,'Audie Bartell','106','1972-07-01 20:51:04','1971-12-31 07:50:02',2,7,82,NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_histories`
--

DROP TABLE IF EXISTS `login_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_histories`
--

LOCK TABLES `login_histories` WRITE;
/*!40000 ALTER TABLE `login_histories` DISABLE KEYS */;
INSERT INTO `login_histories` VALUES (1,'1983-04-17 23:28:00',2,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(2,'1983-01-10 18:52:14',2,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(3,'1990-07-24 12:13:01',3,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(4,'2008-09-26 15:44:16',9,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(5,'2007-02-23 18:57:21',5,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(6,'2015-07-27 12:58:12',5,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(7,'1985-03-06 20:00:12',1,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(8,'2009-07-13 17:02:02',6,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(9,'1989-03-23 20:12:52',8,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(10,'1987-09-09 12:29:07',9,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(11,'1997-04-12 13:15:13',7,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(12,'2013-06-27 08:31:51',5,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(13,'1981-04-13 03:55:24',9,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(14,'1990-11-02 17:58:47',8,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(15,'1996-12-24 11:48:42',2,'2017-11-12 14:46:12','2017-11-12 14:46:12');
/*!40000 ALTER TABLE `login_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=688 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (504,'2017_10_17_051014_create_user_devices_table',1),(675,'2014_10_12_000000_create_users_table',2),(676,'2014_10_12_100000_create_password_resets_table',2),(677,'2016_06_01_000001_create_oauth_auth_codes_table',2),(678,'2016_06_01_000002_create_oauth_access_tokens_table',2),(679,'2016_06_01_000003_create_oauth_refresh_tokens_table',2),(680,'2016_06_01_000004_create_oauth_clients_table',2),(681,'2016_06_01_000005_create_oauth_personal_access_clients_table',2),(682,'2017_10_17_050737_create_devices_table',2),(683,'2017_10_17_050813_create_carriers_table',2),(684,'2017_10_17_050823_create_packages_table',2),(685,'2017_10_17_050838_create_login_histories_table',2),(686,'2017_10_17_050859_create_device_packages_table',2),(687,'2017_10_17_050939_create_device_histories_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `offer_price` decimal(8,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `usage` int(11) NOT NULL,
  `featured` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` VALUES (1,'Jarred O\'Conner','Ut beatae corrupti neque excepturi.',340.00,226.00,17,213,0,NULL,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(2,'Prof. Javier Littel','Repudiandae ut accusamus non deleniti vero cum.',312.00,95.00,16,359,0,NULL,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(3,'Mr. Pietro Bashirian','Iure et deserunt quisquam quia mollitia culpa voluptates.',439.00,157.00,2,398,0,NULL,'2017-11-12 14:46:12','2017-11-12 14:46:12'),(4,'Alaina Wyman','Non officiis molestiae occaecati quisquam odio.',162.00,370.00,2,172,0,NULL,'2017-11-12 14:46:12','2017-11-12 14:46:12');
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `online` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_time` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','admin@admin.com','$2y$10$BokxUIW3ZMek.tY5aLXeaepkP2FHJT5JZYTsxAQMRjBDcxCzsMFqq',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'admin',NULL,NULL,'6RlLdXWvex',NULL,'2017-11-12 14:46:09','2017-11-12 14:46:09'),(2,'subadmin','subadmin','sub@admin.com','$2y$10$BokxUIW3ZMek.tY5aLXeaepkP2FHJT5JZYTsxAQMRjBDcxCzsMFqq',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,NULL,NULL,'subAdmin',NULL,NULL,'SVnDoM0Ayx',NULL,'2017-11-12 14:46:09','2017-11-12 14:46:09'),(3,'Kaela Nienow','jacobson.zackery','blindgren@example.com','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','(621) 785-3892 x4766','9004 Hayes Avenue Apt. 748\nMonserrateville, WV 41023','65432-3564','Elwin Prairie','Flatley Street','Newell Way','Timor-Leste',0,0,'231.142.144.232','HXWKJ3cfd8','public','15:04:15','1987-03-25 07:35:50','wRmMavwxnP',NULL,'2017-11-12 14:46:09','2017-11-12 14:46:09'),(4,'Dr. Adolph Upton','ngleichner','kub.bernadine@example.net','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','(902) 531-8474','8671 Torp Parkway\nAdellatown, OR 29633','18828','Mertz Union','Mayer Parkways','Roxane Mills','Saudi Arabia',0,0,'190.142.175.161','8zrTMGWV53','public','00:39:16','1978-06-19 07:21:34','ShCfVnGyQo',NULL,'2017-11-12 14:46:09','2017-11-12 14:46:09'),(5,'Ms. Kaci Ferry','qstoltenberg','uweissnat@example.org','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','409.947.8814','81468 Archibald Manors\nNorth Clemmie, WV 63903-5707','93680-3767','Bethel Mission','Dach Prairie','Justen Vista','United Kingdom',0,0,'103.178.175.17','i3UFbod5Za','public','14:10:46','2009-05-11 04:37:44','1raLiAy0rO',NULL,'2017-11-12 14:46:09','2017-11-12 14:46:09'),(6,'Mrs. Flavie Streich','zwatsica','bessie98@example.com','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','690-715-5643','5097 Pearlie Valley\nWest Nyasiaport, MT 12572-6997','33853-7748','Danny Parks','Lambert Dale','Mohammed Walks','United States of America',1,0,'56.114.5.187','bEG1aMJwlV','public','18:31:22','2016-08-19 06:27:10','0eCW42dvcA',NULL,'2017-11-12 14:46:09','2017-11-12 14:46:09'),(7,'Hayden Dickens','torphy.marguerite','estevan56@example.com','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','797-288-1940 x74708','25091 Jacques Via Apt. 209\nBrakusside, IN 20380-3844','04765','Bernier Plaza','Okuneva Club','Jadyn Radial','Iraq',1,1,'124.143.146.133','QofkzkYOX4','public','04:59:20','1996-10-29 08:53:54','Ym6GPLKYCn',NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(8,'Dr. Coralie Goodwin','yrogahn','kub.etha@example.net','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','839.819.0150 x786','715 Nikolaus Skyway Suite 966\nLavonneburgh, IN 90492-3788','06005','Nitzsche Radial','Onie Street','Winifred Hill','Seychelles',0,1,'119.208.84.230','JzyY6KhJTN','public','20:04:08','1989-08-21 11:08:42','X2Onlusv12',NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(9,'Pinkie Bosco','xtillman','ykautzer@example.org','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','476.840.8477','2616 Swift Burgs Suite 192\nHowechester, IN 03113-6582','95480-4026','Price Route','Harvey Pine','Cole Ridges','Palestinian Territories',0,1,'224.75.54.232','sOZQIy2QJv','public','08:40:40','2000-05-20 14:43:38','sD6KoV5Vse',NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(10,'Lew Rohan','lavon.mosciski','otilia.hickle@example.org','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','701-331-7736 x438','8961 Ortiz Mountains\nKurtton, CT 04315-8896','07425','Darren Creek','Nannie Course','Daphney Creek','Mozambique',0,0,'99.222.48.51','a4oxLEZAyI','public','04:18:56','1997-05-22 01:50:39','qEca5poXUH',NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(11,'Ozella Treutel','thora.okuneva','issac22@example.net','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','791.796.2452','5780 Marques Prairie\nKeirafort, DC 63190','72579-8503','Cassin Shores','Reyna Unions','Schroeder Crossing','Eritrea',0,1,'48.44.131.232','2ZDobul9mH','public','23:56:05','1970-08-04 00:58:21','uavUGYVXyj',NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10'),(12,'Zachariah Greenholt','delaney.jacobs','keebler.flo@example.net','$2y$10$1TybiaJE.7tclzlI/fe1/eskTF4sjaxXonyLHX5WTYDvDZMAeiNfG','1-964-487-8649','167 Tracy Village\nAbernathyland, ND 67376-4685','89716-5138','Johns Motorway','Mitchell Wall','Frida Courts','Liechtenstein',1,0,'104.226.108.47','WibGDQsMmP','public','02:56:26','2005-08-12 19:07:54','P0OAnAnS9d',NULL,'2017-11-12 14:46:10','2017-11-12 14:46:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-13  1:46:16
