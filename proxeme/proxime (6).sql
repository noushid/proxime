-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 22, 2017 at 12:39 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.25-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proxime`
--

-- --------------------------------------------------------

--
-- Table structure for table `card_details`
--

DROP TABLE IF EXISTS `card_details`;
CREATE TABLE `card_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `cardnumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nameoncard` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expirydate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `card_details`
--

TRUNCATE TABLE `card_details`;
-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

DROP TABLE IF EXISTS `carriers`;
CREATE TABLE `carriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `carriers`
--

TRUNCATE TABLE `carriers`;
--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'vodafone', '2017-11-22 01:05:58', '2017-11-22 01:05:58', NULL),
(2, 'airtel', '2017-11-22 01:05:58', '2017-11-22 01:05:58', NULL),
(5, 'idea', '2017-11-22 01:05:58', '2017-11-22 01:05:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deviceId` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_time` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_time` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `carrier_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `devices`
--

TRUNCATE TABLE `devices`;
-- --------------------------------------------------------

--
-- Table structure for table `device_histories`
--

DROP TABLE IF EXISTS `device_histories`;
CREATE TABLE `device_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `range_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `device_histories`
--

TRUNCATE TABLE `device_histories`;
--
-- Dumping data for table `device_histories`
--

INSERT INTO `device_histories` (`id`, `datetime`, `latitude`, `longitude`, `range_status`, `device_id`, `created_at`, `updated_at`) VALUES
(1, '2000-04-04 23:10:42', '-83.558292', '128.06751', '2', 1, '2017-11-22 01:05:58', '2017-11-22 01:05:58'),
(2, '1985-12-13 06:11:58', '32.367151', '49.307246', '4', 6, '2017-11-22 01:05:58', '2017-11-22 01:05:58'),
(3, '2009-02-20 01:50:07', '41.671061', '-121.336361', '2', 6, '2017-11-22 01:05:58', '2017-11-22 01:05:58'),
(4, '1974-09-16 05:41:38', '-30.622439', '128.782818', '3', 1, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(5, '1988-07-18 13:30:00', '-32.322154', '144.364037', '1', 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59');

-- --------------------------------------------------------

--
-- Table structure for table `device_packages`
--

DROP TABLE IF EXISTS `device_packages`;
CREATE TABLE `device_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `recharge_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `device_packages`
--

TRUNCATE TABLE `device_packages`;
--
-- Dumping data for table `device_packages`
--

INSERT INTO `device_packages` (`id`, `status`, `recharge_date`, `order_no`, `device_id`, `package_id`, `created_at`, `updated_at`) VALUES
(1, 1, '1981-09-09 10:02:51', '971', 3, 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(2, 1, '1998-06-14 03:20:03', '151', 6, 1, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(3, 2, '2010-04-12 02:06:51', '449', 8, 1, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(4, 1, '1985-12-16 14:28:38', '315', 1, 6, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(5, 1, '2013-04-08 13:46:04', '512', 1, 2, '2017-11-22 01:05:59', '2017-11-22 01:05:59');

-- --------------------------------------------------------

--
-- Table structure for table `login_histories`
--

DROP TABLE IF EXISTS `login_histories`;
CREATE TABLE `login_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `login_histories`
--

TRUNCATE TABLE `login_histories`;
--
-- Dumping data for table `login_histories`
--

INSERT INTO `login_histories` (`id`, `datetime`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '1982-05-28 04:47:17', 4, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(2, '1981-11-23 16:31:26', 9, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(3, '1986-06-05 00:20:36', 7, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(4, '1983-12-19 15:24:34', 2, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(5, '1993-08-07 08:21:56', 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(6, '1999-05-06 05:20:45', 1, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(7, '2009-10-19 13:48:43', 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(8, '1990-12-14 21:15:53', 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(9, '1977-05-15 03:26:17', 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(10, '1997-12-15 23:39:57', 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(11, '1985-06-10 17:34:22', 2, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(12, '2017-04-18 09:39:22', 4, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(13, '2013-05-21 08:56:13', 9, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(14, '1973-09-23 22:24:44', 8, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(15, '1977-04-29 10:49:44', 9, '2017-11-22 01:05:59', '2017-11-22 01:05:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(504, '2017_10_17_051014_create_user_devices_table', 1),
(787, '2014_10_12_000000_create_users_table', 2),
(788, '2014_10_12_100000_create_password_resets_table', 2),
(789, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(790, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(791, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(792, '2016_06_01_000004_create_oauth_clients_table', 2),
(793, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(794, '2017_10_17_050737_create_devices_table', 2),
(795, '2017_10_17_050813_create_carriers_table', 2),
(796, '2017_10_17_050823_create_packages_table', 2),
(797, '2017_10_17_050838_create_login_histories_table', 2),
(798, '2017_10_17_050859_create_device_packages_table', 2),
(799, '2017_10_17_050939_create_device_histories_table', 2),
(800, '2017_11_15_151755_create_sessions_table', 2),
(801, '2017_11_21_150705_create_card_details_table', 2),
(802, '2017_11_22_014723_create_support_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `oauth_access_tokens`
--

TRUNCATE TABLE `oauth_access_tokens`;
-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `oauth_auth_codes`
--

TRUNCATE TABLE `oauth_auth_codes`;
-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `oauth_clients`
--

TRUNCATE TABLE `oauth_clients`;
-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `oauth_personal_access_clients`
--

TRUNCATE TABLE `oauth_personal_access_clients`;
-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `oauth_refresh_tokens`
--

TRUNCATE TABLE `oauth_refresh_tokens`;
-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `offer_price` decimal(8,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `usage` int(11) NOT NULL,
  `featured` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `packages`
--

TRUNCATE TABLE `packages`;
--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `description`, `price`, `offer_price`, `duration`, `usage`, `featured`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Oran Keeling', 'Aut adipisci harum ea non.', '411.00', '397.00', 23, 295, 1, NULL, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(2, 'Collin Christiansen', 'Sequi qui odio possimus molestiae quia.', '381.00', '105.00', 8, 521, 0, NULL, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(3, 'Freddy Kessler I', 'Et quo suscipit voluptatem suscipit eaque fugiat in nostrum.', '270.00', '326.00', 28, 299, 0, NULL, '2017-11-22 01:05:59', '2017-11-22 01:05:59'),
(4, 'Yoshiko Jerde', 'Dignissimos ut ullam expedita qui et molestiae adipisci consequatur.', '262.00', '267.00', 18, 937, 1, NULL, '2017-11-22 01:05:59', '2017-11-22 01:05:59');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `sessions`
--

TRUNCATE TABLE `sessions`;
--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('AY8bAYjFBsfNKqeLgiQLSCrPeqPlqXQKqevoUm2u', 13, '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiOFhLOE9DSkdvQTNDenNXb3VPZWdOR3Zzb1Y3SGFTeEY0em5VelNYQyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDk6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9zdWJzY3JpcHRpb25zP3NvcnQ9b2xkVG9OZXciO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjQ6InVzZXIiO2E6NDp7czo1OiJvdHBUbyI7czo0OiJtYWlsIjtzOjM6Im90cCI7aTo4MjM1MDtzOjU6ImVtYWlsIjtzOjE4OiJwbm91c2hpZEBnbWFpbC5jb20iO3M6NToicGhvbmUiO3M6MTI6IjkxODA4OTE4MzYyOCI7fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjEzO30=', 1511334020);

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

DROP TABLE IF EXISTS `supports`;
CREATE TABLE `supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(800) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `supports`
--

TRUNCATE TABLE `supports`;
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `online` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_code` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `secret_key` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verify` tinyint(1) NOT NULL DEFAULT '1',
  `email_token` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `phone`, `address`, `pin`, `place`, `district`, `state`, `country`, `status`, `online`, `ip_address`, `activation_code`, `secret_key`, `type`, `email_verify`, `email_token`, `img`, `last_login`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', '$2y$10$CAZaArT1iIJ3f.xsxSS5r.wP7RNWEoRhh1FgNJEI5rnLfjxxp9VQa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, 'admin', 1, NULL, NULL, NULL, 'XYCWdykPsu', NULL, '2017-11-22 01:05:58', '2017-11-22 01:05:58'),
(2, 'subadmin', 'subadmin', 'sub@admin.com', '$2y$10$CAZaArT1iIJ3f.xsxSS5r.wP7RNWEoRhh1FgNJEI5rnLfjxxp9VQa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, 'subAdmin', 1, NULL, NULL, NULL, 'Wus68JCMTT', NULL, '2017-11-22 01:05:58', '2017-11-22 01:05:58'),
(13, 'noushid', 'pnoushid@gmail.com', 'pnoushid@gmail.com', '$2y$10$9Ptsx8pxB6282jbkGWdoeuJniVuzqaynhRFTbplCE/VjhNIoN2PPq', '918089183628', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, 'public', 1, NULL, NULL, NULL, NULL, NULL, '2017-11-22 01:20:00', '2017-11-22 01:20:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `card_details`
--
ALTER TABLE `card_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `devices_deviceid_unique` (`deviceId`);

--
-- Indexes for table `device_histories`
--
ALTER TABLE `device_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `device_packages`
--
ALTER TABLE `device_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_histories`
--
ALTER TABLE `login_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `card_details`
--
ALTER TABLE `card_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `device_histories`
--
ALTER TABLE `device_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `device_packages`
--
ALTER TABLE `device_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `login_histories`
--
ALTER TABLE `login_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=803;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
