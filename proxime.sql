-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: proxime
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `carriers`
--

DROP TABLE IF EXISTS `carriers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carriers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carriers`
--

LOCK TABLES `carriers` WRITE;
/*!40000 ALTER TABLE `carriers` DISABLE KEYS */;
INSERT INTO `carriers` VALUES (1,'vodafone','2017-10-20 04:50:48','2017-10-20 04:50:48'),(2,'airtel','2017-10-20 04:50:48','2017-10-20 04:50:48'),(3,'airtel','2017-10-20 04:50:48','2017-10-20 04:50:48'),(4,'idea','2017-10-20 04:50:48','2017-10-20 04:50:48'),(5,'airtel','2017-10-20 04:50:48','2017-10-20 04:50:48'),(6,'vodafone','2017-10-20 04:50:48','2017-10-20 04:50:48'),(7,'vodafone','2017-10-20 04:50:48','2017-10-20 04:50:48'),(8,'airtel','2017-10-20 04:50:48','2017-10-20 04:50:48'),(9,'vodafone','2017-10-20 04:50:48','2017-10-20 04:50:48'),(10,'idea','2017-10-20 04:50:48','2017-10-20 04:50:48'),(11,'airtel','2017-10-20 04:50:48','2017-10-20 04:50:48'),(12,'airtel','2017-10-20 04:50:49','2017-10-20 04:50:49'),(13,'airtel','2017-10-20 04:50:49','2017-10-20 04:50:49'),(14,'vodafone','2017-10-20 04:50:49','2017-10-20 04:50:49'),(15,'vodafone','2017-10-20 04:50:49','2017-10-20 04:50:49');
/*!40000 ALTER TABLE `carriers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_histories`
--

DROP TABLE IF EXISTS `device_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `range_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_histories`
--

LOCK TABLES `device_histories` WRITE;
/*!40000 ALTER TABLE `device_histories` DISABLE KEYS */;
INSERT INTO `device_histories` VALUES (1,'1971-11-23 05:01:13','-44.299562','126.203318','3',8,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(2,'1975-04-26 16:57:45','78.44028','-138.983584','4',9,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(3,'1975-09-18 19:56:22','16.490459','-162.742383','4',5,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(4,'1998-09-08 12:45:02','58.100714','-4.345699','4',7,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(5,'2003-11-03 15:02:56','-2.381462','166.741237','4',4,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(6,'2007-05-17 21:33:25','27.480061','30.966789','4',4,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(7,'2001-02-20 20:36:18','-18.224628','-93.267921','3',1,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(8,'1971-07-11 00:36:01','-8.618556','178.366525','1',9,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(9,'1981-06-15 11:00:15','46.182391','139.203232','2',5,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(10,'1978-05-24 21:28:17','17.583623','-171.497459','3',6,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(11,'1983-02-20 01:21:55','13.676165','-47.863887','1',3,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(12,'1984-09-24 09:08:01','89.233533','29.70172','2',3,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(13,'2009-05-22 09:07:39','-5.094591','37.226419','2',9,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(14,'1970-02-10 10:39:49','-39.536099','-149.65823','4',1,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(15,'2004-02-01 22:49:17','-17.997117','88.274821','1',6,'2017-10-20 04:50:49','2017-10-20 04:50:49');
/*!40000 ALTER TABLE `device_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_packages`
--

DROP TABLE IF EXISTS `device_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '1',
  `recharge_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_packages`
--

LOCK TABLES `device_packages` WRITE;
/*!40000 ALTER TABLE `device_packages` DISABLE KEYS */;
INSERT INTO `device_packages` VALUES (1,2,'1994-01-09 21:23:28','883',4,2,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(2,1,'2005-02-16 13:37:05','458',9,9,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(3,1,'1973-09-03 23:01:08','839',1,8,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(4,1,'1996-01-25 01:43:37','376',6,9,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(5,2,'1975-10-07 03:45:40','980',2,6,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(6,1,'1980-06-24 22:40:28','870',8,7,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(7,1,'1989-03-08 03:43:56','519',1,1,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(8,1,'1986-10-30 08:21:42','472',7,4,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(9,1,'1982-10-12 15:27:39','738',3,9,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(10,1,'2003-11-29 10:26:07','812',1,9,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(11,1,'1983-04-17 16:16:23','958',7,9,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(12,1,'1996-01-19 20:21:02','283',3,4,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(13,1,'1997-03-04 14:22:33','366',1,1,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(14,2,'2005-07-25 17:04:25','409',1,3,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(15,2,'2002-01-29 12:45:29','523',1,3,'2017-10-20 04:50:50','2017-10-20 04:50:50');
/*!40000 ALTER TABLE `device_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deviceId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `carrier_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'Estefania Stehr','pl9Vy9fwSB','1986-03-22 22:58:39','1977-07-26 07:49:47',1,5,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(2,'Lizzie Farrell','5r7a3EFXub','2001-03-28 13:27:54','2002-09-08 09:16:32',1,6,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(3,'Melany Wolff','VOTffQyp13','1997-06-13 09:20:52','2003-01-19 18:54:47',1,6,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(4,'Adele Mann','uGg58MDPJY','1990-11-08 02:58:26','1991-09-06 06:15:42',2,7,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(5,'Dr. Jaydon Sanford DDS','IoyYBhl8Fs','1985-01-27 12:11:55','2013-03-09 03:52:01',1,1,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(6,'Jordane Kuhic','cnx0T1cyxJ','1985-11-02 05:54:44','1984-01-07 12:27:43',1,4,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(7,'Abigale Considine','G1x4lPTSB7','2016-08-17 02:15:13','2003-04-07 22:26:35',1,6,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(8,'Ms. Stella Nader DVM','4MgYzqOL4G','1989-09-20 08:59:01','2004-03-01 11:45:15',2,3,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(9,'Avis Rolfson','x9eY2O9OG2','2003-07-29 23:44:23','1986-01-20 04:15:09',2,1,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(10,'Price Fisher','9a3SqIE6rt','1994-09-22 10:55:28','1970-12-08 08:07:09',2,8,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(11,'Maeve Corkery','HG45I51TbR','2017-10-07 21:33:13','1995-08-22 23:26:09',2,7,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(12,'Faye Kunde','pPJIgRr8Ne','2016-07-29 13:09:59','2006-11-30 02:06:17',1,7,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(13,'Sasha Beatty PhD','HpSzOQXXpK','2009-11-22 00:58:59','1985-10-03 06:29:29',1,3,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(14,'Prof. Judy Wolf','XrBdFybG8D','1999-08-20 16:11:25','1997-01-25 16:29:17',1,4,'2017-10-20 04:50:49','2017-10-20 04:50:49'),(15,'Mrs. Angela Kuhn DDS','Vz5geTAw00','1984-07-26 11:04:09','1977-03-22 10:43:09',1,4,'2017-10-20 04:50:49','2017-10-20 04:50:49');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_histories`
--

DROP TABLE IF EXISTS `login_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_histories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_histories`
--

LOCK TABLES `login_histories` WRITE;
/*!40000 ALTER TABLE `login_histories` DISABLE KEYS */;
INSERT INTO `login_histories` VALUES (1,'1998-08-17 07:39:00',9,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(2,'1975-01-01 01:33:26',6,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(3,'2000-09-04 16:42:28',3,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(4,'1979-09-01 19:26:24',6,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(5,'1986-01-20 16:37:16',2,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(6,'1987-04-09 23:19:19',4,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(7,'1976-12-09 04:27:05',8,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(8,'2015-04-17 18:09:39',5,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(9,'2000-08-24 00:48:05',1,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(10,'1983-07-08 13:03:10',2,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(11,'1990-09-24 03:52:51',6,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(12,'2002-07-16 03:25:18',9,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(13,'1972-11-11 14:05:45',6,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(14,'1979-10-16 15:22:25',4,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(15,'2007-01-05 11:03:48',5,'2017-10-20 04:50:50','2017-10-20 04:50:50');
/*!40000 ALTER TABLE `login_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=397 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (388,'2014_10_12_000000_create_users_table',1),(389,'2014_10_12_100000_create_password_resets_table',1),(390,'2017_10_17_050737_create_devices_table',1),(391,'2017_10_17_050813_create_carriers_table',1),(392,'2017_10_17_050823_create_packages_table',1),(393,'2017_10_17_050838_create_login_histories_table',1),(394,'2017_10_17_050859_create_device_packages_table',1),(395,'2017_10_17_050939_create_device_histories_table',1),(396,'2017_10_17_051014_create_user_devices_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `offer_price` decimal(8,2) NOT NULL,
  `duration` int(11) NOT NULL,
  `usage` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
INSERT INTO `packages` VALUES (1,'Lucy Jacobson','Quisquam consequatur sequi et eos nesciunt.',330.00,313.00,16,147,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(2,'Myrtle Sanford','Quasi officiis dolore sunt fuga officiis laboriosam minima.',107.00,184.00,6,227,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(3,'Camila West','Accusamus rerum et facere et harum accusamus.',149.00,75.00,11,932,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(4,'Dr. Jesus Abshire DVM','Necessitatibus et voluptas porro tempore dolorem quo.',235.00,82.00,22,424,'2017-10-20 04:50:50','2017-10-20 04:50:50'),(5,'Rosella Murazik','Inventore non id nulla voluptatibus nisi odit ipsa.',227.00,140.00,3,332,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(6,'Janick Keeling MD','Id natus inventore ducimus laudantium consequuntur molestiae omnis.',318.00,292.00,4,104,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(7,'Prof. Greyson Kshlerin Jr.','Recusandae id ut omnis molestias.',133.00,175.00,6,711,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(8,'Kamren Effertz','Molestiae aut et nulla sit maiores.',220.00,169.00,17,360,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(9,'Asa Grimes','Sapiente error voluptate qui amet deserunt.',274.00,400.00,8,620,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(10,'Sage West','Sunt qui quos non odio atque.',317.00,178.00,23,775,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(11,'Abbigail Beer','Eligendi qui error vel est cum.',139.00,58.00,24,691,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(12,'Hubert Satterfield','Eum enim delectus delectus itaque reprehenderit corporis possimus.',410.00,93.00,6,725,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(13,'Melissa Quitzon','Dolores delectus dolores distinctio a aut.',477.00,236.00,15,765,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(14,'Keanu Baumbach','Sint est reiciendis ullam repellat velit.',315.00,176.00,21,279,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(15,'Zachary Rau DVM','Quia qui non aut ut excepturi.',404.00,203.00,19,113,'2017-10-20 04:50:51','2017-10-20 04:50:51');
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_devices`
--

DROP TABLE IF EXISTS `user_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_devices`
--

LOCK TABLES `user_devices` WRITE;
/*!40000 ALTER TABLE `user_devices` DISABLE KEYS */;
INSERT INTO `user_devices` VALUES (1,8,2,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(2,3,7,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(3,9,7,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(4,4,5,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(5,1,5,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(6,3,8,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(7,8,6,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(8,2,9,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(9,1,6,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(10,9,3,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(11,6,7,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(12,6,2,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(13,5,1,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(14,3,6,'2017-10-20 04:50:51','2017-10-20 04:50:51'),(15,8,4,'2017-10-20 04:50:51','2017-10-20 04:50:51');
/*!40000 ALTER TABLE `user_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgotten_password_time` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','brett26@example.com','$2y$10$YJcD9NqRbz2Xe7w8sHn.7uR3no.f2Xy5UD2.1w4oDVMwXSKmn7bY6','1-251-609-6402 x793','3345 Amanda Light\nKihnborough, OK 81442','34449-3356','Pfeffer Cape','Connelly Passage','Braun Fall','Guinea',2,'50.95.40.102','japeR0j0Rj','public','09:24:56','2006-05-22 05:30:38','GPmqo2PXAW','2017-10-20 04:50:48','2017-10-20 04:50:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-20 15:52:30
